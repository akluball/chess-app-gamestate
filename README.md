# Chess Application Gamestate

This is the gamestate microservice of the [chess-application].

## Test

Integration tests require jdk8 and docker.

Run test setup (start payara, database, and mock service containers and deploy application):
```
./gradlew iTestSetup
```
This only needs to be run before the first test run (not before each).

Run integration tests (redeploys application at start):
```
./gradlew iTest
```

Run test teardown (stops payara and mock service containers):
```
./gradlew iTestTeardown
```
This only needs to be run after the last test run (not after each).

## Build

```
./gradlew war
```

## Expected Environment

The application expects the following variables set in the deployment environment:
1. `CHESSAPP_GAMEHISTORY_URI`
2. `CHESSAPP_GAMESTATE_PRIVATEKEY`
3. `CHESSAPP_GAMEHISTORY_PUBLICKEY`
4. `CHESSAPP_USER_PUBLICKEY`

Additionally, the path to an sql script can be specified using `CHESSAPP_GAMESTATE_SQL`.

## Documentation

Create OpenAPI spec (written to `build/openapi.yaml`):
```
./gradlew resolve
```

## Notes

Some of the integration tests that check time ranges may flake if the tests are running slow.
This can be avoided by increasing the acceptable time range (see [Util]).

The test deployment uses postgres while the dev deployment uses embedded derby.

[chess-application]: https://gitlab.com/akluball/chess-app.git

[Util]: src/test/integration/java/com/gitlab/akluball/chessapp/gamestate/itest/util/Util.java
