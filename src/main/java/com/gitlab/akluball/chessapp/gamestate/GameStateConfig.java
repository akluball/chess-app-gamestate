package com.gitlab.akluball.chessapp.gamestate;

import javax.inject.Singleton;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.minutesToSeconds;

@Singleton
public class GameStateConfig {
    private final String gameHistoryUri;
    private final PrivateKey gameStatePrivateKey;
    private final RSAPublicKey gameHistoryPublicKey;
    private final RSAPublicKey userPublicKey;

    GameStateConfig() throws NoSuchAlgorithmException, InvalidKeySpecException {
        this.gameHistoryUri = System.getenv("CHESSAPP_GAMEHISTORY_URI");
        Base64.Decoder decoder = Base64.getDecoder();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        byte[] gameStatePrivateKeyBytes = decoder.decode(System.getenv("CHESSAPP_GAMESTATE_PRIVATEKEY"));
        this.gameStatePrivateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(gameStatePrivateKeyBytes));
        byte[] gameHistoryPublicKeyBytes = decoder.decode(System.getenv("CHESSAPP_GAMEHISTORY_PUBLICKEY"));
        this.gameHistoryPublicKey = (RSAPublicKey) keyFactory
                .generatePublic(new X509EncodedKeySpec(gameHistoryPublicKeyBytes));
        byte[] userPublicKeyBytes = decoder.decode(System.getenv("CHESSAPP_USER_PUBLICKEY"));
        this.userPublicKey = (RSAPublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(userPublicKeyBytes));
    }

    public String gameHistoryUri() {
        return this.gameHistoryUri;
    }

    public PrivateKey gameStatePrivateKey() {
        return this.gameStatePrivateKey;
    }

    public int tokenLifeSeconds() {
        return minutesToSeconds(5);
    }

    public RSAPublicKey gameHistoryPublicKey() {
        return this.gameHistoryPublicKey;
    }

    public RSAPublicKey userPublicKey() {
        return this.userPublicKey;
    }
}
