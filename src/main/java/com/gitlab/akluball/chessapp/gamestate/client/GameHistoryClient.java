package com.gitlab.akluball.chessapp.gamestate.client;

import com.gitlab.akluball.chessapp.gamestate.GameStateConfig;
import com.gitlab.akluball.chessapp.gamestate.model.EndedGame;
import com.gitlab.akluball.chessapp.gamestate.security.GameStateSecurity;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static com.gitlab.akluball.chessapp.gamestate.security.SecurityUtil.bearer;
import static com.gitlab.akluball.chessapp.gamestate.util.Util.is2xx;

@Singleton
public class GameHistoryClient {
    private final WebTarget target;
    private final GameStateSecurity gameStateSecurity;

    @Inject
    GameHistoryClient(GameStateConfig config, Client client, GameStateSecurity gameStateSecurity) {
        this.target = client.target(config.gameHistoryUri())
                .path("api")
                .path("gamehistory");
        this.gameStateSecurity = gameStateSecurity;
    }

    public boolean end(EndedGame endedGame) {
        Response response = this.target.path("" + endedGame.getGameHistoryId())
                .path("end")
                .request()
                .header("Authorization", bearer(this.gameStateSecurity.getToken()))
                .post(Entity.json(endedGame.toEndGameDto()));
        return is2xx(response);
    }
}
