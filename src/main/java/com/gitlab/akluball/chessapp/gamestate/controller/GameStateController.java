package com.gitlab.akluball.chessapp.gamestate.controller;

import com.gitlab.akluball.chessapp.gamestate.controller.openapi.OpenApiSecuritySchemes;
import com.gitlab.akluball.chessapp.gamestate.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.gamestate.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.security.Bearer;
import com.gitlab.akluball.chessapp.gamestate.security.BearerRole;
import com.gitlab.akluball.chessapp.gamestate.security.CurrentUserId;
import com.gitlab.akluball.chessapp.gamestate.security.ServiceName;
import com.gitlab.akluball.chessapp.gamestate.service.GameStateService;
import com.gitlab.akluball.chessapp.gamestate.transfer.GameStateDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Path("gamestate")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.GAMESTATE)
@SecurityRequirement(name = OpenApiSecuritySchemes.BEARER)
public class GameStateController {
    private GameStateService gameStateService;
    private Integer currentUserId;

    public GameStateController() {
    }

    @Inject
    public GameStateController(GameStateService gameStateService, @CurrentUserId Integer currentUserId) {
        this.gameStateService = gameStateService;
        this.currentUserId = currentUserId;
    }

    @PUT
    @Path("{gameHistoryId}")
    @Bearer(roles = BearerRole.SERVICE, serviceNames = ServiceName.GAMEHISTORY)
    @Operation(summary = "Creates GameState", description = "Creates GameState from create dto")
    @ApiResponse(responseCode = "204", description = "GameState created")
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public void create(@PathParam("gameHistoryId") Integer gameHistoryId, GameState.CreateDto createDto) {
        this.gameStateService.create(gameHistoryId, createDto);
    }

    @GET
    @Path("{gameHistoryId}")
    @Bearer(roles = BearerRole.USER)
    @Operation(
            summary = "Gets GameState by game history id",
            description = "Gets GameState with the given game history id"
    )
    @ApiResponse(
            responseCode = "200",
            description = "Fetched GameState",
            content = @Content(schema = @Schema(implementation = GameStateDto.class))
    )
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "404", description = "not found")
    public GameStateDto get(@PathParam("gameHistoryId") Integer gameHistoryId) {
        return this.gameStateService.findByGameHistoryId(gameHistoryId, this.currentUserId)
                .toDto();
    }

    @GET
    @Bearer(roles = BearerRole.USER)
    @Operation(
            summary = "Gets GameStates by participant",
            description = "Gets GameStates user is participant in"
    )
    @ApiResponse(
            responseCode = "200",
            description = "Fetched GameStates",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = GameStateDto.class)))
    )
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public List<GameStateDto> getByParticipant(
            @QueryParam("userId") Integer userId,
            @DefaultValue("false") @QueryParam("isTurn") Boolean isTurn
    ) {
        userId = (Objects.nonNull(userId)) ? userId : this.currentUserId;
        return this.gameStateService.findByParticipant(userId, isTurn)
                .map(GameState::toDto)
                .collect(Collectors.toList());
    }
}
