package com.gitlab.akluball.chessapp.gamestate.controller.openapi;

public class OpenApiTags {
    public static final String GAMESTATE = "Game State";
}
