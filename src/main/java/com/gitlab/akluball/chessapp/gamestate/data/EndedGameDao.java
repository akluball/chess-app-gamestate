package com.gitlab.akluball.chessapp.gamestate.data;

import com.gitlab.akluball.chessapp.gamestate.model.EndedGame;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class EndedGameDao {
    @PersistenceContext(unitName = "gameStatePu")
    private EntityManager entityManager;

    public void persist(EndedGame endedGame) {
        this.entityManager.persist(endedGame);
    }
}
