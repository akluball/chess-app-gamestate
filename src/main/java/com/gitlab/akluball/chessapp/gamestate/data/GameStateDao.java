package com.gitlab.akluball.chessapp.gamestate.data;

import com.gitlab.akluball.chessapp.gamestate.model.GameState;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class GameStateDao {
    @PersistenceContext(unitName = "gameStatePu")
    private EntityManager entityManager;

    public void persist(GameState gameState) {
        this.entityManager.persist(gameState);
    }

    public GameState findByGameHistoryId(int gameHistoryId) {
        return this.entityManager.find(GameState.class, gameHistoryId);
    }

    public List<GameState> findByParticipant(int userId) {
        return this.entityManager.createNamedQuery(GameState.Query.Name.FIND_BY_PARTICIPANT, GameState.class)
                .setParameter(GameState.Query.Param.USER_ID, userId)
                .getResultList();
    }

    public List<GameState> findByParticipantsTurn(int userId) {
        return this.entityManager.createNamedQuery(GameState.Query.Name.FIND_BY_PARTICIPANT, GameState.class)
                .setParameter(GameState.Query.Param.USER_ID, userId)
                .getResultStream()
                .filter(gameState -> gameState.isParticipantsTurn(userId))
                .collect(Collectors.toList());
    }

    public void delete(GameState gameState) {
        GameState merged = this.entityManager.merge(gameState);
        this.entityManager.remove(merged);
    }
}
