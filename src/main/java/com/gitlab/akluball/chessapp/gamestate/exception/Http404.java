package com.gitlab.akluball.chessapp.gamestate.exception;

import javax.ejb.ApplicationException;

public class Http404 extends HttpException {
    public Http404(String criteria) {
        super(404, String.format("not found: %s", criteria));
    }

    @ApplicationException(rollback = false)
    public static class NoRollback extends Http404 {
        public NoRollback(String criteria) {
            super(criteria);
        }
    }
}
