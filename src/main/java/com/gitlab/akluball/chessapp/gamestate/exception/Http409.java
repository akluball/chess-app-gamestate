package com.gitlab.akluball.chessapp.gamestate.exception;

public class Http409 extends HttpException {
    public Http409(String conflictCriteria) {
        super(409, String.format("conflict: %s", conflictCriteria));
    }
}
