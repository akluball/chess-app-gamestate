package com.gitlab.akluball.chessapp.gamestate.exception;

public class Http500 extends HttpException {
    public Http500(String description) {
        super(500, String.format("internal server error: %s", description));
    }
}
