package com.gitlab.akluball.chessapp.gamestate.http;

import javax.ws.rs.ext.Provider;
import java.util.Objects;

@Provider
public class BooleanTypeCheckParamConverterProvider extends TypeCheckParamConverterProvider<Boolean> {
    protected BooleanTypeCheckParamConverterProvider() {
        super(Boolean.class);
    }

    @Override
    protected boolean isValidType(String value) {
        return Objects.nonNull(value)
                && (value.toLowerCase().equals("true") || value.toLowerCase().equals("false"));
    }
}
