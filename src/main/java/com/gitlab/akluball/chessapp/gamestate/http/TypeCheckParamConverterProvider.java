package com.gitlab.akluball.chessapp.gamestate.http;

import com.gitlab.akluball.chessapp.gamestate.exception.Http400;

import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class TypeCheckParamConverterProvider<P> implements ParamConverterProvider {
    private static final Set<Class<? extends Annotation>> PARAM_ANNOTATIONS = Stream.of(
            CookieParam.class,
            FormParam.class,
            HeaderParam.class,
            MatrixParam.class,
            PathParam.class,
            QueryParam.class
    ).collect(Collectors.toSet());

    private final Class<P> paramType;
    private final String simpleName;
    @Context
    private ParamConverterProvider paramConverterProvider;

    protected TypeCheckParamConverterProvider(Class<P> paramType) {
        this.paramType = paramType;
        this.simpleName = paramType.getSimpleName();
    }

    protected abstract boolean isValidType(String value);

    @Override
    @SuppressWarnings("unchecked")
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        if (rawType.equals(this.paramType)) {
            ParamConverter<P> defaultParamConverter = this.paramConverterProvider
                    .getConverter(this.paramType, genericType, annotations);
            return (ParamConverter<T>) new ParamConverter<P>() {
                @Override
                public P fromString(String value) {
                    if (TypeCheckParamConverterProvider.this.isValidType(value)) {
                        return defaultParamConverter.fromString(value);
                    } else if (Objects.isNull(value)) {
                        return null;
                    } else {
                        Annotation paramAnnotation = Stream.of(annotations)
                                .filter(annotation -> PARAM_ANNOTATIONS.contains(annotation.annotationType()))
                                .findFirst()
                                .orElseThrow(Http400::new);
                        String param = null;
                        String paramName = null;
                        if (paramAnnotation instanceof CookieParam) {
                            param = "cookie";
                            paramName = ((CookieParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof FormParam) {
                            param = "form";
                            paramName = ((FormParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof HeaderParam) {
                            param = "header";
                            paramName = ((HeaderParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof MatrixParam) {
                            param = "matrix";
                            paramName = ((MatrixParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof PathParam) {
                            param = "path";
                            paramName = ((PathParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof QueryParam) {
                            param = "query";
                            paramName = ((QueryParam) paramAnnotation).value();
                        }
                        if (Objects.nonNull(param)) {
                            throw new Http400(String.format(
                                    "%s param %s must be a %s",
                                    param,
                                    paramName,
                                    TypeCheckParamConverterProvider.this.simpleName));
                        } else {
                            throw new Http400();
                        }
                    }
                }

                @Override
                public String toString(P value) {
                    return defaultParamConverter.toString(value);
                }
            };
        } else {
            return null;
        }
    }
}
