package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AdjacentTargetStrategy implements TargetStrategy {
    @Override
    public Set<Position> getTargets(Position source) {
        return Stream.of(new CardinalDirectionTargetStrategy().getTargets(source),
                new DiagonalTargetStrategy().getTargets(source))
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }
}
