package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.HashSet;
import java.util.Set;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.*;
import static com.gitlab.akluball.chessapp.gamestate.util.Util.setOf;

public class AttackChannel {
    private final Board board;
    private final Set<Piece> blockers;
    private Set<Position> positions;

    private AttackChannel(Board board, boolean isWhiteSideAttackChannel) {
        this.board = board;
        if (isWhiteSideAttackChannel) {
            this.blockers = setOf(BLACK_PAWN, BLACK_ROOK, BLACK_KNIGHT, BLACK_BISHOP, BLACK_QUEEN);
        } else {
            this.blockers = setOf(WHITE_PAWN, WHITE_ROOK, WHITE_KNIGHT, WHITE_BISHOP, WHITE_QUEEN);
        }
        this.positions = new HashSet<>();
    }

    AttackChannel(Board board, boolean isWhiteSide, Position attackSource) {
        this(board, isWhiteSide);
        this.positions = setOf(attackSource);
    }

    boolean isBlockable() {
        return this.positions.stream()
                .anyMatch(position -> this.board.canAnyOfMoveTo(this.blockers, position));
    }

    static class Builder {
        private final AttackChannel attackChannel;

        Builder(Board board, boolean isWhiteSide) {
            this.attackChannel = new AttackChannel(board, isWhiteSide);
        }

        Builder position(Position position) {
            this.attackChannel.positions.add(position);
            return this;
        }

        AttackChannel build() {
            return this.attackChannel;
        }
    }
}
