package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Set;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.*;
import static com.gitlab.akluball.chessapp.gamestate.util.Util.setOf;

public class AttackChannelDetector {
    private final Board board;
    private final MoveHistory moveHistory;
    private final Position toAttack;
    private final boolean isWhiteSideAttacking;
    private final Set<Piece> cardinalDirectionAttackers;
    private final Set<Piece> diagonalAttackers;
    private final Piece knightAttacker;
    private final Piece pawnAttacker;
    private final int pawnAttackNegatedRankDelta;

    AttackChannelDetector(Board board, MoveHistory moveHistory, boolean isWhiteSideAttacking, Position toAttack) {
        this.board = board;
        this.moveHistory = moveHistory;
        this.toAttack = toAttack;
        this.isWhiteSideAttacking = isWhiteSideAttacking;
        if (this.isWhiteSideAttacking) {
            this.cardinalDirectionAttackers = setOf(WHITE_ROOK, WHITE_QUEEN);
            this.diagonalAttackers = setOf(WHITE_BISHOP, WHITE_QUEEN);
            this.knightAttacker = WHITE_KNIGHT;
            this.pawnAttacker = WHITE_PAWN;
            this.pawnAttackNegatedRankDelta = -1;
        } else {
            this.cardinalDirectionAttackers = setOf(BLACK_ROOK, BLACK_QUEEN);
            this.diagonalAttackers = setOf(BLACK_BISHOP, BLACK_QUEEN);
            this.knightAttacker = BLACK_KNIGHT;
            this.pawnAttacker = BLACK_PAWN;
            this.pawnAttackNegatedRankDelta = 1;
        }
    }

    private AttackChannel detectAttackChannel(Set<Piece> attackers, int fileShift, int rankShift) {
        AttackChannel.Builder attackChannelBuilder = new AttackChannel.Builder(this.board, this.isWhiteSideAttacking);
        Position position = this.toAttack;
        while (position != null) {
            position = position.shift(fileShift, rankShift);
            if (this.board.isAnyOfAtPosition(attackers, position)) {
                return attackChannelBuilder.position(position).build();
            } else if (this.board.isOccupiedPosition(position)) {
                return null;
            } else {
                attackChannelBuilder.position(position);
            }
        }
        return null;
    }

    private AttackChannel detectAttackChannel(Piece piece, Position attackSource) {
        return (this.board.isPieceAtPosition(piece, attackSource))
                ? new AttackChannel.Builder(this.board, this.isWhiteSideAttacking).position(attackSource).build()
                : null;
    }

    private AttackChannel detectPawnAttackChannel(Position attackSource) {
        return (this.board.isPieceAtPosition(this.pawnAttacker, attackSource))
                ? new PawnAttackChannel(this.board, this.moveHistory, this.isWhiteSideAttacking, attackSource)
                : null;
    }

    AttackChannel detectAttackFromLeft() {
        return this.detectAttackChannel(this.cardinalDirectionAttackers, -1, 0);
    }

    AttackChannel detectAttackFromAbove() {
        return this.detectAttackChannel(this.cardinalDirectionAttackers, 0, 1);
    }

    AttackChannel detectAttackFromRight() {
        return this.detectAttackChannel(this.cardinalDirectionAttackers, 1, 0);
    }

    AttackChannel detectAttackFromBelow() {
        return this.detectAttackChannel(this.cardinalDirectionAttackers, 0, -1);
    }

    AttackChannel detectDiagonalAttackFromUpLeft() {
        return this.detectAttackChannel(this.diagonalAttackers, -1, 1);
    }

    AttackChannel detectDiagonalAttackFromUpRight() {
        return this.detectAttackChannel(this.diagonalAttackers, 1, 1);
    }

    AttackChannel detectDiagonalAttackFromDownRight() {
        return this.detectAttackChannel(this.diagonalAttackers, 1, -1);
    }

    AttackChannel detectDiagonalAttackFromDownLeft() {
        return this.detectAttackChannel(this.diagonalAttackers, -1, -1);
    }

    AttackChannel detectPawnAttackFromLeft() {
        return this.detectPawnAttackChannel(this.toAttack.shift(-1, this.pawnAttackNegatedRankDelta));
    }

    AttackChannel detectPawnAttackFromRight() {
        return this.detectPawnAttackChannel(this.toAttack.shift(1, this.pawnAttackNegatedRankDelta));
    }

    AttackChannel detectKnightAttackFromLeftUp() {
        return this.detectAttackChannel(this.knightAttacker, this.toAttack.shift(-2, 1));
    }

    AttackChannel detectKnightAttackFromUpLeft() {
        return this.detectAttackChannel(this.knightAttacker, this.toAttack.shift(-1, 2));
    }

    AttackChannel detectKnightAttackFromUpRight() {
        return this.detectAttackChannel(this.knightAttacker, this.toAttack.shift(1, 2));
    }

    AttackChannel detectKnightAttackFromRightUp() {
        return this.detectAttackChannel(this.knightAttacker, this.toAttack.shift(2, 1));
    }

    AttackChannel detectKnightAttackFromRightDown() {
        return this.detectAttackChannel(this.knightAttacker, this.toAttack.shift(2, -1));
    }

    AttackChannel detectKnightAttackFromDownRight() {
        return this.detectAttackChannel(this.knightAttacker, this.toAttack.shift(1, -2));
    }

    AttackChannel detectKnightAttackFromDownLeft() {
        return this.detectAttackChannel(this.knightAttacker, this.toAttack.shift(-1, -2));
    }

    AttackChannel detectKnightAttackFromLeftDown() {
        return this.detectAttackChannel(this.knightAttacker, this.toAttack.shift(-2, -1));
    }
}
