package com.gitlab.akluball.chessapp.gamestate.model;

public class BishopMove extends Move {
    private final Board board;
    private final Position source;
    private final Position target;

    BishopMove(Board board, Piece piece, Position source, Position target) {
        super(board, piece, source, target);
        this.board = board;
        this.source = source;
        this.target = target;
    }

    @Override
    boolean isValidPieceSpecific() {
        if (Math.abs(super.computeFileDelta()) == Math.abs(super.computeRankDelta())) {
            return Position.Channel.diagonalBetweenExclusive(this.source, this.target)
                    .isClear(this.board);
        }
        return false;
    }
}
