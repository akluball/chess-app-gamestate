package com.gitlab.akluball.chessapp.gamestate.model;

class BlackKingCastleLeftMove extends CastlingMove {

    BlackKingCastleLeftMove(Board board, MoveHistory moveHistory, Position source, Position target) {
        super(board, moveHistory, false, source, target);
    }

    @Override
    Position kingStart() {
        return Position.E8;
    }

    @Override
    Position kingIntermediate() {
        return Position.D8;
    }

    @Override
    Position kingEnd() {
        return Position.C8;
    }

    @Override
    Position rookStart() {
        return Position.A8;
    }

    @Override
    Position rookEnd() {
        return Position.D8;
    }
}
