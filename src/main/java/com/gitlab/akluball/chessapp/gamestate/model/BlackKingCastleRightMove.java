package com.gitlab.akluball.chessapp.gamestate.model;

class BlackKingCastleRightMove extends CastlingMove {

    BlackKingCastleRightMove(Board board, MoveHistory moveHistory, Position source, Position target) {
        super(board, moveHistory, false, source, target);
    }

    @Override
    Position kingStart() {
        return Position.E8;
    }

    @Override
    Position kingIntermediate() {
        return Position.F8;
    }

    @Override
    Position kingEnd() {
        return Position.G8;
    }

    @Override
    Position rookStart() {
        return Position.H8;
    }

    @Override
    Position rookEnd() {
        return Position.F8;
    }
}
