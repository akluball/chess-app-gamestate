package com.gitlab.akluball.chessapp.gamestate.model;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.BLACK_PAWN;

class BlackPawnMove extends PawnMove {

    BlackPawnMove(Board board, Position source, Position target) {
        super(board, BLACK_PAWN, source, target);
    }

    static Move createMove(Board board,
            MoveHistory moveHistory,
            Position source,
            Position target,
            String promotionPieceLetter) {
        if ((Math.abs(Position.computeFileDelta(source, target)) == 1) && !board.isOpponentOfAt(BLACK_PAWN, target)) {
            return new BlackPawnEnPassantMove(board, moveHistory, source, target);
        } else if (target.hasRank(1)) {
            return new BlackPawnPromotionMove(board, source, target, promotionPieceLetter);
        } else {
            return new BlackPawnMove(board, source, target);
        }
    }

    @Override
    int advanceOneUnit() {
        return -1;
    }

    @Override
    int startingRank() {
        return 7;
    }
}
