package com.gitlab.akluball.chessapp.gamestate.model;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.BLACK_KING;
import static com.gitlab.akluball.chessapp.gamestate.model.Piece.BLACK_PAWN;

class BlackPawnPromotionMove extends PawnPromotionMove {
    private final Piece promotionPiece;

    BlackPawnPromotionMove(Board board, Position source, Position target, String promotionPieceLetter) {
        super(board, BLACK_PAWN, source, target);
        this.promotionPiece = Piece.fromStringRepr(String.format("b%s", promotionPieceLetter));
    }

    @Override
    Piece promotionPiece() {
        return this.promotionPiece;
    }

    @Override
    Piece pawn() {
        return BLACK_PAWN;
    }

    @Override
    Piece king() {
        return BLACK_KING;
    }

    @Override
    int advanceOneUnit() {
        return -1;
    }

    @Override
    int startingRank() {
        return 7;
    }
}
