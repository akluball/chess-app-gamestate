package com.gitlab.akluball.chessapp.gamestate.model;

public class BlackPawnTargetStrategy extends PawnTargetStrategy {
    @Override
    int advanceRankDeltaUnit() {
        return -1;
    }
}
