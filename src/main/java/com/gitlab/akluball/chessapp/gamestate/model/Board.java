package com.gitlab.akluball.chessapp.gamestate.model;

import com.gitlab.akluball.chessapp.gamestate.exception.Http500;
import com.gitlab.akluball.chessapp.gamestate.transfer.GameStateDto;
import com.gitlab.akluball.chessapp.gamestate.util.And;
import com.gitlab.akluball.chessapp.gamestate.util.ShortCircuitCollector;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static com.gitlab.akluball.chessapp.gamestate.model.Move.createMove;
import static com.gitlab.akluball.chessapp.gamestate.model.Piece.*;
import static com.gitlab.akluball.chessapp.gamestate.util.Util.setContainsExactly;

@Embeddable
@Access(AccessType.FIELD)
public class Board {
    @ElementCollection
    @MapKeyColumn(name = "position")
    @MapKeyEnumerated(EnumType.STRING)
    @Column(name = "piece")
    @Enumerated(EnumType.STRING)
    Map<Position, Piece> positionToPiece = new HashMap<>();

    private Piece positionToPieceGet(Position position) {
        return (Objects.isNull(position)) ? null : this.positionToPiece.get(position);
    }

    boolean isPieceAtPosition(Piece expectedPiece, Position position) {
        Piece pieceAtPosition = this.positionToPieceGet(position);
        return Objects.nonNull(pieceAtPosition) && pieceAtPosition.equals(expectedPiece);
    }

    boolean isTeammateAtPosition(Piece piece, Position position) {
        Piece pieceAtPosition = this.positionToPieceGet(position);
        return Objects.nonNull(pieceAtPosition) && pieceAtPosition.isTeammate(piece);
    }

    boolean isOpponentAtTarget(Move move) {
        return this.positionToPiece.entrySet()
                .stream()
                .filter(entry -> move.isOpponent(entry.getValue()))
                .map(Map.Entry::getKey)
                .anyMatch(move::isTarget);
    }

    boolean isVacantPosition(Position position) {
        return this.positionToPiece.keySet()
                .stream()
                .noneMatch(position::equals);
    }

    boolean isOpponentOfAt(Piece piece, Position position) {
        return this.positionToPiece.entrySet()
                .stream()
                .filter(entry -> piece.isOpponent(entry.getValue()))
                .map(Map.Entry::getKey)
                .anyMatch(position::equals);
    }

    boolean isOccupiedPosition(Position position) {
        return Objects.nonNull(this.positionToPieceGet(position));
    }

    boolean isAnyOfAtPosition(Set<Piece> piecesToCheck, Position positionToCheck) {
        Piece piece = this.positionToPieceGet(positionToCheck);
        return Objects.nonNull(piece) && piecesToCheck.stream().anyMatch(piece::equals);
    }

    Board execute(Move move) {
        Builder builder = new Builder();
        this.positionToPiece.forEach((position, piece) -> move.place(builder, position, piece));
        return builder.build();
    }

    private Position getKingPosition(boolean isWhiteSide) {
        return this.positionToPiece.entrySet()
                .stream()
                .filter(entry -> entry.getValue().equals((isWhiteSide) ? WHITE_KING : BLACK_KING))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElseThrow(() -> new Http500("encountered board with side with no king"));
    }

    boolean canAnyOfMoveTo(Set<Piece> pieces, Position target) {
        return this.positionToPiece.entrySet()
                .stream()
                .filter(entry -> pieces.contains(entry.getValue()))
                .map(entry -> createMove(this, entry.getValue(), entry.getKey(), target))
                .anyMatch(Move::isValid);
    }

    boolean isInCheck(boolean isWhiteSide) {
        MoveDetector attackDetector = new MoveDetector(this, !isWhiteSide, this.getKingPosition(isWhiteSide));
        return new And().clause(attackDetector::isMoveFromRight)
                .clause(attackDetector::isMoveFromLeft)
                .clause(attackDetector::isMoveFromAbove)
                .clause(attackDetector::isMoveFromBelow)
                .clause(attackDetector::isMoveFromUpLeftDiagonal)
                .clause(attackDetector::isMoveFromUpRightDiagonal)
                .clause(attackDetector::isMoveFromDownRightDiagonal)
                .clause(attackDetector::isMoveFromDownLeftDiagonal)
                .clause(attackDetector::isKnightMoveFromLeftUp)
                .clause(attackDetector::isKnightMoveFromUpLeft)
                .clause(attackDetector::isKnightMoveFromUpRight)
                .clause(attackDetector::isKnightMoveFromRightUp)
                .clause(attackDetector::isKnightMoveFromRightDown)
                .clause(attackDetector::isKnightMoveFromDownRight)
                .clause(attackDetector::isKnightMoveFromDownLeft)
                .clause(attackDetector::isKnightMoveFromLeftDown)
                .clause(attackDetector::isPawnMoveFromLeft)
                .clause(attackDetector::isPawnMoveFromRight)
                .clause(attackDetector::isKingMoveFromLeft)
                .clause(attackDetector::isKingMoveFromUpLeft)
                .clause(attackDetector::isKingMoveFromAbove)
                .clause(attackDetector::isKingMoveFromUpRight)
                .clause(attackDetector::isKingMoveFromRight)
                .clause(attackDetector::isKingMoveFromDownRight)
                .clause(attackDetector::isKingMoveFromBelow)
                .clause(attackDetector::isKingMoveFromDownLeft)
                .evaluate();
    }

    boolean kingHasValidMove(boolean isWhiteSide) {
        Piece king = (isWhiteSide) ? WHITE_KING : BLACK_KING;
        Position kingPosition = this.getKingPosition(isWhiteSide);
        return new ValidMoveDetector(this, king, kingPosition).hasValidMove();
    }

    boolean checkCanBeBlocked(boolean isWhiteSide, MoveHistory moveHistory) {
        Position kingPosition = this.getKingPosition(isWhiteSide);
        AttackChannelDetector attackChannelDetector = new AttackChannelDetector(this, moveHistory, !isWhiteSide,
                kingPosition);
        Set<AttackChannel> attackChannels = new ShortCircuitCollector<>(AttackChannel.class, 2)
                .supply(attackChannelDetector::detectAttackFromLeft)
                .supply(attackChannelDetector::detectAttackFromAbove)
                .supply(attackChannelDetector::detectAttackFromRight)
                .supply(attackChannelDetector::detectAttackFromBelow)
                .supply(attackChannelDetector::detectDiagonalAttackFromUpLeft)
                .supply(attackChannelDetector::detectDiagonalAttackFromUpRight)
                .supply(attackChannelDetector::detectDiagonalAttackFromDownRight)
                .supply(attackChannelDetector::detectDiagonalAttackFromDownLeft)
                .supply(attackChannelDetector::detectPawnAttackFromLeft)
                .supply(attackChannelDetector::detectPawnAttackFromRight)
                .supply(attackChannelDetector::detectKnightAttackFromLeftUp)
                .supply(attackChannelDetector::detectKnightAttackFromUpLeft)
                .supply(attackChannelDetector::detectKnightAttackFromUpRight)
                .supply(attackChannelDetector::detectKnightAttackFromRightUp)
                .supply(attackChannelDetector::detectKnightAttackFromRightDown)
                .supply(attackChannelDetector::detectKnightAttackFromDownRight)
                .supply(attackChannelDetector::detectKnightAttackFromDownLeft)
                .supply(attackChannelDetector::detectKnightAttackFromLeftDown)
                .getCollected();
        long attackChannelsCount = attackChannels.size();
        if (attackChannelsCount == 1) {
            return attackChannels.iterator().next().isBlockable();
        } else {
            return attackChannelsCount == 0;
        }
    }

    boolean hasValidMove(boolean isWhiteSide, MoveHistory moveHistory) {
        return this.positionToPiece.entrySet()
                .stream()
                .filter(entry -> entry.getValue().isWhiteSidePiece() == isWhiteSide)
                .map(entry -> new ValidMoveDetector(this, moveHistory, entry.getValue(), entry.getKey()))
                .anyMatch(ValidMoveDetector::hasValidMove);
    }

    private boolean isKingVsKing() {
        HashSet<Piece> pieces = new HashSet<>(this.positionToPiece.values());
        return setContainsExactly(pieces, WHITE_KING, BLACK_KING);
    }

    private boolean isKingVsKingAndBishop() {
        HashSet<Piece> pieces = new HashSet<>(this.positionToPiece.values());
        return setContainsExactly(pieces, WHITE_KING, BLACK_KING, WHITE_BISHOP)
                || setContainsExactly(pieces, WHITE_KING, BLACK_KING, BLACK_BISHOP);
    }

    private boolean isKingVsKingAndKnight() {
        HashSet<Piece> pieces = new HashSet<>(this.positionToPiece.values());
        return setContainsExactly(pieces, WHITE_KING, BLACK_KING, WHITE_KNIGHT)
                || setContainsExactly(pieces, WHITE_KING, BLACK_KING, BLACK_KNIGHT);
    }

    private boolean isKingsAndSameSquareBishops() {
        HashSet<Piece> pieces = new HashSet<>(this.positionToPiece.values());
        if (!setContainsExactly(pieces, WHITE_KING, BLACK_KING, WHITE_BISHOP, BLACK_BISHOP)) {
            return false;
        }
        Position whiteBishopPosition = this.positionToPiece.entrySet()
                .stream()
                .filter(entry -> WHITE_BISHOP.equals(entry.getValue()))
                .map(Map.Entry::getKey)
                .findFirst()
                .get();
        Position blackBishopPosition = this.positionToPiece.entrySet()
                .stream()
                .filter(entry -> BLACK_BISHOP.equals(entry.getValue()))
                .map(Map.Entry::getKey)
                .findFirst()
                .get();
        return whiteBishopPosition.isSameColorSquareAs(blackBishopPosition);
    }

    boolean hasInsufficientMaterial() {
        if (this.positionToPiece.size() > 4) {
            return false;
        }
        return this.isKingVsKing()
                || this.isKingVsKingAndBishop()
                || this.isKingVsKingAndKnight()
                || this.isKingsAndSameSquareBishops();
    }

    public void loadPositionToPiece(GameStateDto.Builder gameStateDtoBuilder) {
        gameStateDtoBuilder.positionToPiece(this.positionToPiece);
    }

    static class Builder {
        private final Board board;

        Builder() {
            this.board = new Board();
        }

        Builder place(Position position, Piece piece) {
            this.board.positionToPiece.put(position, piece);
            return this;
        }

        Board build() {
            return this.board;
        }
    }
}
