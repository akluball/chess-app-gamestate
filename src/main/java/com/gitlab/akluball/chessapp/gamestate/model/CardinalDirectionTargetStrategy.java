package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Set;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.setOf;

public class CardinalDirectionTargetStrategy implements TargetStrategy {
    @Override
    public Set<Position> getTargets(Position source) {
        return setOf(
                source.shift(-1, 0),
                source.shift(0, 1),
                source.shift(1, 0),
                source.shift(0, -1)
        );
    }
}
