package com.gitlab.akluball.chessapp.gamestate.model;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.*;

abstract class CastlingMove extends Move {
    private final Board board;
    private final MoveHistory moveHistory;
    private final boolean isWhiteMove;
    private final Piece piece;
    private final Position source;
    private final Position target;

    CastlingMove(Board board, MoveHistory moveHistory, boolean isWhiteMove, Position source, Position target) {
        super(board, (isWhiteMove) ? WHITE_KING : BLACK_KING, source, target);
        this.board = board;
        this.moveHistory = moveHistory;
        this.isWhiteMove = isWhiteMove;
        this.piece = (isWhiteMove) ? WHITE_KING : BLACK_KING;
        this.source = source;
        this.target = target;
    }

    private Piece rook() {
        return (this.isWhiteMove) ? WHITE_ROOK : BLACK_ROOK;
    }

    abstract Position kingStart();

    abstract Position kingIntermediate();

    abstract Position kingEnd();

    abstract Position rookStart();

    abstract Position rookEnd();

    private boolean castlesFromOrTooCheck() {
        return this.board.isInCheck(this.isWhiteMove)
                || createMove(this.board, this.moveHistory, this.piece, this.kingStart(), this.kingIntermediate())
                .execute()
                .isInCheck(this.isWhiteMove);
    }

    @Override
    boolean isValidPieceSpecific() {
        return this.kingStart().equals(this.source)
                && this.kingEnd().equals(this.target)
                && this.board.isPieceAtPosition(this.rook(), this.rookStart())
                && Position.Channel.horizontalBetweenExclusive(this.kingStart(), this.rookStart()).isClear(this.board)
                && !this.castlesFromOrTooCheck()
                && !this.moveHistory.hasKingMoved(this.isWhiteMove)
                && !this.moveHistory.hasRookMovedToPosition(this.isWhiteMove, this.rookStart());
    }

    @Override
    void place(Board.Builder boardBuilder, Position position, Piece piece) {
        Piece rook = this.rook();
        if (rook.equals(piece) && this.rookStart().equals(position)) {
            boardBuilder.place(this.rookEnd(), rook);
        } else {
            super.place(boardBuilder, position, piece);
        }
    }
}
