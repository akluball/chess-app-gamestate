package com.gitlab.akluball.chessapp.gamestate.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Access(AccessType.FIELD)
public class EndedGame {
    @Id
    private int gameHistoryId;
    @Enumerated(EnumType.STRING)
    private ResultTag resultTag;
    @Enumerated(EnumType.STRING)
    private TerminationTag terminationTag;
    @ElementCollection
    private List<MoveSummary> moveSummaries;

    public int getGameHistoryId() {
        return this.gameHistoryId;
    }

    public EndGameDto toEndGameDto() {
        return this.new EndGameDto();
    }

    public static class Builder {
        private final EndedGame endedGame;

        public Builder() {
            this.endedGame = new EndedGame();
        }

        public Builder gameHistoryId(int gameHistoryId) {
            this.endedGame.gameHistoryId = gameHistoryId;
            return this;
        }

        public Builder resultTag(ResultTag resultTag) {
            this.endedGame.resultTag = resultTag;
            return this;
        }

        public Builder terminationTag(TerminationTag terminationTag) {
            this.endedGame.terminationTag = terminationTag;
            return this;
        }

        public Builder moveSummaries(List<MoveSummary> moveSummaries) {
            this.endedGame.moveSummaries = moveSummaries;
            return this;
        }

        public EndedGame build() {
            return this.endedGame;
        }
    }

    public class EndGameDto {
        private ResultTag resultTag;
        private TerminationTag terminationTag;
        private List<MoveSummary.Dto> moveSummaries;

        private EndGameDto() {
            EndedGame endedGame = EndedGame.this;
            this.resultTag = endedGame.resultTag;
            this.terminationTag = endedGame.terminationTag;
            this.moveSummaries = endedGame.moveSummaries.stream()
                    .map(MoveSummary::toDto)
                    .collect(Collectors.toList());
        }
    }
}
