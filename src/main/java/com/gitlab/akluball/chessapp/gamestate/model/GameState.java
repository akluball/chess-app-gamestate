package com.gitlab.akluball.chessapp.gamestate.model;

import com.gitlab.akluball.chessapp.gamestate.transfer.GameStateDto;
import com.gitlab.akluball.chessapp.gamestate.transfer.MoveResultDto;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.*;
import static com.gitlab.akluball.chessapp.gamestate.model.Position.*;

@Entity
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = GameState.Query.Name.FIND_BY_PARTICIPANT, query = GameState.Query.FIND_BY_PARTICIPANT),
})
public class GameState {
    @Id
    private int gameHistoryId;
    private int whiteSideUserId;
    private int blackSideUserId;
    @Embedded
    private MoveTimer moveTimer;
    @Embedded
    private Board board;
    @Embedded
    private MoveHistory moveHistory;

    private boolean isWhiteSideUser(int userId) {
        return this.whiteSideUserId == userId;
    }

    private boolean isBlackSideUser(int userId) {
        return this.blackSideUserId == userId;
    }

    public boolean isParticipant(int userId) {
        return this.isWhiteSideUser(userId) || this.isBlackSideUser(userId);
    }

    public boolean isCurrentTimerExpired() {
        return this.moveTimer.isExpired(this.moveHistory.isWhiteSideTurn());
    }

    public boolean isParticipantsTurn(int userId) {
        if (this.isWhiteSideUser(userId)) {
            return this.moveHistory.isWhiteSideTurn();
        }
        if (this.isBlackSideUser(userId)) {
            return !this.moveHistory.isWhiteSideTurn();
        }
        return false;
    }

    public MoveResultDto attemptMove(int userId, Move.InDto moveDto) {
        MoveResultDto.Builder moveResultDtoBuilder = new MoveResultDto.Builder();
        boolean isWhiteSideTurn = this.moveHistory.isWhiteSideTurn();
        if (!isParticipantsTurn(userId)) {
            return moveResultDtoBuilder.notExecuted().build();
        }
        Move move = moveDto.toMove(this.isWhiteSideUser(userId), this.board, this.moveHistory);
        if (!move.isValid()) {
            return moveResultDtoBuilder.notExecuted().build();
        }
        Board boardAfterMove = move.execute();
        if (boardAfterMove.isInCheck(isWhiteSideTurn)) {
            return moveResultDtoBuilder.notExecuted().build();
        }
        MoveTimer.MoveEnd moveEnd;
        if (isWhiteSideTurn) {
            moveEnd = this.moveTimer.createWhiteMoveEnd();
            if (moveEnd.isExpired()) {
                MoveSummary moveSummary = new MoveSummary.Builder().gameStatus(GameStatus.WHITE_TIME_EXPIRED).build();
                return moveResultDtoBuilder.notExecuted().moveSummary(moveSummary).build();
            } else {
                this.moveTimer.commitWhiteMoveEnd(moveEnd);
            }
        } else {
            moveEnd = this.moveTimer.createBlackMoveEnd();
            if (moveEnd.isExpired()) {
                MoveSummary moveSummary = new MoveSummary.Builder().gameStatus(GameStatus.BLACK_TIME_EXPIRED).build();
                return moveResultDtoBuilder.notExecuted().moveSummary(moveSummary).build();
            } else {
                this.moveTimer.commitBlackMoveEnd(moveEnd);
            }
        }
        moveResultDtoBuilder.executed();
        this.board = boardAfterMove;
        MoveSummary.Builder moveSummaryBuilder = new MoveSummary.Builder()
                .durationMillis(moveEnd.getDurationMillis());
        move.loadMove(moveSummaryBuilder);
        MoveSummary moveSummary = moveSummaryBuilder.build();
        this.moveHistory.pushMoveSummary(moveSummary);
        GameStatus gameStatus;
        if (this.board.isInCheck(!isWhiteSideTurn)) {
            if (this.board.kingHasValidMove(!isWhiteSideTurn)
                    || this.board.checkCanBeBlocked(!isWhiteSideTurn, this.moveHistory)) {
                gameStatus = (!isWhiteSideTurn) ? GameStatus.WHITE_IN_CHECK : GameStatus.BLACK_IN_CHECK;
            } else {
                gameStatus = !isWhiteSideTurn ? GameStatus.WHITE_IN_CHECKMATE : GameStatus.BLACK_IN_CHECKMATE;
            }
        } else if (!this.board.hasValidMove(!isWhiteSideTurn, this.moveHistory)
                || this.board.hasInsufficientMaterial()) {
            gameStatus = GameStatus.DRAW;
        } else {
            gameStatus = GameStatus.IN_PROGRESS;
        }
        moveSummary.setGameStatus(gameStatus);
        return moveResultDtoBuilder.moveSummary(moveSummary).loadMillisRemaining(this.moveTimer, !isWhiteSideTurn)
                .build();
    }

    public boolean isEnded() {
        return this.moveTimer.isExpired(this.moveHistory.isWhiteSideTurn()) || this.moveHistory.isEnded();
    }

    public EndedGame toEndedGame() {
        EndedGame.Builder endedGameBuilder = new EndedGame.Builder().gameHistoryId(this.gameHistoryId);
        boolean whiteSideTurn = this.moveHistory.isWhiteSideTurn();
        if (this.moveTimer.isExpired(whiteSideTurn)) {
            ResultTag resultTag = (whiteSideTurn) ? ResultTag.BLACK_WINS : ResultTag.WHITE_WINS;
            endedGameBuilder.resultTag(resultTag).terminationTag(TerminationTag.TIME_FORFEIT);
        } else {
            this.moveHistory.loadTags(endedGameBuilder);
        }
        this.moveHistory.loadMoveSummaries(endedGameBuilder);
        return endedGameBuilder.build();
    }

    public GameStateDto toDto() {
        return new GameStateDto.Builder()
                .gameHistoryId(this.gameHistoryId)
                .whiteSideUserId(this.whiteSideUserId)
                .blackSideUserId(this.blackSideUserId)
                .moveHistory(this.moveHistory)
                .board(this.board)
                .moveTimer(this.moveTimer, this.moveHistory.isWhiteSideTurn())
                .build();
    }

    public static class Query {
        public static final String FIND_BY_PARTICIPANT
                = "SELECT gameState FROM GameState gameState"
                + " WHERE gameState.whiteSideUserId=:" + Param.USER_ID
                + " OR gameState.blackSideUserId=:" + Param.USER_ID
                + " ORDER BY gameState.moveTimer.moveStart.epochSecond DESC";

        public static class Name {
            public static final String FIND_BY_PARTICIPANT = "findByParticipant";
        }

        public static class Param {
            public static final String USER_ID = "userId";
        }
    }

    @Schema(name = "GameStateCreate")
    public static class CreateDto {
        @NotNull(message = "GameStateCreate.whiteSideUserId is required")
        private Integer whiteSideUserId;
        @NotNull(message = "GameStateCreate.blackSideUserId is required")
        private Integer blackSideUserId;
        @NotNull(message = "GameStateCreate.millisPerPlayer is required")
        private Long millisPerPlayer;

        public GameState toGameState(int gameHistoryId) {
            GameState gameState = new GameState();
            gameState.gameHistoryId = gameHistoryId;
            gameState.whiteSideUserId = whiteSideUserId;
            gameState.blackSideUserId = blackSideUserId;
            gameState.moveTimer = new MoveTimer.Builder().millisPerPlayer(this.millisPerPlayer).startNow().build();
            gameState.board = new Board.Builder()
                    .place(A1, WHITE_ROOK).place(B1, WHITE_KNIGHT).place(C1, WHITE_BISHOP).place(D1, WHITE_QUEEN)
                    .place(E1, WHITE_KING).place(F1, WHITE_BISHOP).place(G1, WHITE_KNIGHT).place(H1, WHITE_ROOK)
                    .place(A2, WHITE_PAWN).place(B2, WHITE_PAWN).place(C2, WHITE_PAWN).place(D2, WHITE_PAWN)
                    .place(E2, WHITE_PAWN).place(F2, WHITE_PAWN).place(G2, WHITE_PAWN).place(H2, WHITE_PAWN)
                    .place(A7, BLACK_PAWN).place(B7, BLACK_PAWN).place(C7, BLACK_PAWN).place(D7, BLACK_PAWN)
                    .place(E7, BLACK_PAWN).place(F7, BLACK_PAWN).place(G7, BLACK_PAWN).place(H7, BLACK_PAWN)
                    .place(A8, BLACK_ROOK).place(B8, BLACK_KNIGHT).place(C8, BLACK_BISHOP).place(D8, BLACK_QUEEN)
                    .place(E8, BLACK_KING).place(F8, BLACK_BISHOP).place(G8, BLACK_KNIGHT).place(H8, BLACK_ROOK)
                    .build();
            gameState.moveHistory = new MoveHistory();
            return gameState;
        }
    }
}
