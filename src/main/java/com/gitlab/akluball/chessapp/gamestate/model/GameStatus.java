package com.gitlab.akluball.chessapp.gamestate.model;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;
import java.util.Set;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.setOf;

@JsonbTypeAdapter(GameStatus.Adapter.class)
public enum GameStatus {
    IN_PROGRESS,
    WHITE_IN_CHECK,
    BLACK_IN_CHECK,
    WHITE_IN_CHECKMATE,
    BLACK_IN_CHECKMATE,
    WHITE_TIME_EXPIRED,
    BLACK_TIME_EXPIRED,
    DRAW;

    private static final Set<GameStatus> NOT_ENDED_STATUSES = setOf(IN_PROGRESS, WHITE_IN_CHECK, BLACK_IN_CHECK);

    public boolean isEndedGame() {
        return !NOT_ENDED_STATUSES.contains(this);
    }

    public static class Adapter implements JsonbAdapter<GameStatus, String> {
        @Override
        public String adaptToJson(GameStatus gameStatus) throws Exception {
            return gameStatus.name().toLowerCase().replace("_", "-");
        }

        @Override
        public GameStatus adaptFromJson(String asString) throws Exception {
            return GameStatus.valueOf(asString.toUpperCase().replace("-", "_"));
        }
    }
}
