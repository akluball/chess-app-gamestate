package com.gitlab.akluball.chessapp.gamestate.model;

public class KingMove extends Move {
    KingMove(Board board, Piece piece, Position source, Position target) {
        super(board, piece, source, target);
    }

    static Move createMove(Board board, MoveHistory moveHistory, Piece piece, Position source, Position target) {
        if (Position.computeFileDelta(source, target) == -2) {
            switch (piece) {
                case WHITE_KING:
                    return new WhiteKingCastleLeftMove(board, moveHistory, source, target);
                case BLACK_KING:
                    return new BlackKingCastleLeftMove(board, moveHistory, source, target);
            }
        } else if (Position.computeFileDelta(source, target) == 2) {
            switch (piece) {
                case WHITE_KING:
                    return new WhiteKingCastleRightMove(board, moveHistory, source, target);
                case BLACK_KING:
                    return new BlackKingCastleRightMove(board, moveHistory, source, target);
            }
        }
        return new KingMove(board, piece, source, target);
    }

    @Override
    boolean isValidPieceSpecific() {
        return (Math.abs(super.computeFileDelta()) < 2) && (Math.abs(super.computeRankDelta()) < 2);
    }
}
