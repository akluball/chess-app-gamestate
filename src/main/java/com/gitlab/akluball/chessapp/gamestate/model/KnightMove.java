package com.gitlab.akluball.chessapp.gamestate.model;

public class KnightMove extends Move {
    KnightMove(Board board, Piece piece, Position source, Position target) {
        super(board, piece, source, target);
    }

    @Override
    boolean isValidPieceSpecific() {
        int fileDeltaMagnitude = Math.abs(this.computeFileDelta());
        int rankDeltaMagnitude = Math.abs(this.computeRankDelta());
        return ((fileDeltaMagnitude == 2) && (rankDeltaMagnitude == 1))
                || ((fileDeltaMagnitude == 1) && (rankDeltaMagnitude == 2));
    }
}
