package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Set;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.setOf;

public class KnightTargetStrategy implements TargetStrategy {
    @Override
    public Set<Position> getTargets(Position source) {
        return setOf(
                source.shift(-2, 1),
                source.shift(-1, 2),
                source.shift(1, 2),
                source.shift(2, 1),
                source.shift(2, -1),
                source.shift(1, -2),
                source.shift(-1, -2),
                source.shift(-2, -1)
        );
    }
}
