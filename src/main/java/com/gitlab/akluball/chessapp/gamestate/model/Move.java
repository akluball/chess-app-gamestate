package com.gitlab.akluball.chessapp.gamestate.model;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;
import java.util.Set;

public abstract class Move {
    private final Board board;
    private final Piece piece;
    private final Position source;
    private final Position target;

    Move(Board board, Piece piece, Position source, Position target) {
        this.board = board;
        this.piece = piece;
        this.source = source;
        this.target = target;
    }

    static Move createMove(Board board,
            MoveHistory moveHistory,
            Piece piece,
            Position source,
            Position target,
            String promotionPieceLetter) {
        switch (piece) {
            case WHITE_PAWN:
                return WhitePawnMove.createMove(board, moveHistory, source, target, promotionPieceLetter);
            case BLACK_PAWN:
                return BlackPawnMove.createMove(board, moveHistory, source, target, promotionPieceLetter);
            default:
                return createMove(board, moveHistory, piece, source, target);
        }
    }

    static Move createMove(Board board, MoveHistory moveHistory, Piece piece, Position source, Position target) {
        switch (piece) {
            case WHITE_KING:
            case BLACK_KING:
                return KingMove.createMove(board, moveHistory, piece, source, target);
            default:
                return createMove(board, piece, source, target);
        }
    }

    static Move createMove(Board board, Piece piece, Position source, Position target) {
        switch (piece) {
            case WHITE_PAWN:
                return new WhitePawnMove(board, source, target);
            case BLACK_PAWN:
                return new BlackPawnMove(board, source, target);
            case WHITE_KING:
            case BLACK_KING:
                return new KingMove(board, piece, source, target);
            case WHITE_ROOK:
            case BLACK_ROOK:
                return new RookMove(board, piece, source, target);
            case WHITE_KNIGHT:
            case BLACK_KNIGHT:
                return new KnightMove(board, piece, source, target);
            case WHITE_BISHOP:
            case BLACK_BISHOP:
                return new BishopMove(board, piece, source, target);
            case WHITE_QUEEN:
            case BLACK_QUEEN:
                return new QueenMove(board, piece, source, target);
            default:
                return new NeverValidMove(board, piece, source, target);
        }
    }

    private boolean isValidSource() {
        return Objects.nonNull(this.piece)
                && Objects.nonNull(this.source)
                && this.board.isPieceAtPosition(this.piece, this.source);
    }

    boolean isValidTarget() {
        return Objects.nonNull(this.target)
                && !this.board.isTeammateAtPosition(this.piece, this.target);
    }

    abstract boolean isValidPieceSpecific();

    final boolean isValid() {
        return this.isValidSource()
                && this.isValidTarget()
                && this.isValidPieceSpecific();
    }

    int computeFileDelta() {
        return Position.computeFileDelta(this.source, this.target);
    }

    int computeRankDelta() {
        return Position.computeRankDelta(this.source, this.target);
    }

    boolean isOpponent(Piece piece) {
        return this.piece.isOpponent(piece);
    }

    boolean isTarget(Position position) {
        return this.target.equals(position);
    }

    Board execute() {
        return this.board.execute(this);
    }

    boolean isCaptureMove() {
        return this.board.isOpponentAtTarget(this);
    }

    SourceDisambiguation sourceDisambiguation() {
        Set<Position> sources = new MoveSourceDetector(this.board, this.piece, this.target).getSources();
        sources.remove(this.source);
        if (sources.size() == 0) {
            return SourceDisambiguation.NONE;
        } else if (sources.stream().noneMatch(this.source::hasSameFile)) {
            return SourceDisambiguation.FILE;
        } else if (sources.stream().noneMatch(this.source::hasSameRank)) {
            return SourceDisambiguation.RANK;
        } else {
            return SourceDisambiguation.BOTH;
        }
    }

    Piece promotionPiece() {
        return null;
    }

    void loadMove(MoveSummary.Builder moveSummaryBuilder) {
        moveSummaryBuilder.piece(this.piece)
                .promotionPiece(this.promotionPiece())
                .from(this.source)
                .to(this.target)
                .isCapture(this.isCaptureMove())
                .sourceDisambiguation(this.sourceDisambiguation());
    }

    void place(Board.Builder boardBuilder, Position position, Piece piece) {
        if (this.source.equals(position) && this.piece.equals(piece)) {
            boardBuilder.place(this.target, this.piece);
        } else if (!this.target.equals(position)) {
            boardBuilder.place(position, piece);
        }
    }

    @Schema(name = "Move")
    public static class InDto {
        private String pieceLetter;
        private String promotionPieceLetter;
        private Position source;
        private Position target;

        Move toMove(boolean isWhiteSideMove, Board board, MoveHistory moveHistory) {
            String stringRepr = String.format("%s%s", (isWhiteSideMove) ? "w" : "b", this.pieceLetter);
            Piece piece = Piece.fromStringRepr(stringRepr);
            if (Objects.isNull(piece) || Objects.isNull(this.source) || Objects.isNull(this.target)) {
                return new NeverValidMove(board, piece, this.source, this.target);
            }
            return createMove(board, moveHistory, piece, this.source, this.target, this.promotionPieceLetter);
        }
    }

    private static class NeverValidMove extends Move {
        private NeverValidMove(Board board, Piece piece, Position source, Position target) {
            super(board, piece, source, target);
        }

        @Override
        boolean isValidPieceSpecific() {
            return false;
        }
    }
}
