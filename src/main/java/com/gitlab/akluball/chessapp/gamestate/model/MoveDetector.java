package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Set;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.*;
import static com.gitlab.akluball.chessapp.gamestate.util.Util.setOf;

class MoveDetector {
    private final Board board;
    private final Position target;
    private final Set<Piece> cardinalDirectionMovers;
    private final Set<Piece> diagonalMovers;
    private final Piece knight;
    private final Piece pawn;
    private final int pawnNegatedAdvanceUnit;
    private final Piece king;

    MoveDetector(Board board, boolean isWhiteSideMoving, Position target) {
        this.board = board;
        this.target = target;
        if (isWhiteSideMoving) {
            this.cardinalDirectionMovers = setOf(WHITE_ROOK, WHITE_QUEEN);
            this.diagonalMovers = setOf(WHITE_BISHOP, WHITE_QUEEN);
            this.knight = WHITE_KNIGHT;
            this.pawn = WHITE_PAWN;
            this.pawnNegatedAdvanceUnit = -1;
            this.king = WHITE_KING;
        } else {
            this.cardinalDirectionMovers = setOf(BLACK_ROOK, BLACK_QUEEN);
            this.diagonalMovers = setOf(BLACK_BISHOP, BLACK_QUEEN);
            this.knight = BLACK_KNIGHT;
            this.pawn = BLACK_PAWN;
            this.pawnNegatedAdvanceUnit = 1;
            this.king = BLACK_KING;
        }
    }

    private boolean detectMove(Set<Piece> movers, int fileShift, int rankShift) {
        Position toCheck = this.target;
        while (toCheck != null) {
            toCheck = toCheck.shift(fileShift, rankShift);
            if (this.board.isAnyOfAtPosition(movers, toCheck)) {
                return true;
            } else if (this.board.isOccupiedPosition(toCheck)) {
                return false;
            }
        }
        return false;
    }

    private boolean detectMove(Piece piece, Position source) {
        return this.board.isPieceAtPosition(piece, source);
    }

    boolean isMoveFromLeft() {
        return this.detectMove(this.cardinalDirectionMovers, -1, 0);
    }

    boolean isMoveFromRight() {
        return this.detectMove(this.cardinalDirectionMovers, 1, 0);
    }

    boolean isMoveFromAbove() {
        return this.detectMove(this.cardinalDirectionMovers, 0, 1);
    }

    boolean isMoveFromBelow() {
        return this.detectMove(this.cardinalDirectionMovers, 0, -1);
    }

    boolean isMoveFromUpLeftDiagonal() {
        return this.detectMove(this.diagonalMovers, -1, 1);
    }

    boolean isMoveFromUpRightDiagonal() {
        return this.detectMove(this.diagonalMovers, 1, 1);
    }

    boolean isMoveFromDownRightDiagonal() {
        return this.detectMove(this.diagonalMovers, 1, -1);
    }

    boolean isMoveFromDownLeftDiagonal() {
        return this.detectMove(this.diagonalMovers, -1, -1);
    }

    boolean isKnightMoveFromLeftUp() {
        Position source = this.target.shift(-2, 1);
        return this.detectMove(this.knight, source);
    }

    boolean isKnightMoveFromUpLeft() {
        Position source = this.target.shift(-1, 2);
        return this.detectMove(this.knight, source);
    }

    boolean isKnightMoveFromUpRight() {
        Position source = this.target.shift(1, 2);
        return this.detectMove(this.knight, source);
    }

    boolean isKnightMoveFromRightUp() {
        Position source = this.target.shift(2, 1);
        return this.detectMove(this.knight, source);
    }

    boolean isKnightMoveFromRightDown() {
        Position source = this.target.shift(2, -1);
        return this.detectMove(this.knight, source);
    }

    boolean isKnightMoveFromDownRight() {
        Position source = this.target.shift(1, -2);
        return this.detectMove(this.knight, source);
    }

    boolean isKnightMoveFromDownLeft() {
        Position source = this.target.shift(-1, -2);
        return this.detectMove(this.knight, source);
    }

    boolean isKnightMoveFromLeftDown() {
        Position source = this.target.shift(-2, -1);
        return this.detectMove(this.knight, source);
    }

    boolean isPawnMoveFromLeft() {
        Position source = this.target.shift(-1, this.pawnNegatedAdvanceUnit);
        return this.detectMove(this.pawn, source);
    }

    boolean isPawnMoveFromRight() {
        Position source = this.target.shift(1, this.pawnNegatedAdvanceUnit);
        return this.detectMove(this.pawn, source);
    }

    boolean isKingMoveFromLeft() {
        Position source = this.target.shift(-1, 0);
        return this.detectMove(this.king, source);
    }

    boolean isKingMoveFromUpLeft() {
        Position source = this.target.shift(-1, 1);
        return this.detectMove(this.king, source);
    }

    boolean isKingMoveFromAbove() {
        Position source = this.target.shift(0, 1);
        return this.detectMove(this.king, source);
    }

    boolean isKingMoveFromUpRight() {
        Position source = this.target.shift(1, 1);
        return this.detectMove(this.king, source);
    }

    boolean isKingMoveFromRight() {
        Position source = this.target.shift(1, 0);
        return this.detectMove(this.king, source);
    }

    boolean isKingMoveFromDownRight() {
        Position source = this.target.shift(1, -1);
        return this.detectMove(this.king, source);
    }

    boolean isKingMoveFromBelow() {
        Position source = this.target.shift(0, -1);
        return this.detectMove(this.king, source);
    }

    boolean isKingMoveFromDownLeft() {
        Position source = this.target.shift(-1, -1);
        return this.detectMove(this.king, source);
    }
}
