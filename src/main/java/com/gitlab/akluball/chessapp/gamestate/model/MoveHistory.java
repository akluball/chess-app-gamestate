package com.gitlab.akluball.chessapp.gamestate.model;

import com.gitlab.akluball.chessapp.gamestate.transfer.GameStateDto;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.OrderColumn;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.isEven;

@Embeddable
@Access(AccessType.FIELD)
public class MoveHistory {
    @ElementCollection
    @OrderColumn
    private List<MoveSummary> moveSummaries = new ArrayList<>();

    void pushMoveSummary(MoveSummary moveSummary) {
        this.moveSummaries.add(moveSummary);
    }

    boolean isWhiteSideTurn() {
        return isEven(this.moveSummaries.size());
    }

    private Stream<MoveSummary> whiteSideMoveSummaries() {
        Alternator alternator = Alternator.start(true);
        return this.moveSummaries.stream()
                .filter(alternator::next);
    }

    private Stream<MoveSummary> blackSideMoveSummaries() {
        Alternator alternator = Alternator.start(false);
        return this.moveSummaries.stream()
                .filter(alternator::next);
    }

    private Stream<MoveSummary> moveSummariesForSide(boolean isWhiteSide) {
        return (isWhiteSide)
                ? this.whiteSideMoveSummaries()
                : this.blackSideMoveSummaries();
    }

    boolean isEnPassantVulnerable(Position position) {
        if (moveSummaries.isEmpty()) {
            return false;
        }
        if (!moveSummaries.get(moveSummaries.size() - 1).isPawnAdvance(position)) {
            return false;
        }
        boolean isWhiteSideTurn = this.isWhiteSideTurn();
        Position skippedPosition = position.rankShift(isWhiteSideTurn ? 1 : -1);
        return this.moveSummariesForSide(!isWhiteSideTurn)
                .noneMatch(moveSummary -> moveSummary.isPawnAdvance(skippedPosition));
    }

    boolean hasKingMoved(boolean isWhiteSide) {
        return this.moveSummariesForSide(isWhiteSide)
                .anyMatch(MoveSummary::isKingMove);
    }

    boolean hasRookMovedToPosition(boolean isWhite, Position position) {
        return this.moveSummariesForSide(isWhite)
                .anyMatch(moveSummary -> moveSummary.isRookMovingTo(position));
    }

    boolean isEnded() {
        return !this.moveSummaries.isEmpty()
                && this.moveSummaries.get(this.moveSummaries.size() - 1).isLastMove();
    }

    void loadTags(EndedGame.Builder endedGameBuilder) {
        if (!this.moveSummaries.isEmpty()) {
            this.moveSummaries.get(this.moveSummaries.size() - 1)
                    .loadTags(endedGameBuilder);
        }
    }

    void loadMoveSummaries(EndedGame.Builder endedGameBuilder) {
        endedGameBuilder.moveSummaries(this.moveSummaries);
    }

    public void loadMoveSummaries(GameStateDto.Builder gameStateDtoBuilder) {
        gameStateDtoBuilder.moveSummaries(this.moveSummaries);
    }

    private static class Alternator {
        private boolean currentValue;

        private static Alternator start(boolean startValue) {
            Alternator alternator = new Alternator();
            alternator.currentValue = startValue;
            return alternator;
        }

        private <T> boolean next(T t) { // ignore param, just want to use as method reference
            boolean next = this.currentValue;
            this.currentValue = !next;
            return next;
        }
    }
}
