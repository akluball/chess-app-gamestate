package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.setOf;

public class MoveSourceDetector {
    private final Board board;
    private final Piece piece;
    private final Position target;
    private final Set<MoveUnit> moveUnits;

    MoveSourceDetector(Board board, Piece piece, Position target) {
        this.board = board;
        this.piece = piece;
        this.target = target;
        this.moveUnits = computeMoveUnits(piece);
    }

    static Set<MoveUnit> computeMoveUnits(Piece piece) {
        switch (piece) {
            case WHITE_ROOK:
            case BLACK_ROOK:
                return setOf(
                        MoveUnit.RIGHT,
                        MoveUnit.LEFT,
                        MoveUnit.UP,
                        MoveUnit.DOWN
                );
            case WHITE_BISHOP:
            case BLACK_BISHOP:
                return setOf(
                        MoveUnit.UP_LEFT_DIAGONAL,
                        MoveUnit.UP_RIGHT_DIAGONAL,
                        MoveUnit.DOWN_RIGHT_DIAGONAL,
                        MoveUnit.DOWN_LEFT_DIAGONAL
                );
            case WHITE_QUEEN:
            case BLACK_QUEEN:
                return setOf(
                        MoveUnit.RIGHT,
                        MoveUnit.LEFT,
                        MoveUnit.UP,
                        MoveUnit.DOWN,
                        MoveUnit.UP_LEFT_DIAGONAL,
                        MoveUnit.UP_RIGHT_DIAGONAL,
                        MoveUnit.DOWN_RIGHT_DIAGONAL,
                        MoveUnit.DOWN_LEFT_DIAGONAL
                );
            case WHITE_KNIGHT:
            case BLACK_KNIGHT:
                return setOf(
                        MoveUnit.LEFT_LEFT_UP,
                        MoveUnit.UP_UP_LEFT,
                        MoveUnit.UP_UP_RIGHT,
                        MoveUnit.RIGHT_RIGHT_UP,
                        MoveUnit.RIGHT_RIGHT_DOWN,
                        MoveUnit.DOWN_DOWN_RIGHT,
                        MoveUnit.DOWN_DOWN_LEFT,
                        MoveUnit.LEFT_LEFT_DOWN
                );
            default:
                return Collections.emptySet();
        }
    }

    private Position findSource(MoveUnit moveUnit) {
        MoveUnit reversed = moveUnit.reverse();
        Position possibleSource = this.target.shift(reversed.fileShift(), reversed.rankShift());
        if (!moveUnit.scales()) {
            if (this.board.isPieceAtPosition(this.piece, possibleSource)) {
                return possibleSource;
            }
        } else {
            while (possibleSource != null) {
                if (this.board.isPieceAtPosition(this.piece, possibleSource)) {
                    return possibleSource;
                } else if (this.board.isOccupiedPosition(possibleSource)) {
                    break;
                }
                possibleSource = possibleSource.shift(reversed.fileShift(), reversed.rankShift());
            }
        }
        return null;
    }

    Set<Position> getSources() {
        return this.moveUnits.stream()
                .map(this::findSource)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }
}
