package com.gitlab.akluball.chessapp.gamestate.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class MoveSummary {
    private String pieceLetter;
    private String promotionPieceLetter;
    @Enumerated(EnumType.STRING)
    private Position source;
    @Enumerated(EnumType.STRING)
    private Position target;
    private boolean isCapture;
    @Enumerated(EnumType.STRING)
    private SourceDisambiguation sourceDisambiguation;
    private long durationMillis;
    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    boolean isPawnAdvance(Position position) {
        return this.target.equals(position);
    }

    boolean isKingMove() {
        return this.pieceLetter.equals("K");
    }

    boolean isRookMovingTo(Position position) {
        return this.pieceLetter.equals("R") && this.target.equals(position);
    }

    boolean isLastMove() {
        return Objects.nonNull(this.gameStatus) && this.gameStatus.isEndedGame();
    }

    void loadTags(EndedGame.Builder endedGameBuilder) {
        switch (this.gameStatus) {
            case WHITE_IN_CHECKMATE:
                endedGameBuilder.resultTag(ResultTag.BLACK_WINS).terminationTag(TerminationTag.NORMAL);
                break;
            case BLACK_IN_CHECKMATE:
                endedGameBuilder.resultTag(ResultTag.WHITE_WINS).terminationTag(TerminationTag.NORMAL);
                break;
            case WHITE_TIME_EXPIRED:
                endedGameBuilder.resultTag(ResultTag.BLACK_WINS).terminationTag(TerminationTag.TIME_FORFEIT);
                break;
            case BLACK_TIME_EXPIRED:
                endedGameBuilder.resultTag(ResultTag.WHITE_WINS).terminationTag(TerminationTag.TIME_FORFEIT);
                break;
            case DRAW:
                endedGameBuilder.resultTag(ResultTag.DRAW).terminationTag(TerminationTag.NORMAL);
                break;
        }
    }

    public Dto toDto() {
        return this.new Dto();
    }

    static class Builder {
        private final MoveSummary moveSummary;

        Builder() {
            this.moveSummary = new MoveSummary();
        }

        Builder piece(Piece piece) {
            this.moveSummary.pieceLetter = piece.getPieceLetter();
            return this;
        }

        Builder promotionPiece(Piece piece) {
            if (Objects.nonNull(piece)) {
                this.moveSummary.promotionPieceLetter = piece.getPieceLetter();
            }
            return this;
        }

        Builder from(Position source) {
            this.moveSummary.source = source;
            return this;
        }

        Builder to(Position target) {
            this.moveSummary.target = target;
            return this;
        }

        Builder isCapture(boolean isCapture) {
            this.moveSummary.isCapture = isCapture;
            return this;
        }

        Builder sourceDisambiguation(SourceDisambiguation sourceDisambiguation) {
            this.moveSummary.sourceDisambiguation = sourceDisambiguation;
            return this;
        }

        Builder durationMillis(long millis) {
            this.moveSummary.durationMillis = millis;
            return this;
        }

        Builder gameStatus(GameStatus gameStatus) {
            this.moveSummary.gameStatus = gameStatus;
            return this;
        }

        MoveSummary build() {
            return this.moveSummary;
        }
    }

    @Schema(name = "MoveSummary")
    public class Dto {
        private String pieceLetter;
        private String promotionPieceLetter;
        private Position source;
        private Position target;
        private boolean isCapture;
        private SourceDisambiguation sourceDisambiguation;
        private long durationMillis;
        private GameStatus gameStatus;

        private Dto() {
            MoveSummary moveSummary = MoveSummary.this;
            this.pieceLetter = moveSummary.pieceLetter;
            this.promotionPieceLetter = moveSummary.promotionPieceLetter;
            this.source = moveSummary.source;
            this.target = moveSummary.target;
            this.isCapture = moveSummary.isCapture;
            this.sourceDisambiguation = moveSummary.sourceDisambiguation;
            this.durationMillis = moveSummary.durationMillis;
            this.gameStatus = moveSummary.gameStatus;
        }

        public boolean isEndedGame() {
            return this.gameStatus.isEndedGame();
        }
    }
}
