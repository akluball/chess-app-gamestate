package com.gitlab.akluball.chessapp.gamestate.model;

import com.gitlab.akluball.chessapp.gamestate.transfer.GameStateDto;
import com.gitlab.akluball.chessapp.gamestate.transfer.MoveResultDto;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Embeddable
@Access(AccessType.FIELD)
public class MoveTimer {
    @Embedded
    @AttributeOverride(name = "epochSecond", column = @Column(name = "moveStartEpochSecond"))
    @AttributeOverride(name = "nanoComponent", column = @Column(name = "moveStartNanoComponent"))
    private PersistableInstant moveStart;
    private long whiteSideMillis;
    private long blackSideMillis;

    private long currentMoveMillis() {
        return this.moveStart.toInstant().until(Instant.now(), ChronoUnit.MILLIS);
    }

    private long whiteSideMillisRemaining(boolean isWhiteSideTurn) {
        if (isWhiteSideTurn) {
            return this.whiteSideMillis - this.currentMoveMillis();
        } else {
            return this.whiteSideMillis;
        }
    }

    private long blackSideMillisRemaining(boolean isWhiteSideTurn) {
        if (isWhiteSideTurn) {
            return this.blackSideMillis;
        } else {
            return this.blackSideMillis - this.currentMoveMillis();
        }
    }

    public MoveEnd createWhiteMoveEnd() {
        return new MoveEnd(this.moveStart, this.whiteSideMillis);
    }

    public MoveEnd createBlackMoveEnd() {
        return new MoveEnd(this.moveStart, this.blackSideMillis);
    }

    public void commitWhiteMoveEnd(MoveEnd moveEnd) {
        this.moveStart = PersistableInstant.from(moveEnd.moveEnd);
        this.whiteSideMillis = moveEnd.millisAtEnd;
    }

    public void commitBlackMoveEnd(MoveEnd moveEnd) {
        this.moveStart = PersistableInstant.from(moveEnd.moveEnd);
        this.blackSideMillis = moveEnd.millisAtEnd;
    }

    boolean isExpired(boolean isWhiteSideMove) {
        long millisRemaining = ((isWhiteSideMove) ? this.whiteSideMillis : this.blackSideMillis) - this
                .currentMoveMillis();
        return millisRemaining < 1;
    }

    public void loadMillisRemaining(GameStateDto.Builder gameStateDtoBuilder, boolean isWhiteSideTurn) {
        gameStateDtoBuilder.whiteSideMillisRemaining(this.whiteSideMillisRemaining(isWhiteSideTurn))
                .blackSideMillisRemaining(this.blackSideMillisRemaining(isWhiteSideTurn));
    }

    public void loadMillisRemaining(MoveResultDto.Builder moveResultDtoBuilder, boolean isWhiteSideTurn) {
        moveResultDtoBuilder.whiteSideMillisRemaining(this.whiteSideMillisRemaining(isWhiteSideTurn))
                .blackSideMillisRemaining(this.blackSideMillisRemaining(isWhiteSideTurn));
    }

    static class Builder {
        private final MoveTimer moveTimer;

        Builder() {
            this.moveTimer = new MoveTimer();
        }

        Builder millisPerPlayer(long millis) {
            this.moveTimer.whiteSideMillis = millis;
            this.moveTimer.blackSideMillis = millis;
            return this;
        }

        Builder startNow() {
            this.moveTimer.moveStart = PersistableInstant.now();
            return this;
        }

        MoveTimer build() {
            return this.moveTimer;
        }
    }

    static class MoveEnd {
        private final Instant moveEnd;
        private final long durationMillis;
        private final long millisAtEnd;

        private MoveEnd(PersistableInstant moveStart, long millisAtStart) {
            this.moveEnd = Instant.now();
            this.durationMillis = moveStart.toInstant().until(this.moveEnd, ChronoUnit.MILLIS);
            this.millisAtEnd = millisAtStart - this.durationMillis;
        }

        boolean isExpired() {
            return this.millisAtEnd < 1;
        }

        long getDurationMillis() {
            return this.durationMillis;
        }
    }
}
