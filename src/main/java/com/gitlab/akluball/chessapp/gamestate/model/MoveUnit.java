package com.gitlab.akluball.chessapp.gamestate.model;

public class MoveUnit {
    static final MoveUnit RIGHT = new MoveUnit(1, 0, true);
    static final MoveUnit LEFT = new MoveUnit(-1, 0, true);
    static final MoveUnit UP = new MoveUnit(0, 1, true);
    static final MoveUnit DOWN = new MoveUnit(0, -1, true);

    static final MoveUnit UP_LEFT_DIAGONAL = new MoveUnit(-1, 1, true);
    static final MoveUnit UP_RIGHT_DIAGONAL = new MoveUnit(1, 1, true);
    static final MoveUnit DOWN_RIGHT_DIAGONAL = new MoveUnit(1, -1, true);
    static final MoveUnit DOWN_LEFT_DIAGONAL = new MoveUnit(-1, -1, true);

    static final MoveUnit LEFT_LEFT_UP = new MoveUnit(-2, 1, false);
    static final MoveUnit UP_UP_LEFT = new MoveUnit(-1, 2, false);
    static final MoveUnit UP_UP_RIGHT = new MoveUnit(1, 2, false);
    static final MoveUnit RIGHT_RIGHT_UP = new MoveUnit(2, 1, false);
    static final MoveUnit RIGHT_RIGHT_DOWN = new MoveUnit(2, -1, false);
    static final MoveUnit DOWN_DOWN_RIGHT = new MoveUnit(1, -2, false);
    static final MoveUnit DOWN_DOWN_LEFT = new MoveUnit(-1, -2, false);
    static final MoveUnit LEFT_LEFT_DOWN = new MoveUnit(-2, -1, false);

    private final int fileShift;
    private final int rankShift;
    private final boolean scales;

    private MoveUnit(int fileShift, int rankShift, boolean scales) {
        this.fileShift = fileShift;
        this.rankShift = rankShift;
        this.scales = scales;
    }

    MoveUnit reverse() {
        return new ReverseMoveUnit(this);
    }

    int fileShift() {
        return this.fileShift;
    }

    int rankShift() {
        return this.rankShift;
    }

    boolean scales() {
        return this.scales;
    }

    static class ReverseMoveUnit extends MoveUnit {
        ReverseMoveUnit(MoveUnit moveUnit) {
            super(-1 * moveUnit.fileShift, -1 * moveUnit.rankShift, moveUnit.scales);
        }
    }
}
