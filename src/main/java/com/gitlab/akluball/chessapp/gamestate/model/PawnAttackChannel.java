package com.gitlab.akluball.chessapp.gamestate.model;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.BLACK_PAWN;
import static com.gitlab.akluball.chessapp.gamestate.model.Piece.WHITE_PAWN;

public class PawnAttackChannel extends AttackChannel {
    private final Board board;
    private final MoveHistory moveHistory;
    private final Position attackSource;
    private final Piece opponentPawn;
    private final Position enPassantLeftSource;
    private final Position enPassantRightSource;

    PawnAttackChannel(Board board, MoveHistory moveHistory, boolean isWhiteSideAttacking, Position attackSource) {
        super(board, isWhiteSideAttacking, attackSource);
        this.board = board;
        this.moveHistory = moveHistory;
        this.attackSource = attackSource;
        this.opponentPawn = (isWhiteSideAttacking) ? BLACK_PAWN : WHITE_PAWN;
        this.enPassantLeftSource = attackSource.fileShift(-1);
        this.enPassantRightSource = attackSource.fileShift(1);
    }

    private boolean isBlockableByEnPassant() {
        return this.moveHistory.isEnPassantVulnerable(this.attackSource)
                && (this.board.isPieceAtPosition(this.opponentPawn, this.enPassantRightSource)
                || this.board.isPieceAtPosition(this.opponentPawn, this.enPassantLeftSource));
    }

    @Override
    boolean isBlockable() {
        return super.isBlockable() || this.isBlockableByEnPassant();
    }
}
