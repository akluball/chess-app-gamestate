package com.gitlab.akluball.chessapp.gamestate.model;

abstract class PawnEnPassantMove extends Move {
    private final Position possibleEnPassantCapture;
    private final Board board;
    private final MoveHistory moveHistory;
    private final Position source;

    PawnEnPassantMove(Board board, MoveHistory moveHistory, Piece piece, Position source, Position target) {
        super(board, piece, source, target);
        this.board = board;
        this.moveHistory = moveHistory;
        this.source = source;
        this.possibleEnPassantCapture = source.fileShift(super.computeFileDelta());
    }

    abstract Piece opponentPawn();

    abstract int advanceOneUnit();

    abstract int captureRank();

    private boolean isDiagonalAdvanceOne() {
        return (Math.abs(super.computeFileDelta()) == 1)
                && (super.computeRankDelta() == this.advanceOneUnit());
    }

    @Override
    boolean isValidPieceSpecific() {
        return this.isDiagonalAdvanceOne()
                && this.source.hasRank(this.captureRank())
                && this.board.isPieceAtPosition(this.opponentPawn(), this.possibleEnPassantCapture)
                && this.moveHistory.isEnPassantVulnerable(this.possibleEnPassantCapture);
    }

    @Override
    void place(Board.Builder boardBuilder, Position position, Piece piece) {
        if (!this.possibleEnPassantCapture.equals(position)) {
            super.place(boardBuilder, position, piece);
        }
    }

    @Override
    boolean isCaptureMove() {
        return true;
    }

    @Override
    SourceDisambiguation sourceDisambiguation() {
        return SourceDisambiguation.FILE;
    }
}
