package com.gitlab.akluball.chessapp.gamestate.model;

abstract class PawnMove extends Move {
    private final Board board;
    private final Position source;
    private final Position target;

    PawnMove(Board board, Piece piece, Position source, Position target) {
        super(board, piece, source, target);
        this.board = board;
        this.source = source;
        this.target = target;
    }

    abstract int advanceOneUnit();

    abstract int startingRank();

    @Override
    boolean isValidPieceSpecific() {
        int fileDelta = super.computeFileDelta();
        int rankDelta = super.computeRankDelta();
        if (Math.abs(fileDelta) == 1) {
            return (rankDelta == this.advanceOneUnit()) && this.board.isOpponentAtTarget(this);
        } else if (fileDelta == 0) {
            if (rankDelta == (2 * this.advanceOneUnit())) {
                return this.source.hasRank(this.startingRank())
                        && this.board.isVacantPosition(this.source.rankShift(this.advanceOneUnit()))
                        && this.board.isVacantPosition(this.target);
            } else if (rankDelta == this.advanceOneUnit()) {
                return this.board.isVacantPosition(this.target);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    SourceDisambiguation sourceDisambiguation() {
        return (Math.abs(this.computeFileDelta()) == 1) ? SourceDisambiguation.FILE : SourceDisambiguation.NONE;
    }
}
