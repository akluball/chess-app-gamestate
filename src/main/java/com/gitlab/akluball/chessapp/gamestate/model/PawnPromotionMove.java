package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Objects;

abstract class PawnPromotionMove extends PawnMove {
    private final Position source;
    private final Position target;

    PawnPromotionMove(Board board, Piece piece, Position source, Position target) {
        super(board, piece, source, target);
        this.source = source;
        this.target = target;
    }

    abstract Piece pawn();

    abstract Piece king();

    @Override
    boolean isValidPieceSpecific() {
        Piece promotionPiece = this.promotionPiece();
        return Objects.nonNull(promotionPiece)
                && !this.pawn().equals(promotionPiece)
                && !this.king().equals(promotionPiece)
                && super.isValidPieceSpecific();
    }

    @Override
    void place(Board.Builder boardBuilder, Position position, Piece piece) {
        if (this.source.equals(position) && this.pawn().equals(piece)) {
            boardBuilder.place(this.target, this.promotionPiece());
        } else {
            super.place(boardBuilder, position, piece);
        }
    }
}
