package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Set;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.setOf;

public abstract class PawnTargetStrategy implements TargetStrategy {
    abstract int advanceRankDeltaUnit();

    @Override
    public Set<Position> getTargets(Position source) {
        int advanceRankDeltaUnit = this.advanceRankDeltaUnit();
        return setOf(
                source.shift(0, advanceRankDeltaUnit),
                source.shift(-1, advanceRankDeltaUnit),
                source.shift(1, advanceRankDeltaUnit)
        );
    }
}
