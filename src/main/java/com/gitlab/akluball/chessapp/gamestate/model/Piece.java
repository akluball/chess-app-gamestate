package com.gitlab.akluball.chessapp.gamestate.model;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;
import java.util.HashMap;

@JsonbTypeAdapter(Piece.Adapter.class)
public enum Piece {
    WHITE_PAWN, WHITE_ROOK, WHITE_KNIGHT, WHITE_BISHOP, WHITE_QUEEN, WHITE_KING,
    BLACK_PAWN, BLACK_ROOK, BLACK_KNIGHT, BLACK_BISHOP, BLACK_QUEEN, BLACK_KING;

    private static final HashMap<String, Piece> STRING_REPR_TO_PIECE;

    static {
        STRING_REPR_TO_PIECE = new HashMap<String, Piece>();
        for (Piece piece : Piece.values()) {
            String stringRepr = String
                    .format("%s%s", String.valueOf(piece.name().charAt(0)).toLowerCase(), piece.pieceLetter);
            STRING_REPR_TO_PIECE.put(stringRepr, piece);
        }
    }

    private final boolean isWhiteSidePiece;
    private final String pieceLetter;

    Piece() {
        String name = this.name();
        this.isWhiteSidePiece = name.startsWith("WHITE");
        this.pieceLetter = (name.endsWith("KNIGHT")) ? "N" : String.valueOf(name.charAt(6));
    }

    static Piece fromStringRepr(String stringRepr) {
        return STRING_REPR_TO_PIECE.get(stringRepr);
    }

    public String toStringRepr() {
        return String.format("%s%s", (this.isWhiteSidePiece) ? "w" : "b", this.pieceLetter);
    }

    String getPieceLetter() {
        return this.pieceLetter;
    }

    boolean isWhiteSidePiece() {
        return this.isWhiteSidePiece;
    }

    boolean isTeammate(Piece piece) {
        return this.isWhiteSidePiece == piece.isWhiteSidePiece;
    }

    boolean isOpponent(Piece piece) {
        return !this.isTeammate(piece);
    }

    public static class Adapter implements JsonbAdapter<Piece, String> {
        @Override
        public String adaptToJson(Piece piece) throws Exception {
            return piece.toStringRepr();
        }

        @Override
        public Piece adaptFromJson(String asString) throws Exception {
            return Piece.fromStringRepr(asString);
        }
    }
}
