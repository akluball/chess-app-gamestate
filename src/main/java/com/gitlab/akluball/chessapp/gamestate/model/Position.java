package com.gitlab.akluball.chessapp.gamestate.model;

import com.gitlab.akluball.chessapp.gamestate.exception.Http500;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.isEven;

@JsonbTypeAdapter(Position.Adapter.class)
public enum Position {
    A8, B8, C8, D8, E8, F8, G8, H8,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A1, B1, C1, D1, E1, F1, G1, H1;

    private final int fileAsInt;
    private final int rankAsInt;

    Position() {
        this.fileAsInt = this.name().charAt(0) - 'A' + 1;
        this.rankAsInt = this.name().charAt(1) - '1' + 1;
    }

    static Position fromMovetextRepr(String stringRepr) {
        try {
            return Position.valueOf(stringRepr.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    static int computeFileDelta(Position source, Position target) {
        return target.fileAsInt - source.fileAsInt;
    }

    static int computeRankDelta(Position source, Position target) {
        return target.rankAsInt - source.rankAsInt;
    }

    public String toMovetextRepr() {
        return this.name().toLowerCase();
    }

    boolean hasRank(int rank) {
        return this.name().endsWith("" + rank);
    }

    boolean isSameColorSquareAs(Position position) {
        return isEven(this.fileAsInt + this.rankAsInt) == isEven(position.fileAsInt + position.rankAsInt);
    }

    Position shift(int fileShift, int rankShift) {
        char fileAsChar = (char) ('A' + this.fileAsInt + fileShift - 1);
        return fromMovetextRepr(String.format("%s%s", fileAsChar, this.rankAsInt + rankShift));
    }

    Position fileShift(int fileShift) {
        return shift(fileShift, 0);
    }

    Position rankShift(int rankShift) {
        return shift(0, rankShift);
    }

    boolean hasSameFile(Position other) {
        return this.fileAsInt == other.fileAsInt;
    }

    boolean hasSameRank(Position other) {
        return this.rankAsInt == other.rankAsInt;
    }

    static class Channel {
        private final Position start;
        private final Position end;
        private final int fileShift;
        private final int rankShift;

        private Channel(Position start, Position end, int fileShift, int rankShift) {
            this.start = start;
            this.end = end;
            this.fileShift = fileShift;
            this.rankShift = rankShift;
        }

        static Channel horizontalBetweenExclusive(Position start, Position end) {
            if (start.rankAsInt != end.rankAsInt) {
                throw new Http500(String.format("invalid horizontal channel endpoints: %s, %s", start, end));
            }
            int fileShift = (start.fileAsInt < end.fileAsInt) ? 1 : -1;
            return new Channel(start, end, fileShift, 0);
        }

        static Channel verticalBetweenExclusive(Position start, Position end) {
            if (start.fileAsInt != end.fileAsInt) {
                throw new Http500(String.format("invalid vertical channel endpoints: %s, %s", start, end));
            }
            int rankShift = (start.rankAsInt < end.rankAsInt) ? 1 : -1;
            return new Channel(start, end, 0, rankShift);
        }

        static Channel diagonalBetweenExclusive(Position start, Position end) {
            int fileDelta = Position.computeFileDelta(start, end);
            int rankDelta = Position.computeRankDelta(start, end);
            if (Math.abs(fileDelta) != Math.abs(rankDelta)) {
                throw new Http500(String.format("invalid diagonal channel endpoints: %s, %s", start, end));
            }
            int fileShift = (start.fileAsInt < end.fileAsInt) ? 1 : -1;
            int rankShift = (start.rankAsInt < end.rankAsInt) ? 1 : -1;
            return new Channel(start, end, fileShift, rankShift);
        }

        boolean isClear(Board board) {
            if (this.start.equals(this.end)) {
                return true;
            }
            Position currentPosition = this.start.shift(this.fileShift, this.rankShift);
            while (!currentPosition.equals(this.end)) {
                if (board.isOccupiedPosition(currentPosition)) {
                    return false;
                }
                currentPosition = currentPosition.shift(this.fileShift, this.rankShift);
            }
            return true;
        }
    }

    public static class Adapter implements JsonbAdapter<Position, String> {
        @Override
        public String adaptToJson(Position position) throws Exception {
            return position.toMovetextRepr();
        }

        @Override
        public Position adaptFromJson(String stringRepr) throws Exception {
            return Position.fromMovetextRepr(stringRepr);
        }
    }
}
