package com.gitlab.akluball.chessapp.gamestate.model;

public class QueenMove extends Move {
    private final Board board;
    private final Position source;
    private final Position target;

    QueenMove(Board board, Piece piece, Position source, Position target) {
        super(board, piece, source, target);
        this.board = board;
        this.source = source;
        this.target = target;
    }

    @Override
    boolean isValidPieceSpecific() {
        int rankDelta = super.computeRankDelta();
        int fileDelta = super.computeFileDelta();
        if (rankDelta == 0) {
            return Position.Channel.horizontalBetweenExclusive(this.source, this.target)
                    .isClear(this.board);
        }
        if (fileDelta == 0) {
            return Position.Channel.verticalBetweenExclusive(this.source, this.target)
                    .isClear(this.board);
        }
        if (Math.abs(fileDelta) == Math.abs(rankDelta)) {
            return Position.Channel.diagonalBetweenExclusive(this.source, this.target)
                    .isClear(this.board);
        }
        return false;
    }
}
