package com.gitlab.akluball.chessapp.gamestate.model;

public class RookMove extends Move {
    private final Board board;
    private final Position source;
    private final Position target;

    RookMove(Board board, Piece piece, Position source, Position target) {
        super(board, piece, source, target);
        this.board = board;
        this.source = source;
        this.target = target;
    }

    @Override
    boolean isValidPieceSpecific() {
        if (super.computeRankDelta() == 0) {
            return Position.Channel.horizontalBetweenExclusive(this.source, this.target)
                    .isClear(this.board);
        }
        if (super.computeFileDelta() == 0) {
            return Position.Channel.verticalBetweenExclusive(this.source, this.target)
                    .isClear(this.board);
        }
        return false;
    }
}
