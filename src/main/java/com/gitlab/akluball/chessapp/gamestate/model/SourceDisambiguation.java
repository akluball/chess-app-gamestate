package com.gitlab.akluball.chessapp.gamestate.model;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

@JsonbTypeAdapter(SourceDisambiguation.Adapter.class)
public enum SourceDisambiguation {
    NONE, FILE, RANK, BOTH;

    public static class Adapter implements JsonbAdapter<SourceDisambiguation, String> {
        @Override
        public String adaptToJson(SourceDisambiguation sourceDisambiguation) throws Exception {
            return sourceDisambiguation.name().toLowerCase();
        }

        @Override
        public SourceDisambiguation adaptFromJson(String asString) throws Exception {
            return SourceDisambiguation.valueOf(asString.toUpperCase());
        }
    }
}
