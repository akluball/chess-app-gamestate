package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Collections;
import java.util.Set;

public interface TargetStrategy {
    static TargetStrategy create(Piece piece) {
        switch (piece) {
            case WHITE_KING:
            case BLACK_KING:
            case WHITE_QUEEN:
            case BLACK_QUEEN:
                return new AdjacentTargetStrategy();
            case WHITE_ROOK:
            case BLACK_ROOK:
                return new CardinalDirectionTargetStrategy();
            case WHITE_BISHOP:
            case BLACK_BISHOP:
                return new DiagonalTargetStrategy();
            case WHITE_PAWN:
                return new WhitePawnTargetStrategy();
            case BLACK_PAWN:
                return new BlackPawnTargetStrategy();
            case WHITE_KNIGHT:
            case BLACK_KNIGHT:
                return new KnightTargetStrategy();
            default:
                return new NoTargetStrategy();
        }
    }

    Set<Position> getTargets(Position source);

    class NoTargetStrategy implements TargetStrategy {
        @Override
        public Set<Position> getTargets(Position source) {
            return Collections.emptySet();
        }
    }
}
