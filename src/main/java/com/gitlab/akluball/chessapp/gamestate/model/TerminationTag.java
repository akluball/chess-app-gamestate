package com.gitlab.akluball.chessapp.gamestate.model;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

@JsonbTypeAdapter(TerminationTag.Adapter.class)
public enum TerminationTag {
    UNTERMINATED,
    NORMAL,
    TIME_FORFEIT;

    public static class Adapter implements JsonbAdapter<TerminationTag, String> {
        @Override
        public String adaptToJson(TerminationTag terminationTag) throws Exception {
            return terminationTag.name().toLowerCase().replace("_", "-");
        }

        @Override
        public TerminationTag adaptFromJson(String asString) throws Exception {
            return TerminationTag.valueOf(asString.replace("-", "_").toUpperCase());
        }
    }
}
