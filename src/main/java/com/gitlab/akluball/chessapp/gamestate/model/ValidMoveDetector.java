package com.gitlab.akluball.chessapp.gamestate.model;

import java.util.Objects;

class ValidMoveDetector {
    private final Board board;
    private final Piece piece;
    private final Position source;
    private final TargetStrategy targetStrategy;
    private MoveHistory moveHistory;

    ValidMoveDetector(Board board, Piece piece, Position source) {
        this.board = board;
        this.piece = piece;
        this.source = source;
        this.targetStrategy = TargetStrategy.create(piece);
    }

    ValidMoveDetector(Board board, MoveHistory moveHistory, Piece piece, Position source) {
        this.board = board;
        this.moveHistory = moveHistory;
        this.piece = piece;
        this.source = source;
        this.targetStrategy = TargetStrategy.create(piece);
    }

    Move createMove(Position target) {
        return Objects.isNull(this.moveHistory)
                ? Move.createMove(this.board, this.piece, this.source, target)
                : Move.createMove(this.board, this.moveHistory, this.piece, this.source, target, "Q");
    }

    boolean hasValidMove() {
        return this.targetStrategy.getTargets(this.source)
                .stream()
                .filter(Objects::nonNull)
                .map(this::createMove)
                .filter(Move::isValid)
                .map(Move::execute)
                .anyMatch(b -> !b.isInCheck(this.piece.isWhiteSidePiece()));
    }
}
