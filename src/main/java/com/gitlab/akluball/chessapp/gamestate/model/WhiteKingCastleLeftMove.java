package com.gitlab.akluball.chessapp.gamestate.model;

class WhiteKingCastleLeftMove extends CastlingMove {
    WhiteKingCastleLeftMove(Board board, MoveHistory moveHistory, Position source, Position target) {
        super(board, moveHistory, true, source, target);
    }

    @Override
    Position kingStart() {
        return Position.E1;
    }

    @Override
    Position kingIntermediate() {
        return Position.D1;
    }

    @Override
    Position kingEnd() {
        return Position.C1;
    }

    @Override
    Position rookStart() {
        return Position.A1;
    }

    @Override
    Position rookEnd() {
        return Position.D1;
    }
}
