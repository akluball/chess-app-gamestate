package com.gitlab.akluball.chessapp.gamestate.model;

class WhiteKingCastleRightMove extends CastlingMove {

    WhiteKingCastleRightMove(Board board, MoveHistory moveHistory, Position source, Position target) {
        super(board, moveHistory, true, source, target);
    }

    @Override
    Position kingStart() {
        return Position.E1;
    }

    @Override
    Position kingIntermediate() {
        return Position.F1;
    }

    @Override
    Position kingEnd() {
        return Position.G1;
    }

    @Override
    Position rookStart() {
        return Position.H1;
    }

    @Override
    Position rookEnd() {
        return Position.F1;
    }
}
