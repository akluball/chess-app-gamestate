package com.gitlab.akluball.chessapp.gamestate.model;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.BLACK_PAWN;
import static com.gitlab.akluball.chessapp.gamestate.model.Piece.WHITE_PAWN;

class WhitePawnEnPassantMove extends PawnEnPassantMove {

    WhitePawnEnPassantMove(Board board, MoveHistory moveHistory, Position source, Position target) {
        super(board, moveHistory, WHITE_PAWN, source, target);
    }

    @Override
    Piece opponentPawn() {
        return BLACK_PAWN;
    }

    @Override
    int advanceOneUnit() {
        return 1;
    }

    @Override
    int captureRank() {
        return 5;
    }
}
