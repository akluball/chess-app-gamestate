package com.gitlab.akluball.chessapp.gamestate.model;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.WHITE_PAWN;

class WhitePawnMove extends PawnMove {
    WhitePawnMove(Board board, Position source, Position target) {
        super(board, WHITE_PAWN, source, target);
    }

    static Move createMove(Board board,
            MoveHistory moveHistory,
            Position source,
            Position target,
            String promotionPieceLetter) {
        if ((Math.abs(Position.computeFileDelta(source, target)) == 1) && !board.isOpponentOfAt(WHITE_PAWN, target)) {
            return new WhitePawnEnPassantMove(board, moveHistory, source, target);
        } else if (target.hasRank(8)) {
            return new WhitePawnPromotionMove(board, source, target, promotionPieceLetter);
        } else {
            return new WhitePawnMove(board, source, target);
        }
    }

    @Override
    int advanceOneUnit() {
        return 1;
    }

    @Override
    int startingRank() {
        return 2;
    }
}
