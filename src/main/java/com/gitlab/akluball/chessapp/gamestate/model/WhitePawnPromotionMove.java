package com.gitlab.akluball.chessapp.gamestate.model;

import static com.gitlab.akluball.chessapp.gamestate.model.Piece.WHITE_KING;
import static com.gitlab.akluball.chessapp.gamestate.model.Piece.WHITE_PAWN;

class WhitePawnPromotionMove extends PawnPromotionMove {
    private final Piece promotionPiece;

    WhitePawnPromotionMove(Board board, Position source, Position target, String promotionPieceLetter) {
        super(board, WHITE_PAWN, source, target);
        this.promotionPiece = Piece.fromStringRepr(String.format("w%s", promotionPieceLetter));
    }

    @Override
    Piece promotionPiece() {
        return this.promotionPiece;
    }

    @Override
    Piece pawn() {
        return WHITE_PAWN;
    }

    @Override
    Piece king() {
        return WHITE_KING;
    }

    @Override
    int advanceOneUnit() {
        return 1;
    }

    @Override
    int startingRank() {
        return 7;
    }
}
