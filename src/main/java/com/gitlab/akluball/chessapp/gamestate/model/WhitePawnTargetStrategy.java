package com.gitlab.akluball.chessapp.gamestate.model;

public class WhitePawnTargetStrategy extends PawnTargetStrategy {
    @Override
    int advanceRankDeltaUnit() {
        return 1;
    }
}
