package com.gitlab.akluball.chessapp.gamestate.security;

import com.gitlab.akluball.chessapp.gamestate.GameStateConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class GameHistorySecurity implements TokenSignatureValidator {
    private RSASSAVerifier verifier;

    @Inject
    GameHistorySecurity(GameStateConfig gameStateConfig) {
        this.verifier = new RSASSAVerifier(gameStateConfig.gameHistoryPublicKey());
    }

    @Override
    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            return false;
        }
    }
}
