package com.gitlab.akluball.chessapp.gamestate.security;

import com.gitlab.akluball.chessapp.gamestate.GameStateConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.asRuntime;

@Singleton
public class GameStateSecurity {
    private final RSASSASigner signer;
    private final int secondsToExpiration;
    private SignedJWT token;
    private Instant expiration;

    @Inject
    GameStateSecurity(GameStateConfig gameStateConfig) {
        this.signer = new RSASSASigner(gameStateConfig.gameStatePrivateKey());
        this.secondsToExpiration = gameStateConfig.tokenLifeSeconds();
    }

    private boolean requiresRefresh() {
        return Objects.isNull(this.token) || this.expiration.isAfter(Instant.now().plusSeconds(5));
    }

    private void refreshToken() {
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        this.expiration = Instant.now().plusSeconds(this.secondsToExpiration);
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(ServiceName.GAMESTATE.asString())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.SERVICE.asString()))
                .expirationTime(Date.from(this.expiration))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        try {
            token.sign(this.signer);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        this.token = token;
    }

    public String getToken() {
        if (this.requiresRefresh()) {
            this.refreshToken();
        }
        return this.token.serialize();
    }
}
