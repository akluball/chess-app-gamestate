package com.gitlab.akluball.chessapp.gamestate.security;

import com.gitlab.akluball.chessapp.gamestate.exception.Http401;
import com.nimbusds.jwt.SignedJWT;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

public class SecurityUtil {
    public static final Pattern BEARER_TOKEN_QUERY_PATTERN = Pattern.compile("&?bearer=([^&]+)&?");

    public static String bearer(String serializedToken) {
        return String.format("Bearer %s", serializedToken);
    }

    public static SignedJWT parseSignedToken(String serializedToken) {
        try {
            return SignedJWT.parse(serializedToken);
        } catch (ParseException e) {
            throw new Http401();
        }
    }

    public static boolean isExpired(SignedJWT token) {
        try {
            Date expiration = token.getJWTClaimsSet().getExpirationTime();
            return Objects.isNull(expiration) || !expiration.toInstant().isAfter(Instant.now());
        } catch (ParseException e) {
            return true;
        }
    }

    public static boolean isNotExpired(SignedJWT token) {
        return !isExpired(token);
    }
}
