package com.gitlab.akluball.chessapp.gamestate.security;

import com.gitlab.akluball.chessapp.gamestate.GameStateConfig;
import com.gitlab.akluball.chessapp.gamestate.exception.Http401;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.ParseException;

@Singleton
public class UserSecurity implements TokenSignatureValidator {
    private RSASSAVerifier verifier;

    @Inject
    UserSecurity(GameStateConfig gameStateConfig) {
        this.verifier = new RSASSAVerifier(gameStateConfig.userPublicKey());
    }

    public boolean isUserRole(SignedJWT token) {
        try {
            return token.getJWTClaimsSet()
                    .getStringListClaim(BearerRole.CLAIM_NAME)
                    .contains(BearerRole.USER.asString());
        } catch (ParseException e) {
            return false;
        }
    }

    @Override
    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            return false;
        }
    }

    public int extractUserId(SignedJWT token) {
        try {
            return Integer.parseInt(token.getJWTClaimsSet().getSubject());
        } catch (ParseException e) {
            throw new Http401();
        }
    }
}
