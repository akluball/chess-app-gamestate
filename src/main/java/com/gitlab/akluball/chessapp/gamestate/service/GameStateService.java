package com.gitlab.akluball.chessapp.gamestate.service;

import com.gitlab.akluball.chessapp.gamestate.client.GameHistoryClient;
import com.gitlab.akluball.chessapp.gamestate.data.EndedGameDao;
import com.gitlab.akluball.chessapp.gamestate.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.exception.Http401;
import com.gitlab.akluball.chessapp.gamestate.exception.Http403;
import com.gitlab.akluball.chessapp.gamestate.exception.Http404;
import com.gitlab.akluball.chessapp.gamestate.model.EndedGame;
import com.gitlab.akluball.chessapp.gamestate.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.model.Move;
import com.gitlab.akluball.chessapp.gamestate.transfer.MoveResultDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Stateless
public class GameStateService {
    private GameStateDao gameStateDao;
    private EndedGameDao endedGameDao;
    private GameHistoryClient gameHistoryClient;

    public GameStateService() {
    }

    @Inject
    public GameStateService(GameStateDao gameStateDao, EndedGameDao endedGameDao, GameHistoryClient gameHistoryClient) {
        this.gameStateDao = gameStateDao;
        this.endedGameDao = endedGameDao;
        this.gameHistoryClient = gameHistoryClient;
    }

    public void create(
            @NotNull(message = "gameHistoryId is required") Integer gameHistoryId,
            @NotNull(message = "GameStateCreate is required") @Valid GameState.CreateDto createDto
    ) {
        GameState gameState = createDto.toGameState(gameHistoryId);
        this.gameStateDao.persist(gameState);
    }

    public GameState findByGameHistoryId(
            @NotNull(message = "gameHistoryId is required") Integer gameHistoryId,
            @NotNull(payload = Http401.Payload.class) Integer currentUserId
    ) {
        GameState gameState = this.findByGameHistoryId(gameHistoryId);
        if (!gameState.isParticipant(currentUserId)) {
            throw new Http403();
        }
        if (gameState.isCurrentTimerExpired()) {
            this.endGame(gameState);
            throw new Http404.NoRollback(String.format("GameState with gameHistoryId %s", gameHistoryId));
        }
        return gameState;
    }

    public GameState findByGameHistoryId(@NotNull(message = "gameHistoryId is required") Integer gameHistoryId) {
        GameState gameState = this.gameStateDao.findByGameHistoryId(gameHistoryId);
        if (Objects.isNull(gameState)) {
            throw new Http404(String.format("GameState with gameHistoryId %s", gameHistoryId));
        }
        return gameState;
    }

    public Stream<GameState> findByParticipant(
            @NotNull(message = "userId is required") Integer userId,
            @NotNull(message = "isTurn is required") Boolean isTurn
    ) {
        List<GameState> gameStates = (isTurn) ?
                this.gameStateDao.findByParticipantsTurn(userId) :
                this.gameStateDao.findByParticipant(userId);
        return gameStates.stream()
                .filter(gameState -> {
                    boolean isEnded = gameState.isEnded();
                    if (isEnded) {
                        this.endGame(gameState);
                    }
                    return !isEnded;
                });
    }

    public MoveResultDto attemptMove(
            @NotNull(message = "gameHistoryId is required") Integer gameHistoryId,
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "Move is required") Move.InDto moveDto) {
        return this.findByGameHistoryId(gameHistoryId)
                .attemptMove(currentUserId, moveDto);
    }

    public void endGame(@NotNull(message = "gameState is required") GameState gameState) {
        if (gameState.isEnded()) {
            EndedGame endedGame = gameState.toEndedGame();
            if (!this.gameHistoryClient.end(endedGame)) {
                this.endedGameDao.persist(endedGame);
            }
            this.gameStateDao.delete(gameState);
        }
    }

    public void endGame(@NotNull(message = "gameHistoryId is required") Integer gameHistoryId) {
        this.endGame(this.findByGameHistoryId(gameHistoryId));
    }
}
