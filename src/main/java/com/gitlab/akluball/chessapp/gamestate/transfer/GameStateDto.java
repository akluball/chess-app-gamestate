package com.gitlab.akluball.chessapp.gamestate.transfer;

import com.gitlab.akluball.chessapp.gamestate.model.Board;
import com.gitlab.akluball.chessapp.gamestate.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamestate.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.model.Piece;
import com.gitlab.akluball.chessapp.gamestate.model.Position;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Schema(name = "GameState")
public class GameStateDto {
    private int gameHistoryId;
    private int whiteSideUserId;
    private int blackSideUserId;
    private List<MoveSummary.Dto> moveSummaries;
    private Map<String, String> positionToPiece;
    private long whiteSideMillisRemaining;
    private long blackSideMillisRemaining;

    public static class Builder {
        private final GameStateDto gameStateDto;

        public Builder() {
            this.gameStateDto = new GameStateDto();
        }

        public Builder gameHistoryId(int gameHistoryId) {
            this.gameStateDto.gameHistoryId = gameHistoryId;
            return this;
        }

        public Builder whiteSideUserId(int whiteSideUserId) {
            this.gameStateDto.whiteSideUserId = whiteSideUserId;
            return this;
        }

        public Builder blackSideUserId(int blackSideUserId) {
            this.gameStateDto.blackSideUserId = blackSideUserId;
            return this;
        }

        public Builder moveSummaries(List<MoveSummary> moveSummaries) {
            this.gameStateDto.moveSummaries = moveSummaries.stream()
                    .map(MoveSummary::toDto)
                    .collect(Collectors.toList());
            return this;
        }

        public Builder moveHistory(MoveHistory moveHistory) {
            moveHistory.loadMoveSummaries(this);
            return this;
        }

        public Builder positionToPiece(Map<Position, Piece> positionToPiece) {
            HashMap<String, String> serialized = new HashMap<>();
            positionToPiece.forEach((position, piece) -> {
                serialized.put(position.toMovetextRepr(), piece.toStringRepr());
            });
            this.gameStateDto.positionToPiece = serialized;
            return this;
        }

        public Builder board(Board board) {
            board.loadPositionToPiece(this);
            return this;
        }

        public Builder whiteSideMillisRemaining(long millisRemaining) {
            this.gameStateDto.whiteSideMillisRemaining = millisRemaining;
            return this;
        }

        public Builder blackSideMillisRemaining(long millisRemaining) {
            this.gameStateDto.blackSideMillisRemaining = millisRemaining;
            return this;
        }

        public Builder moveTimer(MoveTimer moveTimer, boolean isWhiteSideTurn) {
            moveTimer.loadMillisRemaining(this, isWhiteSideTurn);
            return this;
        }

        public GameStateDto build() {
            return this.gameStateDto;
        }
    }
}
