package com.gitlab.akluball.chessapp.gamestate.transfer;

import com.gitlab.akluball.chessapp.gamestate.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamestate.model.MoveTimer;

import java.util.Objects;

public class MoveResultDto {
    private boolean wasExecuted;
    private long whiteSideMillisRemaining;
    private long blackSideMillisRemaining;
    private MoveSummary.Dto moveSummary;

    public boolean wasExecuted() {
        return this.wasExecuted;
    }

    public boolean isEndedGame() {
        return Objects.nonNull(this.moveSummary) && this.moveSummary.isEndedGame();
    }

    public static class Builder {
        private final MoveResultDto moveResultDto;

        public Builder() {
            this.moveResultDto = new MoveResultDto();
        }

        public Builder notExecuted() {
            this.moveResultDto.wasExecuted = false;
            return this;
        }

        public Builder executed() {
            this.moveResultDto.wasExecuted = true;
            return this;
        }

        public Builder moveSummary(MoveSummary moveSummary) {
            this.moveResultDto.moveSummary = moveSummary.toDto();
            return this;
        }

        public Builder whiteSideMillisRemaining(long millis) {
            this.moveResultDto.whiteSideMillisRemaining = millis;
            return this;
        }

        public Builder blackSideMillisRemaining(long millis) {
            this.moveResultDto.blackSideMillisRemaining = millis;
            return this;
        }

        public Builder loadMillisRemaining(MoveTimer moveTimer, boolean isWhiteSideTurn) {
            moveTimer.loadMillisRemaining(this, isWhiteSideTurn);
            return this;
        }

        public MoveResultDto build() {
            return this.moveResultDto;
        }
    }
}
