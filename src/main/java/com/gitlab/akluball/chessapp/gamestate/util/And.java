package com.gitlab.akluball.chessapp.gamestate.util;

import java.util.ArrayList;
import java.util.function.Supplier;

public class And {
    private final ArrayList<Supplier<Boolean>> clauses;

    public And() {
        this.clauses = new ArrayList<>();
    }

    public And clause(Supplier<Boolean> clause) {
        this.clauses.add(clause);
        return this;
    }

    public boolean evaluate() {
        return this.clauses.stream()
                .anyMatch(Supplier::get);
    }
}
