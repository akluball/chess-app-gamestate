package com.gitlab.akluball.chessapp.gamestate.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ShortCircuitCollector<T> {
    private final int limit;
    private List<Supplier<T>> suppliers;

    public ShortCircuitCollector(Class<T> klass, int limit) {
        this.limit = limit;
        this.suppliers = new ArrayList<>();
    }

    public ShortCircuitCollector<T> supply(Supplier<T> supplier) {
        this.suppliers.add(supplier);
        return this;
    }

    public Set<T> getCollected() {
        return this.suppliers.stream()
                .map(Supplier::get)
                .filter(Objects::nonNull)
                .limit(this.limit)
                .collect(Collectors.toSet());
    }
}
