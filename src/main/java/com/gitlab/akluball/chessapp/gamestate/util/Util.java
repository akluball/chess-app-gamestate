package com.gitlab.akluball.chessapp.gamestate.util;

import com.gitlab.akluball.chessapp.gamestate.exception.HttpException;

import javax.ws.rs.core.Response;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Util {
    public static boolean is2xx(Response response) {
        return Integer.toString(response.getStatus()).startsWith("2");
    }

    public static boolean isEven(int toCheck) {
        return (toCheck & 1) == 0;
    }

    public static int minutesToSeconds(int minutes) {
        return minutes * 60;
    }

    @SafeVarargs
    public static <T> Set<T> setOf(T... elements) {
        return Stream.of(elements).collect(Collectors.toSet());
    }

    @SafeVarargs
    public static <T> boolean setContainsExactly(Set<T> set, T... elements) {
        if (set.size() != elements.length) {
            return false;
        }
        for (T element : elements) {
            if (!set.contains(element)) {
                return false;
            }
        }
        return true;
    }

    public static RuntimeException asRuntime(Throwable throwable) {
        return new RuntimeException(throwable);
    }

    public static <T extends Throwable> T getExceptionCause(Throwable throwable, Class<T> exceptionType) {
        while (throwable != null) {
            if (exceptionType.isInstance(throwable)) {
                return exceptionType.cast(throwable);
            }
            throwable = throwable.getCause();
        }
        return null;
    }

    public static HttpException getHttpExceptionCause(Throwable throwable) {
        return getExceptionCause(throwable, HttpException.class);
    }
}
