package com.gitlab.akluball.chessapp.gamestate.websocket;

import com.gitlab.akluball.chessapp.gamestate.exception.Http401;
import com.gitlab.akluball.chessapp.gamestate.exception.Http403;
import com.gitlab.akluball.chessapp.gamestate.exception.HttpException;
import com.gitlab.akluball.chessapp.gamestate.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.model.Move;
import com.gitlab.akluball.chessapp.gamestate.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamestate.service.GameStateService;
import com.gitlab.akluball.chessapp.gamestate.transfer.MoveResultDto;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;

import static com.gitlab.akluball.chessapp.gamestate.security.SecurityUtil.*;
import static com.gitlab.akluball.chessapp.gamestate.util.Util.asRuntime;
import static com.gitlab.akluball.chessapp.gamestate.util.Util.getHttpExceptionCause;

@ServerEndpoint(
        value = "/api/gamestate/{gameHistoryId}",
        decoders = GameStateWebSocket.MoveDtoDecoder.class,
        encoders = GameStateWebSocket.MoveResultDtoEncoder.class
)
public class GameStateWebSocket {
    private static final String USER_ID_PROP = "isWhiteSideUser";

    private final UserSecurity userSecurity;
    private final GameStateService gameStateService;
    private final GameStateWebSocketMesh mesh;

    @Inject
    GameStateWebSocket(UserSecurity userSecurity, GameStateService gameStateService, GameStateWebSocketMesh mesh) {
        this.userSecurity = userSecurity;
        this.gameStateService = gameStateService;
        this.mesh = mesh;
    }

    @OnOpen
    public void connect(Session session, @PathParam("gameHistoryId") int gameHistoryId) {
        String queryString = session.getQueryString();
        queryString = Objects.isNull(queryString) ? "" : queryString;
        Matcher bearerMatcher = BEARER_TOKEN_QUERY_PATTERN.matcher(queryString);
        if (!bearerMatcher.find()) {
            throw new Http401();
        }
        SignedJWT token = parseSignedToken(bearerMatcher.group(1));
        if (isExpired(token)) {
            throw new Http401();
        }
        if (!this.userSecurity.isUserRole(token)) {
            throw new Http401();
        }
        if (!this.userSecurity.isValidSignature(token)) {
            throw new Http401();
        }
        GameState gameState = this.gameStateService.findByGameHistoryId(gameHistoryId);
        int userId = this.userSecurity.extractUserId(token);
        if (!gameState.isParticipant(userId)) {
            throw new Http403();
        }
        session.getUserProperties().put(USER_ID_PROP, userId);
        this.mesh.register(gameHistoryId, session);
    }

    @OnClose
    public void disconnect(Session session, @PathParam("gameHistoryId") int gameHistoryId) {
        this.mesh.unregister(gameHistoryId, session);
    }

    @OnError
    public void handleError(Session session, Throwable throwable) {
        HttpException httpException = getHttpExceptionCause(throwable);
        try {
            if (httpException != null) {
                session.close(new CloseReason(CloseCodes.UNEXPECTED_CONDITION, httpException.getReason()));
            } else {
                session.close(new CloseReason(CloseCodes.UNEXPECTED_CONDITION, "internal server error"));
                throw asRuntime(throwable);
            }
        } catch (IOException e) {
            throw asRuntime(e);
        }
    }

    @OnMessage
    public void handleMove(Session session, @PathParam("gameHistoryId") int gameHistoryId, Move.InDto moveDto) {
        int userId = (int) session.getUserProperties().get(USER_ID_PROP);
        MoveResultDto moveResultDto = this.gameStateService.attemptMove(gameHistoryId, userId, moveDto);
        if (moveResultDto.wasExecuted()) {
            this.mesh.broadcast(gameHistoryId, moveResultDto);
        } else {
            try {
                session.getBasicRemote().sendObject(moveResultDto);
            } catch (IOException | EncodeException e) {
                throw asRuntime(e);
            }
        }
        if (moveResultDto.isEndedGame()) {
            this.gameStateService.endGame(gameHistoryId);
        }
    }

    public static class MoveDtoDecoder implements Decoder.Text<Move.InDto> {
        private final Jsonb jsonb;

        @Inject
        MoveDtoDecoder(Jsonb jsonb) {
            this.jsonb = jsonb;
        }

        @Override
        public Move.InDto decode(String encoded) throws DecodeException {
            return this.jsonb.fromJson(encoded, Move.InDto.class);
        }

        @Override
        public boolean willDecode(String s) {
            return true;
        }

        @Override
        public void init(EndpointConfig config) {
        }

        @Override
        public void destroy() {
        }
    }

    public static class MoveResultDtoEncoder implements Encoder.Text<MoveResultDto> {
        private final Jsonb jsonb;

        @Inject
        MoveResultDtoEncoder(Jsonb jsonb) {
            this.jsonb = jsonb;
        }

        @Override
        public String encode(MoveResultDto moveResultDto) throws EncodeException {
            return this.jsonb.toJson(moveResultDto);
        }

        @Override
        public void init(EndpointConfig config) {
        }

        @Override
        public void destroy() {
        }
    }
}
