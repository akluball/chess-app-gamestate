package com.gitlab.akluball.chessapp.gamestate.websocket;

import com.gitlab.akluball.chessapp.gamestate.transfer.MoveResultDto;

import javax.inject.Singleton;
import javax.websocket.EncodeException;
import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.gitlab.akluball.chessapp.gamestate.util.Util.asRuntime;

@Singleton
public class GameStateWebSocketMesh {
    private final Map<Integer, List<Session>> gameHistoryIdToSessions;

    GameStateWebSocketMesh() {
        this.gameHistoryIdToSessions = new ConcurrentHashMap<>();
    }

    void register(int gameHistoryId, Session session) {
        this.gameHistoryIdToSessions.computeIfAbsent(gameHistoryId, id -> new ArrayList<>()).add(session);
    }

    void unregister(int gameHistoryId, Session session) {
        this.gameHistoryIdToSessions.getOrDefault(gameHistoryId, new ArrayList<>())
                .remove(session);
    }

    void broadcast(int gameHistoryId, MoveResultDto moveResultDto) {
        this.gameHistoryIdToSessions.get(gameHistoryId)
                .stream()
                .map(Session::getBasicRemote)
                .forEach(remote -> {
                    try {
                        remote.sendObject(moveResultDto);
                    } catch (IOException | EncodeException e) {
                        throw asRuntime(e);
                    }
                });
    }
}
