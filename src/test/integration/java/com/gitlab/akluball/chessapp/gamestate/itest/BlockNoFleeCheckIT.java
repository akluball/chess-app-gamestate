package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.MoveResultDto;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class BlockNoFleeCheckIT {
    private final TestSetup testSetup;
    private final Builders builders;

    @Guicy
    BlockNoFleeCheckIT(TestSetup testSetup, Builders builders) {
        this.testSetup = testSetup;
        this.builders = builders;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    void blockFromLeftCheck() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|wR|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|bR|bR|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a2").to("a1").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }

    @Test
    void blockFromAboveCheck() {
        String[] before = {
                "8|--|--|--|wR|--|wR|--|--",
                "7|--|--|--|wR|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|bN|--|bK|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("d8").to("e8").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void blockFromRightCheck() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|bP|bP|--|--|--|--|--|--",
                "4|bK|--|--|--|--|--|--|--",
                "3|bP|bP|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|wR",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("h2").to("h4").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void blockFromBelowCheck() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|bR|--|--|--|--|--|--|--",
                "5|--|--|--|wP|wK|wP|--|--",
                "4|bR|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|wB|--",
                "2|--|--|--|--|--|--|--|--",
                "1|bR|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a1").to("e1").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }

    @Test
    void blockFromUpLeftDiagonalCheck() {
        String[] before = {
                "8|--|--|--|--|--|wQ|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|bP|--",
                "5|wR|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|bP|bK",
                "3|wR|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().queen().from("f8").to("f6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void blockFromUpRightDiagonalCheck() {
        String[] before = {
                "8|--|--|--|--|bK|bB|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|bR|--|--|--|--|--|--|--",
                "5|--|--|--|wP|wK|wP|--|--",
                "4|--|--|--|wP|wP|wP|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("f8").to("g7").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }

    @Test
    void blockFromDownRightDiagonalCheck() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|bP|bP|--|--|--|--|--|--",
                "4|bK|bP|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|wR",
                "2|--|--|--|--|wB|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("e2").to("d1").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void blockFromDownLeftDiagonalCheck() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|wN|wP|wP",
                "4|--|--|--|--|--|--|wP|wK",
                "3|bR|--|--|--|--|--|--|--",
                "2|--|--|--|bB|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("d2").to("e1").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }

    @Test
    void blockPawnFromLeftCheck() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|bK",
                "4|--|--|bR|--|--|--|--|bP",
                "3|--|--|--|wB|wB|--|wP|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|wR|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("g3").to("g4").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void blockPawnFromRightCheck() {
        String[] before = {
                "8|--|bR|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|bP|--|bB|bB|--|--|--",
                "4|wP|--|--|--|--|--|--|wR",
                "3|wK|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b5").to("b4").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }

    @Test
    void blockKnightFromLeftUpCheck() {
        String[] before = {
                "8|--|wN|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|bR|--|--|bP|bP|bP|--|--",
                "5|--|--|--|bP|bK|bP|--|--",
                "4|--|--|--|--|--|--|--|wR",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("b8").to("c6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void blockKnightFromUpLeftCheck() {
        String[] before = {
                "8|--|--|bN|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|wR",
                "5|bR|--|--|--|--|--|--|--",
                "4|--|--|--|wP|wK|wP|--|--",
                "3|--|--|--|wP|wP|wP|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("c8").to("d6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }

    @Test
    void blockKnightFromUpRightCheck() {
        String[] before = {
                "8|--|--|--|--|bB|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|bP|bP|bP|--|wN",
                "5|--|--|--|bP|bK|bP|--|--",
                "4|--|--|--|--|--|--|--|wR",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("h6").to("f7").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void blockKnightFromRightUpCheck() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|bN",
                "6|--|--|--|--|--|--|--|--",
                "5|bR|--|--|--|--|--|--|--",
                "4|--|--|--|wP|wK|wP|--|--",
                "3|--|--|--|wP|wP|wP|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("h7").to("g5").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }

    @Test
    void blockKnightFromRightDownCheck() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|bP|bP|bP|--|--",
                "5|--|--|--|bP|bK|bP|--|--",
                "4|wR|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|wN",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("h2").to("g4").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void blockKnightFromDownRightCheck() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|bR|--|--|--|--|--|--|--",
                "4|--|--|--|wP|wK|wP|--|--",
                "3|--|--|--|wP|wP|wP|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|wN|--|--|--|bN",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("h1").to("f2").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }

    @Test
    void blockKnightFromDownLeftCheck() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|bP|bP|bP|--|--",
                "5|--|bB|--|bP|bK|bP|--|--",
                "4|wR|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|wN|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("c1").to("d3").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void blockKnightFromLeftDownCheck() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|bR|--|--|--|--|--|--|--",
                "4|bN|--|--|wP|wK|wP|--|--",
                "3|--|--|--|wP|wP|wP|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|wB|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("a4").to("c3").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }

    @Test
    void pawnAttackFromLeftCounteredWithEnPassant() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|bB|bR|bB|--|--",
                "5|--|--|--|bP|bK|bP|--|--",
                "4|wR|--|--|--|bP|bP|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|wP|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d2").to("d4").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void pawnAttackFromRightCounteredWithEnPassant() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|bP|bN|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|wP|wP|--|--|--",
                "4|--|--|--|wP|wK|wP|--|--",
                "3|--|--|--|wB|wR|wB|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("f7").to("f5").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }
}
