package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class CheckDetectionIT {
    private final TestSetup testSetup;
    private final Builders builders;

    @Guicy
    CheckDetectionIT(TestSetup testSetup, Builders builders) {
        this.testSetup = testSetup;
        this.builders = builders;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    void attackFromLeft() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|bR|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void attackFromLeftBlocked() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|bR|wN|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void attackFromRight() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|wQ",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void attackFromAbove() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bQ|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|wB|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("f1").to("g2").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void attackFromBelow() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|bP|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wR|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d7").to("d6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void attackFromBelowBlocked() {
        String[] before = {
                "8|--|--|--|bB|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wR|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before, GameStatus.BLACK_IN_CHECK);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("d8").to("e7").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void attackFromUpLeftDiagonal() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|bB|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|wP|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d2").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void attackFromUpLeftDiagonalBlocked() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|bB|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|wP|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void attackFromUpRight() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|wQ|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|bP|bK|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b4").to("b3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void attackFromDownRightDiagonal() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|wK|--|--|--|--|--|--",
                "4|wR|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|bQ|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a4").to("a1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void attackFromDownLeft() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|bK|bP|--|--|--|--",
                "3|--|wB|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void attackFromDownLeftBlocked() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|bK|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|bR|--",
                "3|--|wB|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before, GameStatus.BLACK_IN_CHECK);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("g4").to("c4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void leftUpKnightAttack() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|bN|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void upLeftKnightAttack() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|wN|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|bK|bP|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void upRightKnightAttack() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|bN|--|--",
                "2|--|--|--|wP|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d2").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void rightUpKnightAttack() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|wN|--|--|--",
                "4|--|--|bK|bP|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void rightDownKnightAttack() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|wP|wK|--|--|--",
                "3|--|--|--|--|--|--|bN|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void downRightKnightAttack() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|bK|bP|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|wN|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void downLeftKnightAttack() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|wP|wK|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|bN|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void leftDownKnightAttack() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|bK|bP|--|--|--|--",
                "3|wN|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void blackPawnAttackFromLeft() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|bP|--|--|--|--",
                "4|--|--|--|--|wK|wP|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("f4").to("f5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void blackPawnAttackFromRight() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|bP|--|--",
                "4|--|--|--|wP|wK|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void whitePawnAttackFromLeft() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|bK|bP|--|--|--|--",
                "3|--|wP|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void whitePawnAttackFromRight() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|bP|bK|--|--|--|--|--",
                "3|--|--|--|wP|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void kingAttackFromLeft() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|bP|bK|--|--|wP|--|--",
                "4|--|--|--|--|wK|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e4").to("d5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void kingAttackFromRight() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|wP|--|--",
                "4|--|bP|bK|--|wK|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("c4").to("d4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void kingAttackFromAbove() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|bP|--|bK|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|wK|--|--|--",
                "3|--|--|--|--|--|wP|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e4").to("e5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void kingAttackFromBelow() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|wP|--|--",
                "4|--|bP|bK|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|wK|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("c4").to("c3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void kingAttackUpLeftDiagonal() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|bP|--|bK|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|wK|--|--|--",
                "3|--|--|--|--|--|wP|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e4").to("f5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void kingAttackUpRightDiagonal() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|wK|wP|--|--",
                "4|--|bP|bK|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("c4").to("d4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void kingAttackDownRightDiagonal() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|wK|--|--|--",
                "3|--|--|--|wP|--|--|bK|bP",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e4").to("f4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void kingAttackDownLeftDiagonal() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|bK|bP|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|wK|wP|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("f7").to("f6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }
}
