package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.MoveResultDto;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class CheckmateIT {
    private final TestSetup testSetup;
    private final Builders builders;
    private final MockGameHistory mockGameHistory;

    @Guicy
    CheckmateIT(TestSetup testSetup, Builders builders, MockGameHistory mockGameHistory) {
        this.testSetup = testSetup;
        this.builders = builders;
        this.mockGameHistory = mockGameHistory;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    void checkmate() {
        String[] before = {
                "8|--|--|--|bQ|bK|bR|--|--",
                "7|--|--|--|--|--|bR|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("f7").to("e7").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECKMATE);
        this.mockGameHistory.clearEndGamePost(gameState);
    }

    @Test
    void obstrcutedFlee() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|bR|--|bR|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|bP|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|wR|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("d7").to("e7").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECKMATE);
        this.mockGameHistory.clearEndGamePost(gameState);
    }

    @Test
    void noFleeMultipleBlockable() {
        String[] before = {
                "8|--|--|--|bR|bK|bR|--|--",
                "7|--|--|--|--|bQ|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|bB|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|wR|--|--|--|--|--|--|wR",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("e5").to("c3").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECKMATE);
        this.mockGameHistory.clearEndGamePost(gameState);
    }
}
