package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.data.EndedGameDao;
import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.model.SourceDisambiguation;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.security.GameStateSecurity;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.EndGameDto;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.MoveResultDto;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class EndGameIT {
    private final TestSetup testSetup;
    private final Builders builders;
    private final MockGameHistory mockGameHistory;
    private final GameStateDao gameStateDao;
    private final EndedGameDao endedGameDao;

    @Guicy
    EndGameIT(TestSetup testSetup,
            Builders builders,
            MockGameHistory mockGameHistory,
            GameStateDao gameStateDao,
            EndedGameDao endedGameDao) {
        this.testSetup = testSetup;
        this.builders = builders;
        this.mockGameHistory = mockGameHistory;
        this.gameStateDao = gameStateDao;
        this.endedGameDao = endedGameDao;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    void inProgress() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.mockGameHistory.verifyNoEndGamePost(gameState);
    }

    @Test
    @Guicy
    void auth(GameStateSecurity gameStateSecurity) {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|wR|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|wR|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a1").to("a8").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECKMATE);
        SignedJWT token = this.mockGameHistory.retrieveEndGamePostToken(gameState);
        assertThat(gameStateSecurity.isValidSignature(token)).isTrue();
        this.mockGameHistory.clearEndGamePost(gameState);
    }

    @Test
    void whiteInCheck() {
        String[] board = {
                "8|bR|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a8").to("a1").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
        this.mockGameHistory.verifyNoEndGamePost(gameState);
    }

    @Test
    void blackInCheck() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|wR",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("h1").to("h8").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
        this.mockGameHistory.verifyNoEndGamePost(gameState);
    }

    @Test
    void whiteInCheckmate() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|bR|--|--|--|--|--|--|--",
                "2|--|bR|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a3").to("a1").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECKMATE);
        EndGameDto endGameDto = this.builders.endGameDto().whiteInCheckmate().build();
        this.mockGameHistory.verifyEndGamePost(gameState, endGameDto);
    }

    @Test
    void blackInCheckmate() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|wN|--|--|--|--|--|wR",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|wQ|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().queen().from("g2").to("g8").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECKMATE);
        EndGameDto endGameDto = this.builders.endGameDto().blackInCheckmate().build();
        this.mockGameHistory.verifyEndGamePost(gameState, endGameDto);
    }

    @Test
    void blackTimerExpired() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(10_000L)
                .secondsSinceStart(11).build();
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isFalse();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_TIME_EXPIRED);
        EndGameDto endGameDto = this.builders.endGameDto().blackTimerExpired().build();
        this.mockGameHistory.verifyEndGamePost(gameState, endGameDto);
    }

    @Test
    void whiteTimerExpired() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(7_000L).blackSideMillis(100_000L)
                .secondsSinceStart(10).build();
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isFalse();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_TIME_EXPIRED);
        EndGameDto endGameDto = this.builders.endGameDto().whiteTimerExpired().build();
        this.mockGameHistory.verifyEndGamePost(gameState, endGameDto);
    }

    @Test
    void stalemate() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|bB|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|bR|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|wK",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("f4").to("e3").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.DRAW);
        EndGameDto endGameDto = this.builders.endGameDto().draw().build();
        this.mockGameHistory.verifyEndGamePost(gameState, endGameDto);
    }

    @Test
    void insufficentMaterial() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|bN|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|wN|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("f1").to("g3").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.DRAW);
        EndGameDto endGameDto = this.builders.endGameDto().draw().build();
        this.mockGameHistory.verifyEndGamePost(gameState, endGameDto);
    }

    @Test
    void persistOnPostFail() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|bR|--|--|--|--|--|--|--",
                "2|--|bR|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        this.mockGameHistory.expectEndGamePostFail(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a3").to("a1").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECKMATE);
        this.mockGameHistory.verifyEndGamePost(gameState);
        assertThat(this.endedGameDao.findByGameHistoryId(gameState.getGameHistoryId())).isNotNull();
    }

    @Test
    void noPersistOnPostSuccess() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|wN|--|--|--|--|--|wR",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|wQ|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        this.mockGameHistory.expectEndGamePost(gameState);
        wsClient.sendMove(this.builders.moveDto().queen().from("g2").to("g8").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECKMATE);
        this.mockGameHistory.verifyEndGamePost(gameState);
        assertThat(this.endedGameDao.findByGameHistoryId(gameState.getGameHistoryId())).isNull();
    }

    @Test
    void deleteGameState() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|bR|--|--|--|--|--|--|--",
                "2|--|bR|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        this.mockGameHistory.expectEndGamePostFail(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a3").to("a1").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECKMATE);
        this.mockGameHistory.verifyEndGamePost(gameState);
        this.gameStateDao.detach(gameState);
        assertThat(this.gameStateDao.findByGameHistoryId(gameState.getGameHistoryId())).isNull();
    }

    @Test
    void endedGameMoveSummaries() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|wR|--|--|--|--|--|--",
                "6|wR|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveSummary firstMove = this.builders.moveSummary().rook().from("b6").to("a6").build();
        MoveSummary secondMove = this.builders.moveSummary().king().from("f8").to("e8").build();
        MoveHistory moveHistory = this.builders.moveHistory().moveSummary(firstMove).moveSummary(secondMove).build();
        GameState gameState = this.testSetup.persistedGameState(board, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        this.mockGameHistory.expectEndGamePostFail(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a6").to("a8").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        this.mockGameHistory.verifyEndGamePost(gameState);
        MoveSummary thirdMove = this.builders.moveSummary().rook().from("a6").to("a8")
                .sourceDisambiguation(SourceDisambiguation.NONE).millis(moveResultDto.getDurationMillis())
                .gameStatus(GameStatus.BLACK_IN_CHECKMATE).build();
        List<MoveSummary> moveSummaries = this.endedGameDao.findByGameHistoryId(gameState.getGameHistoryId())
                .getMoveSummaries();
        assertThat(moveSummaries).hasSize(3);
        assertThat(moveSummaries.get(0)).isEqualTo(firstMove);
        assertThat(moveSummaries.get(1)).isEqualTo(secondMove);
        assertThat(moveSummaries.get(2)).isEqualTo(thirdMove);
    }

    @Test
    void endedGameDtoMoveSummaries() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|bR|--|--|--|--|--|--|--",
                "2|--|bR|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveSummary firstWhiteMove = this.builders.moveSummary().king().from("g1").to("f1").seconds(1).build();
        MoveSummary firstBlackMove = this.builders.moveSummary().rook().from("b3").to("a3").seconds(2).build();
        MoveSummary secondWhiteMove = this.builders.moveSummary().king().from("f1").to("e1").seconds(3).build();
        MoveHistory moveHistory = this.builders.moveHistory().moveSummary(firstWhiteMove).moveSummary(firstBlackMove)
                .moveSummary(secondWhiteMove).build();
        GameState gameState = this.testSetup.persistedGameState(board, moveHistory);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        this.mockGameHistory.expectEndGamePostFail(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a3").to("a1").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECKMATE);
        MoveSummary secondBlackMove = this.builders.moveSummary().rook().from("a3").to("a1")
                .millis(moveResultDto.getDurationMillis()).gameStatus(GameStatus.WHITE_IN_CHECKMATE).build();
        EndGameDto endGameDto = this.builders.endGameDto().whiteInCheckmate().moveSummary(firstWhiteMove)
                .moveSummary(firstBlackMove).moveSummary(secondWhiteMove).moveSummary(secondBlackMove).build();
        this.mockGameHistory.verifyEndGamePost(gameState, endGameDto);
    }
}
