package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.client.GameStateClient;
import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.security.GameHistorySecurity;
import com.gitlab.akluball.chessapp.gamestate.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.GameStateCreateDto;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.GameStateDto;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.UniqueBuilders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameStateControllerIT {
    private final Builders builders;
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final GameStateDao gameStateDao;
    private final GameHistorySecurity gameHistorySecurity;
    private final UserSecurity userSecurity;
    private final GameStateClient gameStateClient;

    @Guicy
    GameStateControllerIT(Builders builders,
            UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            GameStateDao gameStateDao,
            GameHistorySecurity gameHistorySecurity,
            UserSecurity userSecurity,
            GameStateClient gameStateClient) {
        this.builders = builders;
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.gameStateDao = gameStateDao;
        this.gameHistorySecurity = gameHistorySecurity;
        this.userSecurity = userSecurity;
        this.gameStateClient = gameStateClient;
    }

    @Test
    void create() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        String token = this.gameHistorySecurity.getToken();
        Response response = this.gameStateClient.create(token, gameHistoryId, gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    void createNoAuth() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        Response response = this.gameStateClient.create(null, gameHistoryId, gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createBadRole() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        String badRoleToken = this.gameHistorySecurity.getBadRoleToken();
        Response response = this.gameStateClient.create(badRoleToken, gameHistoryId, gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createBadServiceName() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        String badServiceNameToken = this.gameHistorySecurity.getBadServiceNameToken();
        Response response = this.gameStateClient.create(badServiceNameToken, gameHistoryId, gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createBadSignature() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        String badSignatureToken = this.gameHistorySecurity.getBadSignatureToken();
        Response response = this.gameStateClient.create(badSignatureToken, gameHistoryId, gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createExpiredToken() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        String expiredToken = this.gameHistorySecurity.getExpiredToken();
        Response response = this.gameStateClient.create(expiredToken, gameHistoryId, gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createNonIntegerId() {
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        String token = this.gameHistorySecurity.getToken();
        Response response = this.gameStateClient.create(token, "not-an-integer", gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoGameStateCreateDto() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        String token = this.gameHistorySecurity.getToken();
        Response response = this.gameStateClient.create(token, gameHistoryId, null);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoWhiteSideUserId() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().whiteSideUserId(null).build();
        String token = this.gameHistorySecurity.getToken();
        Response response = this.gameStateClient.create(token, gameHistoryId, gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoBlackSideUserId() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().blackSideUserId(null).build();
        String token = this.gameHistorySecurity.getToken();
        Response response = this.gameStateClient.create(token, gameHistoryId, gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoMillisPerPlayer() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().millisPerPlayer(null).build();
        String token = this.gameHistorySecurity.getToken();
        Response response = this.gameStateClient.create(token, gameHistoryId, gameStateCreateDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void get() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void getNoAuth() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        Response response = this.gameStateClient.get(null, gameState.getGameHistoryId());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getBadRole() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        String badRoleToken = this.userSecurity.badRoleTokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(badRoleToken, gameState.getGameHistoryId());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getBadSignature() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(badSignatureToken, gameState.getGameHistoryId());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getExpiredToken() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        String expiredToken = this.userSecurity.expiredTokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(expiredToken, gameState.getGameHistoryId());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getNonIntegerId() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(token, "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void getNotParticipant() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    void getNotFound() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        String token = this.userSecurity.tokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    @Guicy
    void getExpiredTimerRespondNotFound(MockGameHistory mockGameHistory) {
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(102).build();
        GameState gameState = this.uniqueBuilders.gameState().moveTimer(moveTimer).build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
        mockGameHistory.clearEndGamePost(gameState);
    }

    @Test
    @Guicy
    void getInformsGameHistoryExpiredTimerGame(MockGameHistory mockGameHistory) {
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(102).build();
        GameState gameState = this.uniqueBuilders.gameState().moveTimer(moveTimer).build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
        mockGameHistory.verifyEndGamePost(gameState);
    }

    @Test
    void getByParticipantCurrentUser() {
        int userId = this.uniqueData.userId();
        GameState isParticipant = this.uniqueBuilders.gameState().whiteSideUserId(userId).build();
        GameState isNotParticipant = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(isParticipant, isNotParticipant);
        String token = this.userSecurity.tokenFor(userId);
        Response response = this.gameStateClient.getByParticipant(token, (String) null, null);
        assertThat(response.getStatus()).isEqualTo(200);
        List<GameStateDto> gameStates = response.readEntity(new GenericType<List<GameStateDto>>() {});
        assertThat(gameStates.stream().map(GameStateDto::getGameHistoryId).collect(Collectors.toList()))
                .containsExactly(isParticipant.getGameHistoryId());
    }

    @Test
    void getByParticipantNonCurrentUser() {
        int userId = this.uniqueData.userId();
        GameState isParticipant = this.uniqueBuilders.gameState().whiteSideUserId(userId).build();
        GameState isNotParticipant = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(isParticipant, isNotParticipant);
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameStateClient.getByParticipant(token, userId, null);
        assertThat(response.getStatus()).isEqualTo(200);
        List<GameStateDto> gameStates = response.readEntity(new GenericType<List<GameStateDto>>() {});
        assertThat(gameStates.stream().map(GameStateDto::getGameHistoryId).collect(Collectors.toList()))
                .containsExactly(isParticipant.getGameHistoryId());
    }

    @Test
    void getByParticipantExpiredTimerGameIgnored() {
        int userId = this.uniqueData.userId();
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(102).build();
        GameState isParticipant = this.uniqueBuilders.gameState().whiteSideUserId(userId).moveTimer(moveTimer).build();
        this.gameStateDao.persist(isParticipant);
        String token = this.userSecurity.tokenFor(userId);
        Response response = this.gameStateClient.getByParticipant(token, userId, null);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(new GenericType<List<GameStateDto>>() {})).isEmpty();
    }

    //    @Test
//    @Guicy
    void getByParticipantInformGameHistoryExpiredTimerGame(MockGameHistory mockGameHistory) {
        int userId = this.uniqueData.userId();
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(102).build();
        GameState expired = this.uniqueBuilders.gameState().whiteSideUserId(userId).moveTimer(moveTimer).build();
        this.gameStateDao.persist(expired);
        String token = this.userSecurity.tokenFor(userId);
        Response response = this.gameStateClient.getByParticipant(token, userId, null);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(new GenericType<List<GameStateDto>>() {})).isEmpty();
        mockGameHistory.verifyEndGamePost(expired);
    }

    @Test
    void getByParticipantsIsTurn() {
        int userId = this.uniqueData.userId();
        GameState myMoveAsWhiteSide = this.uniqueBuilders.gameState().whiteSideUserId(userId).build();
        MoveHistory moveHistory = this.builders.moveHistory().moveSummary(this.builders.moveSummary().build()).build();
        GameState myMoveAsBlackSide = this.uniqueBuilders.gameState().blackSideUserId(userId).moveHistory(moveHistory)
                .build();
        GameState notMyMove = this.uniqueBuilders.gameState().blackSideUserId(userId).build();
        this.gameStateDao.persist(myMoveAsWhiteSide, myMoveAsBlackSide, notMyMove);
        Response response = this.gameStateClient.getByParticipant(this.userSecurity.tokenFor(userId), null, true);
        assertThat(response.getStatus()).isEqualTo(200);
        List<GameStateDto> myMoveGames = response.readEntity(new GenericType<List<GameStateDto>>() {});
        assertThat(myMoveGames.stream().map(GameStateDto::getGameHistoryId).collect(Collectors.toList()))
                .containsExactly(myMoveAsWhiteSide.getGameHistoryId(), myMoveAsBlackSide.getGameHistoryId())
                .inOrder();
    }

    @Test
    void getByParticipantOrdersByMoveStart() {
        int userId = this.uniqueData.userId();
        MoveTimer freshMoveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(7).build();
        MoveTimer freshestMoveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(5).build();
        MoveTimer staleMoveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(11).build();
        GameState fresh = this.uniqueBuilders.gameState().whiteSideUserId(userId).moveTimer(freshMoveTimer).build();
        GameState freshest = this.uniqueBuilders.gameState().blackSideUserId(userId).moveTimer(freshestMoveTimer)
                .build();
        GameState stale = this.uniqueBuilders.gameState().blackSideUserId(userId).moveTimer(staleMoveTimer).build();
        this.gameStateDao.persist(fresh, freshest, stale);
        String token = this.userSecurity.tokenFor(userId);
        Response response = this.gameStateClient.getByParticipant(token, (String) null, null);
        assertThat(response.getStatus()).isEqualTo(200);
        List<GameStateDto> gameStates = response.readEntity(new GenericType<List<GameStateDto>>() {});
        assertThat(gameStates.stream().map(GameStateDto::getGameHistoryId).collect(Collectors.toList()))
                .containsExactly(freshest.getGameHistoryId(), fresh.getGameHistoryId(), stale.getGameHistoryId())
                .inOrder();
    }

    @Test
    void getByParticipantNoAuth() {
        Response response = this.gameStateClient.getByParticipant(null, (String) null, null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getByParticipantNonIntegerUserId() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameStateClient.getByParticipant(token, "not-an-integer", null);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void getByParticipantNonBooleanIsTurn() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameStateClient.getByParticipant(token, null, "not-a-boolean");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }
}
