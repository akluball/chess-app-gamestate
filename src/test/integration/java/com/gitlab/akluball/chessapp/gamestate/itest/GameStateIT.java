package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.client.GameStateClient;
import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.security.GameHistorySecurity;
import com.gitlab.akluball.chessapp.gamestate.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.GameStateCreateDto;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.UniqueBuilders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.Instant;

import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.bufferMinusFourSeconds;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameStateIT {
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final GameStateDao gameStateDao;
    private final GameHistorySecurity gameHistorySecurity;
    private final GameStateClient gameStateClient;

    @Guicy
    GameStateIT(UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            GameStateDao gameStateDao,
            GameHistorySecurity gameHistorySecurity,
            GameStateClient gameStateClient) {
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.gameStateDao = gameStateDao;
        this.gameHistorySecurity = gameHistorySecurity;
        this.gameStateClient = gameStateClient;
    }

    @Test
    void createPersists() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        this.gameStateClient.create(this.gameHistorySecurity.getToken(), gameHistoryId, gameStateCreateDto);
        GameState gameState = this.gameStateDao.findByGameHistoryId(gameHistoryId);
        assertThat(gameState).isNotNull();
    }

    @Test
    void createSetsUserIds() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        this.gameStateClient.create(this.gameHistorySecurity.getToken(), gameHistoryId, gameStateCreateDto);
        GameState gameState = this.gameStateDao.findByGameHistoryId(gameHistoryId);
        assertThat(gameState.getWhiteSideUserId()).isEqualTo(gameStateCreateDto.getWhiteSideUserId());
        assertThat(gameState.getBlackSideUserId()).isEqualTo(gameStateCreateDto.getBlackSideUserId());
    }

    @Test
    void createInitialBoard() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        this.gameStateClient.create(this.gameHistorySecurity.getToken(), gameHistoryId, gameStateCreateDto);
        GameState gameState = this.gameStateDao.findByGameHistoryId(gameHistoryId);
        String[] board = {
                "8|bR|bN|bB|bQ|bK|bB|bN|bR",
                "7|bP|bP|bP|bP|bP|bP|bP|bP",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|wP|wP|wP|wP|wP|wP|wP|wP",
                "1|wR|wN|wB|wQ|wK|wB|wN|wR",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(board);
    }

    @Test
    void createMoveStart() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().build();
        this.gameStateClient.create(this.gameHistorySecurity.getToken(), gameHistoryId, gameStateCreateDto);
        GameState gameState = this.gameStateDao.findByGameHistoryId(gameHistoryId);
        assertThat(gameState.getMoveTimer().getMoveStart()).isIn(bufferMinusFourSeconds(Instant.now()));
    }

    @Test
    void createWhiteSideMillis() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().millisPerPlayer(600_000L)
                .build();
        this.gameStateClient.create(this.gameHistorySecurity.getToken(), gameHistoryId, gameStateCreateDto);
        GameState gameState = this.gameStateDao.findByGameHistoryId(gameHistoryId);
        assertThat(gameState.getMoveTimer().getWhiteSideMillis()).isEqualTo(600_000L);
    }

    @Test
    void createBlackSideMillis() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameStateCreateDto gameStateCreateDto = this.uniqueBuilders.gameStateCreateDto().millisPerPlayer(600_000L)
                .build();
        this.gameStateClient.create(this.gameHistorySecurity.getToken(), gameHistoryId, gameStateCreateDto);
        GameState gameState = this.gameStateDao.findByGameHistoryId(gameHistoryId);
        assertThat(gameState.getMoveTimer().getBlackSideMillis()).isEqualTo(600_000L);
    }

    @Test
    @Guicy
    void getExpiredTimerGameDeleted(Builders builders,
            UserSecurity userSecurity,
            MockGameHistory mockGameHistory) {
        MoveTimer moveTimer = builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(102).build();
        GameState gameState = this.uniqueBuilders.gameState().moveTimer(moveTimer).build();
        this.gameStateDao.persist(gameState);
        mockGameHistory.expectEndGamePost(gameState);
        this.gameStateClient.get(userSecurity.tokenFor(gameState.getWhiteSideUserId()), gameState.getGameHistoryId());
        this.gameStateDao.detach(gameState);
        assertThat(this.gameStateDao.findByGameHistoryId(gameState.getGameHistoryId())).isNull();
        mockGameHistory.clearEndGamePost(gameState);
    }

    @Test
    @Guicy
    void getByParticipantExpiredTimerGameDeleted(Builders builders,
            UserSecurity userSecurity,
            MockGameHistory mockGameHistory) {
        int userId = this.uniqueData.userId();
        MoveTimer moveTimer = builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(102).build();
        GameState gameState = this.uniqueBuilders.gameState().blackSideUserId(userId).moveTimer(moveTimer).build();
        this.gameStateDao.persist(gameState);
        mockGameHistory.expectEndGamePost(gameState);
        this.gameStateClient.getByParticipant(userSecurity.tokenFor(userId), (String) null, null);
        this.gameStateDao.detach(gameState);
        assertThat(this.gameStateDao.findByGameHistoryId(gameState.getGameHistoryId())).isNull();
        mockGameHistory.clearEndGamePost(gameState);
    }
}
