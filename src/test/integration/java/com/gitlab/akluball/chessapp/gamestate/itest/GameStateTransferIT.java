package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.client.GameStateClient;
import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.model.Piece;
import com.gitlab.akluball.chessapp.gamestate.itest.model.Position;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.GameStateDto;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.UniqueBuilders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.bufferBelow5000;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameStateTransferIT {
    private final Builders builders;
    private final UniqueBuilders uniqueBuilders;
    private final GameStateDao gameStateDao;
    private final UserSecurity userSecurity;
    private final GameStateClient gameStateClient;

    @Guicy
    GameStateTransferIT(Builders builders,
            UniqueBuilders uniqueBuilders,
            GameStateDao gameStateDao,
            UserSecurity userSecurity,
            GameStateClient gameStateClient) {
        this.builders = builders;
        this.uniqueBuilders = uniqueBuilders;
        this.gameStateDao = gameStateDao;
        this.userSecurity = userSecurity;
        this.gameStateClient = gameStateClient;
    }

    @Test
    void whiteSideUserId() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        assertThat(response.readEntity(GameStateDto.class).getWhiteSideUserId())
                .isEqualTo(gameState.getWhiteSideUserId());
    }

    @Test
    void blackSideUserId() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        assertThat(response.readEntity(GameStateDto.class).getBlackSideUserId())
                .isEqualTo(gameState.getBlackSideUserId());
    }

    @Test
    void moveSummaries() {
        MoveSummary firstMove = this.builders.moveSummary().pawn().from("a2").to("a3").build();
        MoveSummary secondMove = this.builders.moveSummary().pawn().from("a7").to("a6").build();
        MoveHistory moveHistory = this.builders.moveHistory().moveSummary(firstMove).moveSummary(secondMove).build();
        GameState gameState = this.uniqueBuilders.gameState().moveHistory(moveHistory).build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getBlackSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        List<MoveSummary.Dto> moveSummaries = response.readEntity(GameStateDto.class).getMoveSummaries();
        assertThat(moveSummaries).hasSize(2);
        assertThat(moveSummaries.get(0)).isEqualTo(firstMove.toDto());
        assertThat(moveSummaries.get(1)).isEqualTo(secondMove.toDto());
    }

    @Test
    void getPositionToPieceProperty() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.uniqueBuilders.gameState().board(board).build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getWhiteSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        Map<Position, Piece> positionToPiece = response.readEntity(GameStateDto.class).getPositionToPiece();
        assertThat(positionToPiece.size()).isEqualTo(4);
        assertThat(positionToPiece.get(Position.E1)).isEqualTo(Piece.WHITE_KING);
        assertThat(positionToPiece.get(Position.E2)).isEqualTo(Piece.WHITE_PAWN);
        assertThat(positionToPiece.get(Position.E7)).isEqualTo(Piece.BLACK_PAWN);
        assertThat(positionToPiece.get(Position.E8)).isEqualTo(Piece.BLACK_KING);
    }

    @Test
    void getMillisRemainingPropertyIsTurn() {
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(15).build();
        GameState gameState = this.uniqueBuilders.gameState().moveTimer(moveTimer).build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getBlackSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        assertThat(response.readEntity(GameStateDto.class).getWhiteSideMillisRemaining())
                .isIn(bufferBelow5000(85_000L));
    }

    @Test
    void getMillisRemainingPropertyIsNotTurn() {
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(15).build();
        GameState gameState = this.uniqueBuilders.gameState().moveTimer(moveTimer).build();
        this.gameStateDao.persist(gameState);
        String token = this.userSecurity.tokenFor(gameState.getBlackSideUserId());
        Response response = this.gameStateClient.get(token, gameState.getGameHistoryId());
        assertThat(response.readEntity(GameStateDto.class).getBlackSideMillisRemaining())
                .isEqualTo(100_000L);
    }
}
