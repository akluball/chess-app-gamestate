package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.util.UniqueBuilders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.UniqueData;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameStateWebSocketIT {
    private final TestSetup testSetup;
    private final Builders builders;
    private final UniqueBuilders uniqueBuilders;
    private final GameStateDao gameStateDao;
    private final UserSecurity userSecurity;
    private final GameStateWsManager gameStateWsManager;

    @Guicy
    GameStateWebSocketIT(TestSetup testSetup,
            Builders builders,
            UniqueBuilders uniqueBuilders,
            GameStateDao gameStateDao,
            UserSecurity userSecurity,
            GameStateWsManager gameStateWsManager) {
        this.testSetup = testSetup;
        this.builders = builders;
        this.uniqueBuilders = uniqueBuilders;
        this.gameStateDao = gameStateDao;
        this.userSecurity = userSecurity;
        this.gameStateWsManager = gameStateWsManager;
    }

    @AfterEach
    void cleanup() {
        this.gameStateWsManager.cleanup();
        this.testSetup.cleanup();
    }

    @Test
    void noAuth() {
        GameState gameState = this.testSetup.persistedGameState();
        GameStateWsClient wsClient = this.gameStateWsManager.createClient(null, gameState);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void badRole() {
        GameState gameState = this.testSetup.persistedGameState();
        String badRoleToken = this.userSecurity.badRoleTokenFor(gameState.getBlackSideUserId());
        GameStateWsClient wsClient = this.gameStateWsManager.createClient(badRoleToken, gameState);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void badSignature() {
        GameState gameState = this.testSetup.persistedGameState();
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(gameState.getWhiteSideUserId());
        GameStateWsClient wsClient = this.gameStateWsManager.createClient(badSignatureToken, gameState);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void expiredToken() {
        GameState gameState = this.testSetup.persistedGameState();
        String expiredToken = this.userSecurity.expiredTokenFor(gameState.getWhiteSideUserId());
        GameStateWsClient wsClient = this.gameStateWsManager.createClient(expiredToken, gameState);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void gameStateNotFound() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("not found");
    }

    @Test
    @Guicy
    void notParticipant(UniqueData uniqueData) {
        GameState gameState = this.testSetup.persistedGameState();
        String token = this.userSecurity.tokenFor(uniqueData.userId());
        GameStateWsClient wsClient = this.gameStateWsManager.createClient(token, gameState);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("not authorized");
    }

    @Test
    void gameStateNotFoundAfterConnection() {
        GameState gameState = this.testSetup.persistedGameState();
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        assertThat(wsClient.waitForCloseReason()).isNull();
        this.gameStateDao.delete(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("not found");
    }

    @Test
    void whiteSideMove() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void blackSideMove() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void broadcastOnMoveExecuted() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClientOne = this.testSetup.whiteSideWsClient(gameState);
        GameStateWsClient wsClientTwo = this.testSetup.blackSideWsClient(gameState);
        wsClientOne.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClientOne.waitForMoveResult().wasExecuted()).isTrue();
        assertThat(wsClientTwo.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void noBroadcastOnMoveNotExecuted() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClientOne = this.testSetup.blackSideWsClient(gameState);
        GameStateWsClient wsClientTwo = this.testSetup.blackSideWsClient(gameState);
        wsClientOne.sendMove(this.builders.moveDto().pawn().from("e7").to("f7").build());
        assertThat(wsClientOne.waitForMoveResult().wasExecuted()).isFalse();
        assertThat(wsClientTwo.waitForMoveResult()).isNull();
    }

    @Test
    void whiteSideMoveNotTurn() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void badPieceChar() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pieceLetter('X').from("a7").to("a6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void badSource() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("z2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void pieceNotAtSource() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("a7").to("a6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void wrongPieceAtSource() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void badTarget() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|bR",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("h8").to("h9").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }

    @Test
    void teammateAtTarget() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|wP|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
    }
}
