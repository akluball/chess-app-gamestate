package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class KingIT {
    private final TestSetup testSetup;
    private final Builders builders;
    private final GameStateDao gameStateDao;

    @Guicy
    KingIT(TestSetup testSetup, Builders builders, GameStateDao gameStateDao) {
        this.testSetup = testSetup;
        this.builders = builders;
        this.gameStateDao = gameStateDao;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    void horizontalOne() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e1").to("d1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|wK|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void diagonalOne() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e8").to("f7").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|bP|bK|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void horizontalMoreThanOne() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e1").to("a1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void diagonalMoreThanOne() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e8").to("g6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void knightMove() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e1").to("f3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void whiteKingCastleLeft() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|wR|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory().build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e1").to("c1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|wK|wR|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void blackKingCastleLeft() {
        String[] before = {
                "8|bR|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|wP|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("e2").to("e3").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e8").to("c8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|bK|bR|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|wP|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void whiteKingCastleRight() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|wR",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory().build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e1").to("g1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|wR|wK|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void blackKingCastleRight() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|bR",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|wP|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("e2").to("e3").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e8").to("g8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|--|bR|bK|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|wP|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void castleNotStartingRank() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wK|--|--|wR",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory().build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e2").to("g2").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void castleKingNotAtStart() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wK|--|--|--",
                "1|wR|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory().build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e2").to("c1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void castleRookNotAtStart() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|bR",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|wP|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("e2").to("e3").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e8").to("g8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void castleBlocked() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|wR|wN|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory().build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e1").to("c1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void castleKingAlreadyMoved() {
        String[] before = {
                "8|bR|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|wP|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("e2").to("e3").build())
                .moveSummary(this.builders.moveSummary().king().from("e8").to("e7").build())
                .moveSummary(this.builders.moveSummary().pawn().from("e3").to("e4").build())
                .moveSummary(this.builders.moveSummary().king().from("e7").to("e8").build())
                .moveSummary(this.builders.moveSummary().pawn().from("e4").to("e5").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e8").to("c8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void castleOpponentKingAlreadyMoved() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|bP|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|wP|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|wR|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("e2").to("e3").build())
                .moveSummary(this.builders.moveSummary().king().from("e8").to("e7").build())
                .moveSummary(this.builders.moveSummary().pawn().from("e3").to("e4").build())
                .moveSummary(this.builders.moveSummary().king().from("e7").to("e8").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e1").to("c1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|bP|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|wP|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|wK|wR|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void castleRookAlreadyMoved() {
        String[] before = {
                "8|bR|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|wP|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("e2").to("e3").build())
                .moveSummary(this.builders.moveSummary().rook().from("a8").to("a7").build())
                .moveSummary(this.builders.moveSummary().pawn().from("e3").to("e4").build())
                .moveSummary(this.builders.moveSummary().rook().from("a7").to("a8").build())
                .moveSummary(this.builders.moveSummary().pawn().from("e4").to("e5").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e8").to("c8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void castleFromCheck() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bR|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|wR|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory().build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e1").to("c1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void castleThroughCheck() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|bR",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|wR|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().rook().from("g1").to("f1").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e8").to("g8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }
}
