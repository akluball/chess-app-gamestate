package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.model.SourceDisambiguation;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.MoveResultDto;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.bufferAbove5000;
import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.bufferBelow5000;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class MoveResultTransferIT {
    private final TestSetup testSetup;
    private final Builders builders;
    private final GameStateWsManager gameStateWsManager;

    @Guicy
    MoveResultTransferIT(TestSetup testSetup, Builders builders, GameStateWsManager gameStateWsManager) {
        this.testSetup = testSetup;
        this.builders = builders;
        this.gameStateWsManager = gameStateWsManager;
    }

    @AfterEach
    void cleanup() {
        this.gameStateWsManager.cleanup();
        this.testSetup.cleanup();
    }

    @Test
    void movingSideMillisRemaining() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(15).build();
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getBlackSideMillisRemaining()).isIn(bufferBelow5000(85_000L));
    }

    @Test
    void nonMovingSideMillisRemaining() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(15).build();
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getWhiteSideMillisRemaining()).isIn(bufferBelow5000(100_000L));
    }

    @Test
    void moveResultDtoPiece() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getPieceLetter()).isEqualTo("P");
    }

    @Test
    void moveResultDtoPromotion() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|bP|--|--|wK|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b2").to("b1").promoteToQueen().build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getPromotionPieceLetter()).isEqualTo("Q");
    }

    @Test
    void moveResultDtoSource() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getSource()).isEqualTo("e2");
    }

    @Test
    void moveResultDtoTarget() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getTarget()).isEqualTo("e6");
    }

    @Test
    void moveResultDtoSourceAmbiguity() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|wR|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wR|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("e2").to("e4").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getSourceDisambiguation()).isEqualTo(SourceDisambiguation.RANK);
    }

    @Test
    void moveResultDtoIsCapture() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|wP|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("d6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.isCapture()).isTrue();
    }

    @Test
    void moveResultDtoDurationMillis() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(31).build();
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getDurationMillis()).isIn(bufferAbove5000(31_000));
    }

    @Test
    void moveResultDtoGameStatus() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.IN_PROGRESS);
    }
}
