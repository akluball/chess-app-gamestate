package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.model.Position;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.bufferAbove5000;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class MoveSummaryIT {
    private final TestSetup testSetup;
    private final Builders builders;
    private final GameStateDao gameStateDao;

    @Guicy
    MoveSummaryIT(TestSetup testSetup, Builders builders, GameStateDao gameStateDao) {
        this.testSetup = testSetup;
        this.builders = builders;
        this.gameStateDao = gameStateDao;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    void pieceLetter() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|wN|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("g2").to("f4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getPieceLetter()).isEqualTo("N");
    }

    @Test
    void promotionPieceLetterForPromotion() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|bP|--|--|--|wK|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("a2").to("a1").promoteToRook().build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getPromotionPieceLetter()).isEqualTo("R");
    }

    @Test
    void promotionPieceLetterForNonPromotion() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getPromotionPieceLetter()).isNull();
    }

    @Test
    void source() {
        String[] board = {
                "8|--|--|--|--|bK|bB|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("f8").to("d6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSource()).isEqualTo(Position.F8);
    }

    @Test
    void target() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|wR",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("h1").to("h3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getTarget()).isEqualTo(Position.H3);
    }

    @Test
    void isNotCapture() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|bN|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("c7").to("d5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().isCapture()).isFalse();
    }

    @Test
    void isCapture() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|bB",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|wR",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("h1").to("h6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().isCapture()).isTrue();
    }

    @Test
    void isCaptureEnPassant() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|bP|wP|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("e2").to("e4").build()).build();
        GameState gameState = this.testSetup.persistedGameState(board, moveHistory);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d4").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().isCapture()).isTrue();
    }

    @Test
    void moveDuration() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(17).build();
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before, moveTimer);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getDurationMillis()).isIn(bufferAbove5000(17_000L));
    }

    @Test
    void gameStatus() {
        String[] board = {
                "8|bR|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a8").to("a1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getGameStatus()).isEqualTo(GameStatus.WHITE_IN_CHECK);
    }
}
