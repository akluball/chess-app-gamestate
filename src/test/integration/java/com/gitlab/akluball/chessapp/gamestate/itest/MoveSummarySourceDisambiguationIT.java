package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.SourceDisambiguation;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class MoveSummarySourceDisambiguationIT {
    private final TestSetup testSetup;
    private final Builders builders;
    private final GameStateDao gameStateDao;

    @Guicy
    MoveSummarySourceDisambiguationIT(TestSetup testSetup, Builders builders, GameStateDao gameStateDao) {
        this.testSetup = testSetup;
        this.builders = builders;
        this.gameStateDao = gameStateDao;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    void rookNoSourceAmbiguity() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|wR|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a1").to("a4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.NONE);
    }

    @Test
    void fileResolvesOtherRookRight() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|bR",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|bR|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("h8").to("h6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.FILE);
    }

    @Test
    void fileResolvesOtherRookLeft() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|wR|--|--|wR|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("a4").to("c4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.FILE);
    }

    @Test
    void fileResolvesOtherRookUp() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bR|--|--|--",
                "6|--|--|--|--|--|--|--|bR",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("e7").to("h7").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.FILE);
    }

    @Test
    void rankResolvesOtherRookDown() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|wR|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|wR|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("d1").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.RANK);
    }

    @Test
    void otherRookBlocked() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bR|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|bN",
                "4|--|--|--|--|--|--|--|bR",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("e7").to("h7").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.NONE);
    }

    @Test
    void fileResolvesOtherBishopUpLeft() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|wB|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wB|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("f5").to("d3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.FILE);
    }

    @Test
    void rankResolvesOtherBishopDownRight() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bB|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|bB|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("e3").to("g5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.RANK);
    }

    @Test
    void bothResolvesOtherBishopsUpRightAndDownLeft() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|wB|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|wB|--|wB|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("f2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.BOTH);
    }

    @Test
    void bothResolvesOtherQueensDownRightAndLeft() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|bQ|--|--|--|--",
                "5|--|--|--|bQ|--|bQ|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().queen().from("d5").to("e5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.BOTH);
    }

    @Test
    void bothResolvesOtherKnightsLeftLeftDownRightRightUp() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|wN|--|--|--|wN|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|wN|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().knight().from("c4").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.BOTH);
    }

    @Test
    void fileForPawnCaptureLeft() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|bP|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("f3").to("e2").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.FILE);
    }

    @Test
    void fileForEnPassantRight() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|wP|bP|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("d4").to("d5").build())
                .moveSummary(this.builders.moveSummary().pawn().from("e7").to("e5").build()).build();
        GameState gameState = this.testSetup.persistedGameState(board, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d5").to("e6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getFreshestMoveSummary().getSourceDisambiguation()).isEqualTo(SourceDisambiguation.FILE);
    }
}
