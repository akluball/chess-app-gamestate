package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.Instant;

import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.bufferBelow5000;
import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.bufferMinusFourSeconds;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class MoveTimerIT {
    private final TestSetup testSetup;
    private final Builders builders;
    private final GameStateDao gameStateDao;

    @Guicy
    MoveTimerIT(TestSetup testSetup, Builders builders, GameStateDao gameStateDao) {
        this.testSetup = testSetup;
        this.builders = builders;
        this.gameStateDao = gameStateDao;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    void whiteSideTimerUpdated() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(10).build();
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getMoveTimer().getWhiteSideMillis()).isIn(bufferBelow5000(90_000L));
        assertThat(gameState.getMoveTimer().getBlackSideMillis()).isEqualTo(100_000L);
    }

    @Test
    void blackSideTimerUpdated() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(10).build();
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getMoveTimer().getBlackSideMillis()).isIn(bufferBelow5000(90_000L));
        assertThat(gameState.getMoveTimer().getWhiteSideMillis()).isEqualTo(100_000L);
    }

    @Test
    @Guicy
    void whiteSideTimerExpired(MockGameHistory mockGameHistory) {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(9_000L).blackSideMillis(100_000L)
                .secondsSinceStart(10).build();
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        mockGameHistory.clearEndGamePost(gameState);
    }

    @Test
    @Guicy
    void blackSideTimerExpired(MockGameHistory mockGameHistory) {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(17_000L)
                .secondsSinceStart(17).build();
        GameState gameState = this.testSetup.blackTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e7").to("e6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        mockGameHistory.clearEndGamePost(gameState);
    }

    @Test
    void updateMoveStart() {
        String[] board = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(100_000L).blackSideMillis(100_000L)
                .secondsSinceStart(10).build();
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(board, moveTimer);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getMoveTimer().getMoveStart()).isIn(bufferMinusFourSeconds(Instant.now()));
    }
}
