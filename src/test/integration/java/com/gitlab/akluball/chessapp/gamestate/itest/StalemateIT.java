package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.MoveResultDto;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class StalemateIT {
    private final TestSetup testSetup;
    private final Builders builders;

    @Guicy
    StalemateIT(TestSetup testSetup, Builders builders) {
        this.testSetup = testSetup;
        this.builders = builders;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    @Guicy
    void stalemate(MockGameHistory mockGameHistory) {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bN|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|wR|wR|--|wQ|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().queen().from("g2").to("f2").build());
        MoveResultDto moveResultDto = wsClient.waitForMoveResult();
        assertThat(moveResultDto.wasExecuted()).isTrue();
        assertThat(moveResultDto.getGameStatus()).isEqualTo(GameStatus.DRAW);
        mockGameHistory.clearEndGamePost(gameState);
    }

    @Test
    void kingLeftBreaks() {
        String[] before = {
                "8|--|--|--|--|bK|bR|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|bP|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e4").to("e3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void kingUpLeftBreaks() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|bK|--|--|--",
                "6|--|--|wB|--|bP|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|wP|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|wR|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e4").to("e5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void queenUpBreaks() {
        String[] before = {
                "8|--|bN|--|--|--|--|--|--",
                "7|bQ|bP|--|bP|--|--|--|--",
                "6|bK|bR|bP|wP|--|--|--|--",
                "5|wP|bP|wP|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|wP|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b3").to("b4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void queenUpRightBreaks() {
        String[] before = {
                "8|--|--|bR|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|bP|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|wP|--|--|--|--|--|--|--",
                "1|wQ|wK|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("a4").to("a3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void rookRightBreaks() {
        String[] before = {
                "8|bR|--|--|--|--|--|--|--",
                "7|bK|--|--|--|--|--|--|--",
                "6|bP|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|wP|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|wK|wR|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("a4").to("a5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void bishopDownRightBreaks() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|bP|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|bP|wP|bN|--|--|--|--|--",
                "2|wB|--|--|--|--|--|--|--",
                "1|wK|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b5").to("b4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void rookDownBreaks() {
        String[] before = {
                "8|--|--|bR|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|bP|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|wP|bN|--|--|--|--|--|--",
                "2|wR|wP|--|--|--|--|--|--",
                "1|--|wK|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("a5").to("a4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void bishopDownLeftBreaks() {
        String[] before = {
                "8|--|--|bB|bK|--|--|--|--",
                "7|--|--|--|bP|--|--|wN|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|wP|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d5").to("d6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void pawnAdvanceBreaks() {
        String[] before = {
                "8|--|--|bR|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|wP|--|--|--|--|--|--|--",
                "1|wK|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("c8").to("b8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void pawnAttackLeftBreaks() {
        String[] before = {
                "8|--|--|--|--|--|--|--|bK",
                "7|--|--|--|--|--|--|--|bP",
                "6|--|--|--|--|wB|--|--|wP",
                "5|--|--|--|--|--|--|wP|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("g5").to("g6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void pawnAttackRightBreaks() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|bP|--|--|--|--|--|--",
                "3|bP|--|--|--|--|--|--|--",
                "2|wP|--|--|bN|--|--|--|--",
                "1|wK|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b4").to("b3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void knightLeftUpBreaks() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|bN",
                "6|--|--|--|--|wN|bP|--|bK",
                "5|--|--|--|--|--|wP|bP|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|wP|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("g3").to("g4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void knightUpLeftBreaks() {
        String[] before = {
                "8|--|--|--|--|bK|bR|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|bN|--|--",
                "3|--|--|--|--|--|bP|--|--",
                "2|--|--|--|--|--|wP|--|wK",
                "1|--|--|--|--|--|--|--|wN",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("f8").to("g8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void knightUpRightBreaks() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|bK|--|bP|--|--|--|wR|--",
                "1|bN|--|wQ|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("g2").to("g3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void knightRightUpBreaks() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|bP|--|--|--|--|--|--",
                "3|--|wP|--|bN|--|--|--|bR",
                "2|--|--|--|--|--|--|--|--",
                "1|wN|wK|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("h3").to("h2").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void knightRightDownBreaks() {
        String[] before = {
                "8|bN|bK|--|--|--|--|--|--",
                "7|--|--|--|--|--|--|--|wR",
                "6|--|bP|--|wN|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|wP|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b4").to("b5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void knightDownRightBreaks() {
        String[] before = {
                "8|wN|--|bQ|--|bK|--|--|--",
                "7|wK|--|wP|bR|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().rook().from("d7").to("d6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void knightDownLeftBreaks() {
        String[] before = {
                "8|--|--|--|--|--|--|bK|bN",
                "7|--|--|--|--|wP|bP|--|--",
                "6|--|--|--|--|--|wP|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|wB|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().bishop().from("g4").to("f5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void knightLeftDownBreaks() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|wN",
                "7|--|--|--|--|--|--|bP|wK",
                "6|--|--|--|--|--|--|wP|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().king().from("e8").to("f8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void enPassantLeftBreaks() {
        String[] before = {
                "8|--|--|--|--|--|--|--|bK",
                "7|wR|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|bP|--|--",
                "3|--|--|--|--|--|wP|--|--",
                "2|--|--|--|--|wP|--|--|--",
                "1|--|--|--|--|wK|--|wR|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("e2").to("e4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }

    @Test
    void enPassantRightBreaks() {
        String[] before = {
                "8|--|bR|--|--|bK|--|--|--",
                "7|--|--|--|--|--|bP|--|--",
                "6|--|--|--|--|bP|--|--|--",
                "5|--|--|--|--|wP|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|bR",
                "1|wK|--|--|--|--|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.blackTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.blackSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("f7").to("f5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
    }
}
