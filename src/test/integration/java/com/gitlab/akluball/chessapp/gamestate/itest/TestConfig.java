package com.gitlab.akluball.chessapp.gamestate.itest;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Properties;

import static com.gitlab.akluball.chessapp.gamestate.itest.hook.DeployUtil.*;

@Singleton
public class TestConfig {
    public static final String GAMESTATE_PUBLICKEY_PROP = "chessapp.gamestate.publickey";
    public static final String GAMEHISTORY_PRIVATEKEY_PROP = "chessapp.gamehistory.privatekey";
    public static final String USER_PRIVATEKEY_PROP = "chessapp.user.privatekey";

    private final String gameStateUri;
    private final String gameHistoryHost;
    private final RSAPublicKey gameStatePublicKey;
    private final PrivateKey gameHistoryPrivateKey;
    private final PrivateKey userPrivateKey;
    private final String dbUri;
    private final String dbUser;
    private final String dbPassword;

    TestConfig() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        this.gameStateUri = getGameStateUri();
        this.gameHistoryHost = getContainerIp(GAMEHISTORY_CONTAINER);
        this.dbUri = getDbUri();
        this.dbUser = DB_USER;
        this.dbPassword = DB_PASSWORD;
        try (InputStream configStream = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("gamestate-test.properties")) {
            Properties config = new Properties();
            config.load(configStream);
            Base64.Decoder decoder = Base64.getDecoder();
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] gameStatePublicKeyBytes = decoder.decode(config.getProperty(GAMESTATE_PUBLICKEY_PROP));
            this.gameStatePublicKey = (RSAPublicKey) keyFactory
                    .generatePublic(new X509EncodedKeySpec(gameStatePublicKeyBytes));
            byte[] gameHistoryPrivateKeyBytes = decoder.decode(config.getProperty(GAMEHISTORY_PRIVATEKEY_PROP));
            this.gameHistoryPrivateKey = keyFactory
                    .generatePrivate(new PKCS8EncodedKeySpec(gameHistoryPrivateKeyBytes));
            byte[] userPrivateKeyBytes = decoder.decode(config.getProperty(USER_PRIVATEKEY_PROP));
            this.userPrivateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(userPrivateKeyBytes));
        }
    }

    public String gameStateUri() {
        return this.gameStateUri;
    }

    public String gameHistoryHost() {
        return this.gameHistoryHost;
    }

    public int gameHistoryPort() {
        return 1080;
    }

    public String dbUri() {
        return this.dbUri;
    }

    public String dbUser() {
        return this.dbUser;
    }

    public String dbPassword() {
        return this.dbPassword;
    }

    public RSAPublicKey gameStatePublicKey() {
        return this.gameStatePublicKey;
    }

    public PrivateKey gameHistoryPrivateKey() {
        return this.gameHistoryPrivateKey;
    }

    public PrivateKey userPrivateKey() {
        return this.userPrivateKey;
    }
}
