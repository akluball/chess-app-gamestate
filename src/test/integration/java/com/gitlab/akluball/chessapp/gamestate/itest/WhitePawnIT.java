package com.gitlab.akluball.chessapp.gamestate.itest;

import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;
import com.gitlab.akluball.chessapp.gamestate.itest.util.TestSetup;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class WhitePawnIT {
    private final TestSetup testSetup;
    private final Builders builders;
    private final GameStateDao gameStateDao;

    @Guicy
    WhitePawnIT(TestSetup testSetup, Builders builders, GameStateDao gameStateDao) {
        this.testSetup = testSetup;
        this.builders = builders;
        this.gameStateDao = gameStateDao;
    }

    @AfterEach
    void cleanup() {
        this.testSetup.cleanup();
    }

    @Test
    void forwardOne() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|wP|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b2").to("b3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|wP|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void horizontalOne() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|wP|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b2").to("c2").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void backwardOne() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|wP|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b2").to("b1").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void captureRight() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|bB",
                "2|--|--|--|--|--|--|wP|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("g2").to("h3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|wP",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void captureLeft() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|bQ|--|--",
                "2|--|--|--|--|--|--|wP|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("g2").to("f3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|wP|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void diagonalNoCapture() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|wP|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("g2").to("f3").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void advanceTwo() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|wP|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("g2").to("g4").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|wP|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void advanceTwoNotInitialMove() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|wP|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("g3").to("g5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void enPassant() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|wP|bP|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("c4").to("c5").build())
                .moveSummary(this.builders.moveSummary().pawn().from("d7").to("d5").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("c5").to("d6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|wP|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void enPassantBadRank() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|wP|bP|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("c3").to("c4").build())
                .moveSummary(this.builders.moveSummary().pawn().from("d5").to("d4").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("c4").to("d5").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void enPassantNotLastMoveAdvanceTwo() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|bP",
                "5|--|--|bP|wP|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("d3").to("d4").build())
                .moveSummary(this.builders.moveSummary().pawn().from("c7").to("c5").build())
                .moveSummary(this.builders.moveSummary().pawn().from("d4").to("d5").build())
                .moveSummary(this.builders.moveSummary().pawn().from("h7").to("h6").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d5").to("c6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void enPassantNotAdvanceTwo() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|--|--|--|--",
                "6|--|--|--|--|--|--|--|bP",
                "5|--|--|bP|wP|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("d3").to("d4").build())
                .moveSummary(this.builders.moveSummary().pawn().from("c7").to("c6").build())
                .moveSummary(this.builders.moveSummary().pawn().from("d4").to("d5").build())
                .moveSummary(this.builders.moveSummary().pawn().from("c6").to("c5").build()).build();
        GameState gameState = this.testSetup.persistedGameState(before, moveHistory);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d5").to("c6").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void promotion() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|wP|--|--|bP|bK|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b7").to("b8").promoteToQueen().build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isTrue();
        this.gameStateDao.refresh(gameState);
        String[] after = {
                "8|--|wQ|--|--|--|--|--|--",
                "7|--|--|--|--|bP|bK|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(after);
    }

    @Test
    void promotionNoPromotionPieceLetter() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|wP|--|--|bP|bK|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b7").to("b8").build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void promotionToPawn() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|wP|--|--|bP|bK|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b7").to("b8").promoteToPawn().build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void promotionToKing() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|wP|--|--|bP|bK|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|--|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("b7").to("b8").promoteToKing().build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void advanceBlockedByOpponent() {
        String[] before = {
                "8|--|--|--|--|--|--|--|--",
                "7|--|--|--|--|--|bK|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|bP|--|--|--|--",
                "3|--|--|--|wP|--|--|--|--",
                "2|--|--|--|--|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d3").to("d4").promoteToKing().build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }

    @Test
    void advanceTwoBlockedByOpponent() {
        String[] before = {
                "8|--|--|--|--|bK|--|--|--",
                "7|--|--|--|--|bP|--|--|--",
                "6|--|--|--|--|--|--|--|--",
                "5|--|--|--|--|--|--|--|--",
                "4|--|--|--|bP|--|--|--|--",
                "3|--|--|--|--|--|--|--|--",
                "2|--|--|--|wP|--|--|--|--",
                "1|--|--|--|--|wK|--|--|--",
                " |a |b |c |d |e |f |g |h "
        };
        GameState gameState = this.testSetup.whiteTurnPersistedGameState(before);
        GameStateWsClient wsClient = this.testSetup.whiteSideWsClient(gameState);
        wsClient.sendMove(this.builders.moveDto().pawn().from("d2").to("d4").promoteToKing().build());
        assertThat(wsClient.waitForMoveResult().wasExecuted()).isFalse();
        this.gameStateDao.refresh(gameState);
        assertThat(gameState.getBoard().toStringArray()).isEqualTo(before);
    }
}
