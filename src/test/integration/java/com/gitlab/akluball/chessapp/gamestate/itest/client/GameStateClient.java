package com.gitlab.akluball.chessapp.gamestate.itest.client;

import com.gitlab.akluball.chessapp.gamestate.itest.TestConfig;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.GameStateCreateDto;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

import static com.gitlab.akluball.chessapp.gamestate.itest.security.SecurityUtil.bearer;

public class GameStateClient {
    private final WebTarget target;

    @Inject
    GameStateClient(Client client, TestConfig testConfig) {
        this.target = client.target(testConfig.gameStateUri())
                .path("api")
                .path("gamestate");
    }

    public Response create(String token, String gameHistoryId, GameStateCreateDto gameStateCreateDto) {
        Invocation.Builder invocationBuilder = this.target.path(gameHistoryId)
                .request();
        bearer(invocationBuilder, token);
        return invocationBuilder.put(Entity.json(Objects.nonNull(gameStateCreateDto) ? gameStateCreateDto : "null"));
    }

    public Response create(String token, int gameHistoryId, GameStateCreateDto gameStateCreateDto) {
        return this.create(token, "" + gameHistoryId, gameStateCreateDto);
    }

    public Response get(String token, String gameHistoryId) {
        Invocation.Builder invocationBuilder = this.target.path(gameHistoryId).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response get(String token, int gameHistoryId) {
        return this.get(token, "" + gameHistoryId);
    }

    public Response getByParticipant(String token, String userId, String isTurn) {
        WebTarget target = this.target;
        if (Objects.nonNull(userId)) {
            target = target.queryParam("userId", userId);
        }
        if (Objects.nonNull(isTurn)) {
            target = target.queryParam("isTurn", isTurn);
        }
        Invocation.Builder invocationBuilder = target.request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response getByParticipant(String token, Integer userId, Boolean isTurn) {
        String userIdAsString = Objects.nonNull(userId) ? userId.toString() : null;
        String isTurnAsString = Objects.nonNull(isTurn) ? isTurn.toString() : null;
        return this.getByParticipant(token, userIdAsString, isTurnAsString);
    }
}
