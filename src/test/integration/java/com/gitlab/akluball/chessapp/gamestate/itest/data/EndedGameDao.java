package com.gitlab.akluball.chessapp.gamestate.itest.data;

import com.gitlab.akluball.chessapp.gamestate.itest.model.EndedGame;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class EndedGameDao {
    private final EntityManager entityManager;

    @Inject
    EndedGameDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EndedGame findByGameHistoryId(int gameHistoryId) {
        return this.entityManager.find(EndedGame.class, gameHistoryId);
    }
}
