package com.gitlab.akluball.chessapp.gamestate.itest.data;

import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.stream.Stream;

public class GameStateDao {
    private final EntityManager entityManager;

    @Inject
    GameStateDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(GameState... gameStates) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        Stream.of(gameStates).forEach(this.entityManager::persist);
        tx.commit();
    }

    public GameState findByGameHistoryId(int gameHistoryId) {
        return this.entityManager.find(GameState.class, gameHistoryId);
    }

    public void refresh(GameState gameState) {
        this.entityManager.refresh(gameState);
    }

    public void detach(GameState gameState) {
        this.entityManager.detach(gameState);
    }

    public void delete(GameState gameState) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.remove(gameState);
        tx.commit();
    }
}
