package com.gitlab.akluball.chessapp.gamestate.itest.hook;

import com.gitlab.akluball.chessapp.gamestate.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.param.ParameterResolverModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import static com.gitlab.akluball.chessapp.gamestate.itest.hook.DeployUtil.*;

public class Before {
    public static void main(String[] args) {
        String gameStateWar = System.getenv("CHESSAPP_GAMESTATE_WAR");
        copyToContainer(gameStateWar, PAYARA_CONTAINER, CONTAINER_GAMESTATE_WAR);
        containerAsadmin("redeploy", "--name", GAMESTATE_DEPLOY_NAME, CONTAINER_GAMESTATE_WAR);
        Injector injector = Guice.createInjector(new ParameterResolverModule());
        injector.getInstance(MockGameHistory.class).reset();
        EntityManager entityManager = injector.getInstance(EntityManager.class);
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.flush();
        tx.commit();
    }
}
