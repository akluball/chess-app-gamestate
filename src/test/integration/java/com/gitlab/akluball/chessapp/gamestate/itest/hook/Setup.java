package com.gitlab.akluball.chessapp.gamestate.itest.hook;

import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;

import static com.gitlab.akluball.chessapp.gamestate.itest.TestConfig.*;
import static com.gitlab.akluball.chessapp.gamestate.itest.hook.DeployUtil.*;
import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.sleepMillis;

public class Setup {
    public static void main(String[] args) throws IOException, InterruptedException, NoSuchAlgorithmException {
        // keys
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        Base64.Encoder encoder = Base64.getEncoder();
        // gamestate keys
        KeyPair gameStateKeyPair = keyPairGenerator.generateKeyPair();
        String gameStatePrivateKey = new String(encoder.encode(gameStateKeyPair.getPrivate().getEncoded()));
        String gameStatePublicKey = new String(encoder.encode(gameStateKeyPair.getPublic().getEncoded()));
        // gamehistory keys
        KeyPair gameHistoryKeyPair = keyPairGenerator.generateKeyPair();
        String gameHistoryPrivateKey = new String(encoder.encode(gameHistoryKeyPair.getPrivate().getEncoded()));
        String gameHistoryPublicKey = new String(encoder.encode(gameHistoryKeyPair.getPublic().getEncoded()));
        // user keys
        KeyPair userKeyPair = keyPairGenerator.generateKeyPair();
        String userPrivateKey = new String(encoder.encode(userKeyPair.getPrivate().getEncoded()));
        String userPublicKey = new String(encoder.encode(userKeyPair.getPublic().getEncoded()));
        // store keys for test runs
        Properties testProps = new Properties();
        testProps.put(GAMESTATE_PUBLICKEY_PROP, gameStatePublicKey);
        testProps.put(GAMEHISTORY_PRIVATEKEY_PROP, gameHistoryPrivateKey);
        testProps.put(USER_PRIVATEKEY_PROP, userPrivateKey);
        FileWriter testPropsWriter = new FileWriter(System.getenv("GAMESTATE_TEST_PROPERTIES"));
        testProps.store(testPropsWriter, null);
        // containers
        new ProcessBuilder("docker", "network", "create", NETWORK_NAME)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", DB_CONTAINER, "--network", NETWORK_NAME,
                "--env", String.format("POSTGRES_DB=%s", DB_NAME),
                "--env", String.format("POSTGRES_USER=%s", DB_USER),
                "--env", String.format("POSTGRES_PASSWORD=%s", DB_PASSWORD),
                "--rm", "--detach",
                POSTGRES_IMAGE,
                "-c", "max_connections=200")
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", GAMEHISTORY_CONTAINER, "--network", NETWORK_NAME,
                "--detach", "--rm", MOCKSERVER_IMAGE)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", PAYARA_CONTAINER, "--network", NETWORK_NAME,
                "--env", String.format("DB_SERVER=%s", getDbIp()),
                "--env", String.format("DB_NAME=%s", DB_NAME),
                "--env", String.format("DB_USER=%s", DB_USER),
                "--env", String.format("DB_PASSWORD=%s", DB_PASSWORD),
                "--env", String.format("CHESSAPP_GAMEHISTORY_URI=%s", getGameHistoryUri()),
                "--env", String.format("CHESSAPP_GAMESTATE_PRIVATEKEY=%s", gameStatePrivateKey),
                "--env", String.format("CHESSAPP_GAMEHISTORY_PUBLICKEY=%s", gameHistoryPublicKey),
                "--env", String.format("CHESSAPP_USER_PUBLICKEY=%s", userPublicKey),
                "--rm", "--detach", PAYARA_IMAGE)
                .inheritIO().start().waitFor();
        String gameStateWar = System.getenv("CHESSAPP_GAMESTATE_WAR");
        copyToContainer(gameStateWar, PAYARA_CONTAINER, CONTAINER_GAMESTATE_WAR);
        // wait for payara to come up
        int uptimeAttempts = 6;
        while (uptimeAttempts > 0 && !containerAsadmin("uptime")) {
            sleepMillis(500);
            uptimeAttempts--;
        }
        containerAsadmin("deploy", "--name", GAMESTATE_DEPLOY_NAME, CONTAINER_GAMESTATE_WAR);
    }
}
