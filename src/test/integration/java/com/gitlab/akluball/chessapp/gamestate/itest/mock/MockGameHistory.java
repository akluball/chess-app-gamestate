package com.gitlab.akluball.chessapp.gamestate.itest.mock;

import com.gitlab.akluball.chessapp.gamestate.itest.TestConfig;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.EndGameDto;
import com.nimbusds.jwt.SignedJWT;
import org.mockserver.client.MockServerClient;
import org.mockserver.matchers.MatchType;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.JsonBody;
import org.mockserver.verify.VerificationTimes;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.bind.Jsonb;

import static com.gitlab.akluball.chessapp.gamestate.itest.mock.MockUtil.extractToken;
import static com.gitlab.akluball.chessapp.gamestate.itest.mock.MockUtil.verifyWithRetry;

@Singleton
public class MockGameHistory {
    private final MockServerClient client;
    private final Jsonb jsonb;

    @Inject
    MockGameHistory(Jsonb jsonb, TestConfig testConfig) {
        this.jsonb = jsonb;
        this.client = new MockServerClient(testConfig.gameHistoryHost(), testConfig.gameHistoryPort());
    }

    private static HttpRequest createEndGamePost(int gameHistoryId) {
        return HttpRequest.request().withMethod("POST")
                .withPath(String.format("/api/gamehistory/%s/end", gameHistoryId));
    }

    public void reset() {
        this.client.reset();
    }

    public void expectEndGamePost(GameState gameState) {
        HttpRequest req = createEndGamePost(gameState.getGameHistoryId());
        HttpResponse res = HttpResponse.response().withStatusCode(204);
        this.client.when(req).respond(res);
    }

    public void expectEndGamePostFail(GameState gameState) {
        HttpRequest req = createEndGamePost(gameState.getGameHistoryId());
        HttpResponse res = HttpResponse.response().withStatusCode(400);
        this.client.when(req).respond(res);
    }

    public SignedJWT retrieveEndGamePostToken(GameState gameState) {
        HttpRequest req = createEndGamePost(gameState.getGameHistoryId());
        return extractToken(this.client, req);
    }

    public void verifyEndGamePost(GameState gameState) {
        HttpRequest req = createEndGamePost(gameState.getGameHistoryId());
        verifyWithRetry(this.client, req);
        this.client.clear(req);
    }

    public void verifyEndGamePost(GameState gameState, EndGameDto endGameDto) {
        JsonBody body = JsonBody.json(this.jsonb.toJson(endGameDto), MatchType.ONLY_MATCHING_FIELDS);
        HttpRequest req = createEndGamePost(gameState.getGameHistoryId()).withBody(body);
        verifyWithRetry(this.client, req);
        this.client.clear(req);
    }

    public void verifyNoEndGamePost(GameState gameState) {
        HttpRequest req = createEndGamePost(gameState.getGameHistoryId());
        this.client.verify(req, VerificationTimes.exactly(0));
        this.client.clear(req);
    }

    public void clearEndGamePost(GameState gameState) {
        this.client.clear(createEndGamePost(gameState.getGameHistoryId()));
    }
}
