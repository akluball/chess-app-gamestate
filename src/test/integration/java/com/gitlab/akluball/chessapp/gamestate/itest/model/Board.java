package com.gitlab.akluball.chessapp.gamestate.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Embeddable
@Access(AccessType.FIELD)
public class Board {
    @ElementCollection
    @MapKeyColumn(name = "position")
    @MapKeyEnumerated(EnumType.STRING)
    @Column(name = "piece")
    @Enumerated(EnumType.STRING)
    Map<Position, Piece> positionToPiece = new HashMap<>();

    public String[] toStringArray() {
        String[][] rows = new String[8][8];
        positionToPiece.forEach((position, value) -> {
            rows[position.getRankAsInt() - 1][position.getFileAsInt() - 1] = value.toStringRepr();
        });
        String[] asStringArray = new String[9];
        for (int i = 0; i < 8; i++) {
            asStringArray[i] = String.format("%s|", 8 - i) + Stream.of(rows[7 - i])
                    .map(pieceRepr -> (Objects.isNull(pieceRepr)) ? "--" : pieceRepr)
                    .collect(Collectors.joining("|"));
        }
        asStringArray[8] = " |a |b |c |d |e |f |g |h ";
        return asStringArray;
    }

    public static class Builder {
        private final Board board;

        Builder() {
            this.board = new Board();
        }

        public Builder place(Position position, Piece piece) {
            this.board.positionToPiece.put(position, piece);
            return this;
        }

        public Board build() {
            return this.board;
        }
    }
}
