package com.gitlab.akluball.chessapp.gamestate.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.util.List;

@Entity
@Access(AccessType.FIELD)
public class EndedGame {
    @Id
    private int gameHistoryId;
    @Enumerated(EnumType.STRING)
    private ResultTag resultTag;
    @Enumerated(EnumType.STRING)
    private TerminationTag terminationTag;
    @ElementCollection
    private List<MoveSummary> moveSummaries;

    public List<MoveSummary> getMoveSummaries() {
        return moveSummaries;
    }
}
