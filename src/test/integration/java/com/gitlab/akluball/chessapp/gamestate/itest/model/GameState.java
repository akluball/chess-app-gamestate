package com.gitlab.akluball.chessapp.gamestate.itest.model;

import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;

import javax.inject.Inject;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Arrays;

@Entity
@Access(AccessType.FIELD)
public class GameState {
    @Id
    private int gameHistoryId;
    private int whiteSideUserId;
    private int blackSideUserId;
    @Embedded
    private MoveTimer moveTimer;
    @Embedded
    private Board board;
    @Embedded
    private MoveHistory moveHistory;

    public int getGameHistoryId() {
        return this.gameHistoryId;
    }

    public int getWhiteSideUserId() {
        return whiteSideUserId;
    }

    public int getBlackSideUserId() {
        return blackSideUserId;
    }

    public MoveTimer getMoveTimer() {
        return this.moveTimer;
    }

    public Board getBoard() {
        return board;
    }

    public MoveSummary getFreshestMoveSummary() {
        return moveHistory.getFreshestMoveSummary();
    }

    public static class Builder {
        private final Builders builders;
        private final GameState gameState;

        @Inject
        Builder(Builders builders) {
            this.builders = builders;
            this.gameState = new GameState();
        }

        public Builder gameHistoryId(int gameHistoryId) {
            this.gameState.gameHistoryId = gameHistoryId;
            return this;
        }

        public Builder whiteSideUserId(int whiteSideUserId) {
            this.gameState.whiteSideUserId = whiteSideUserId;
            return this;
        }

        public Builder blackSideUserId(int blackSideUserId) {
            this.gameState.blackSideUserId = blackSideUserId;
            return this;
        }

        public Builder moveTimer(MoveTimer moveTimer) {
            this.gameState.moveTimer = moveTimer;
            return this;
        }

        public Builder board(String[] boardAsString) {
            // remove bottom row (the file axis)
            boardAsString = Arrays.copyOfRange(boardAsString, 0, boardAsString.length - 1);
            String[][] rows = Arrays.stream(boardAsString)
                    .map(row -> row.substring(2)) // remove first two positions of each string (the rank axis)
                    .map(row -> row.split("\\|"))
                    .toArray(String[][]::new);
            Board.Builder boardBuilder = this.builders.board();
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    String pieceRepr = rows[i][j];
                    if ("--".equals(pieceRepr)) {
                        continue;
                    }
                    char file = (char) ('a' + j);
                    int rank = 8 - i;
                    Position position = Position.fromFileAndRank(file, rank);
                    Piece piece = Piece.fromStringRepr(pieceRepr);
                    boardBuilder.place(position, piece);
                }
            }
            this.gameState.board = boardBuilder.build();
            return this;
        }

        public Builder moveHistory(MoveHistory moveHistory) {
            this.gameState.moveHistory = moveHistory;
            return this;
        }

        public GameState build() {
            return this.gameState;
        }
    }
}
