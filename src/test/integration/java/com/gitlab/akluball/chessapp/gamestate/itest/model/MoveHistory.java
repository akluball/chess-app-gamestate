package com.gitlab.akluball.chessapp.gamestate.itest.model;

import com.gitlab.akluball.chessapp.gamestate.itest.util.Builders;

import javax.inject.Inject;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.OrderColumn;
import java.util.ArrayList;
import java.util.List;

@Embeddable
@Access(AccessType.FIELD)
public class MoveHistory {
    @ElementCollection
    @OrderColumn
    private List<MoveSummary> moveSummaries = new ArrayList<>();

    public MoveSummary getFreshestMoveSummary() {
        return this.moveSummaries.isEmpty()
                ? null
                : this.moveSummaries.get(this.moveSummaries.size() - 1);
    }

    public static class Builder {
        private final Builders builders;
        private final MoveHistory moveHistory;

        @Inject
        Builder(Builders builders) {
            this.builders = builders;
            this.moveHistory = new MoveHistory();
        }

        public Builder moveSummary(MoveSummary moveSummary) {
            this.moveHistory.moveSummaries.add(moveSummary);
            return this;
        }

        public MoveHistory build() {
            return this.moveHistory;
        }
    }
}
