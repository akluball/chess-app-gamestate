package com.gitlab.akluball.chessapp.gamestate.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Objects;

import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.secondsToMillis;

@Embeddable
@Access(AccessType.FIELD)
public class MoveSummary {
    private String pieceLetter;
    private String promotionPieceLetter;
    @Enumerated(EnumType.STRING)
    private Position source;
    @Enumerated(EnumType.STRING)
    private Position target;
    private boolean isCapture;
    @Enumerated(EnumType.STRING)
    private SourceDisambiguation sourceDisambiguation;
    private long durationMillis;
    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    public String getPieceLetter() {
        return this.pieceLetter;
    }

    public String getPromotionPieceLetter() {
        return promotionPieceLetter;
    }

    public Position getSource() {
        return source;
    }

    public Position getTarget() {
        return target;
    }

    public boolean isCapture() {
        return this.isCapture;
    }

    public SourceDisambiguation getSourceDisambiguation() {
        return sourceDisambiguation;
    }

    public long getDurationMillis() {
        return durationMillis;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public Dto toDto() {
        Dto dto = new Dto();
        dto.pieceLetter = this.pieceLetter;
        dto.source = this.source;
        dto.target = this.target;
        dto.isCapture = this.isCapture;
        dto.sourceDisambiguation = this.sourceDisambiguation;
        dto.durationMillis = this.durationMillis;
        dto.gameStatus = this.gameStatus;
        return dto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MoveSummary that = (MoveSummary) o;
        return isCapture == that.isCapture &&
                durationMillis == that.durationMillis &&
                Objects.equals(pieceLetter, that.pieceLetter) &&
                source == that.source &&
                target == that.target &&
                sourceDisambiguation == that.sourceDisambiguation &&
                gameStatus == that.gameStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pieceLetter, source, target, isCapture, sourceDisambiguation, durationMillis, gameStatus);
    }

    public static class Builder {
        private final MoveSummary moveSummary;

        Builder() {
            this.moveSummary = new MoveSummary();
        }

        public Builder pieceLetter(String pieceLetter) {
            this.moveSummary.pieceLetter = pieceLetter;
            return this;
        }

        public Builder pawn() {
            return this.pieceLetter("P");
        }

        public Builder rook() {
            return this.pieceLetter("R");
        }

        public Builder king() {
            return this.pieceLetter("K");
        }

        public Builder from(String movetextRepr) {
            this.moveSummary.source = Position.fromMovetextRepr(movetextRepr);
            return this;
        }

        public Builder to(String movetextRepr) {
            this.moveSummary.target = Position.fromMovetextRepr(movetextRepr);
            return this;
        }

        public Builder sourceDisambiguation(SourceDisambiguation sourceDisambiguation) {
            this.moveSummary.sourceDisambiguation = sourceDisambiguation;
            return this;
        }

        public Builder seconds(long seconds) {
            this.moveSummary.durationMillis = secondsToMillis(seconds);
            return this;
        }

        public Builder millis(long millis) {
            this.moveSummary.durationMillis = millis;
            return this;
        }

        public Builder gameStatus(GameStatus gameStatus) {
            this.moveSummary.gameStatus = gameStatus;
            return this;
        }

        public MoveSummary build() {
            return this.moveSummary;
        }
    }

    public static class Dto {
        private String pieceLetter;
        private String promotionPieceLetter;
        private Position source;
        private Position target;
        private boolean isCapture;
        private SourceDisambiguation sourceDisambiguation;
        private long durationMillis;
        private GameStatus gameStatus;

        public String getPieceLetter() {
            return pieceLetter;
        }

        public String getPromotionPieceLetter() {
            return promotionPieceLetter;
        }

        public Position getSource() {
            return source;
        }

        public Position getTarget() {
            return target;
        }

        public boolean isCapture() {
            return isCapture;
        }

        public SourceDisambiguation getSourceDisambiguation() {
            return sourceDisambiguation;
        }

        public long getDurationMillis() {
            return durationMillis;
        }

        public GameStatus getGameStatus() {
            return gameStatus;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Dto dto = (Dto) o;
            return isCapture == dto.isCapture &&
                    durationMillis == dto.durationMillis &&
                    Objects.equals(pieceLetter, dto.pieceLetter) &&
                    Objects.equals(promotionPieceLetter, dto.promotionPieceLetter) &&
                    source == dto.source &&
                    target == dto.target &&
                    sourceDisambiguation == dto.sourceDisambiguation &&
                    gameStatus == dto.gameStatus;
        }

        @Override
        public int hashCode() {
            return Objects
                    .hash(pieceLetter, promotionPieceLetter, source, target, isCapture, sourceDisambiguation,
                            durationMillis, gameStatus);
        }
    }
}
