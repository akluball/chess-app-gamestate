package com.gitlab.akluball.chessapp.gamestate.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.time.Instant;

@Embeddable
@Access(AccessType.FIELD)
public class MoveTimer {
    @Embedded
    @AttributeOverride(name = "epochSecond", column = @Column(name = "moveStartEpochSecond"))
    @AttributeOverride(name = "nanoComponent", column = @Column(name = "moveStartNanoComponent"))
    private PersistableInstant moveStart;
    private long whiteSideMillis;
    private long blackSideMillis;

    public Instant getMoveStart() {
        return this.moveStart.toInstant();
    }

    public long getWhiteSideMillis() {
        return whiteSideMillis;
    }

    public long getBlackSideMillis() {
        return blackSideMillis;
    }

    public static class Builder {
        private final MoveTimer moveTimer;

        Builder() {
            this.moveTimer = new MoveTimer();
        }

        public Builder secondsSinceStart(int seconds) {
            this.moveTimer.moveStart = PersistableInstant.nowMinusSeconds(seconds);
            return this;
        }

        public Builder whiteSideMillis(long millis) {
            this.moveTimer.whiteSideMillis = millis;
            return this;
        }

        public Builder blackSideMillis(long millis) {
            this.moveTimer.blackSideMillis = millis;
            return this;
        }

        public MoveTimer build() {
            return this.moveTimer;
        }
    }
}
