package com.gitlab.akluball.chessapp.gamestate.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import java.time.Instant;

@Embeddable
@Access(AccessType.FIELD)
public class PersistableInstant {
    private long epochSecond;
    private int nanoComponent;

    static PersistableInstant nowMinusSeconds(int seconds) {
        Instant instant = Instant.now().minusSeconds(seconds);
        PersistableInstant persistableInstant = new PersistableInstant();
        persistableInstant.epochSecond = instant.getEpochSecond();
        persistableInstant.nanoComponent = instant.getNano();
        return persistableInstant;
    }

    public Instant toInstant() {
        return Instant.ofEpochSecond(this.epochSecond, this.nanoComponent);
    }
}
