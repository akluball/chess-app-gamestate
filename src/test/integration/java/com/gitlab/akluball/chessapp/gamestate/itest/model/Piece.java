package com.gitlab.akluball.chessapp.gamestate.itest.model;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;
import java.util.HashMap;

@JsonbTypeAdapter(Piece.Adapter.class)
public enum Piece {
    WHITE_PAWN, WHITE_ROOK, WHITE_KNIGHT, WHITE_BISHOP, WHITE_QUEEN, WHITE_KING,
    BLACK_PAWN, BLACK_ROOK, BLACK_KNIGHT, BLACK_BISHOP, BLACK_QUEEN, BLACK_KING;

    private static HashMap<String, Piece> STRING_REPR_TO_PIECE;

    static {
        STRING_REPR_TO_PIECE = new HashMap<>();
        for (Piece piece : Piece.values()) {
            String stringRepr = String.format("%s%s", (piece.isWhiteSidePiece) ? "w" : "b", piece.pieceLetter);
            STRING_REPR_TO_PIECE.put(stringRepr, piece);
        }
    }

    private final boolean isWhiteSidePiece;
    private final String pieceLetter;

    Piece() {
        this.isWhiteSidePiece = this.name().startsWith("WHITE");
        this.pieceLetter = (this.name().endsWith("KNIGHT")) ? "N" : String.valueOf(this.name().charAt(6));
    }

    public static Piece fromStringRepr(String stringRepr) {
        return STRING_REPR_TO_PIECE.get(stringRepr);
    }

    public String toStringRepr() {
        return String.format("%s%s", (this.isWhiteSidePiece) ? "w" : "b", this.pieceLetter);
    }

    public static class Adapter implements JsonbAdapter<Piece, String> {
        @Override
        public String adaptToJson(Piece piece) throws Exception {
            return piece.toStringRepr();
        }

        @Override
        public Piece adaptFromJson(String asString) throws Exception {
            return Piece.fromStringRepr(asString);
        }
    }
}
