package com.gitlab.akluball.chessapp.gamestate.itest.model;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

@JsonbTypeAdapter(Position.Adapter.class)
public enum Position {
    A8, B8, C8, D8, E8, F8, G8, H8,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A1, B1, C1, D1, E1, F1, G1, H1;

    private final int fileAsInt;
    private final int rankAsInt;

    Position() {
        this.fileAsInt = this.name().charAt(0) - 'A' + 1;
        this.rankAsInt = this.name().charAt(1) - '1' + 1;
    }

    static Position fromFileAndRank(char file, int rank) {
        String name = String.format("%s%s", String.valueOf(file).toUpperCase(), rank);
        return Position.valueOf(name);
    }

    public static Position fromMovetextRepr(String movetextRepr) {
        return Position.valueOf(movetextRepr.toUpperCase());
    }

    public String toMovetextRepr() {
        return this.name().toLowerCase();
    }

    int getFileAsInt() {
        return this.fileAsInt;
    }

    int getRankAsInt() {
        return this.rankAsInt;
    }

    public static class Adapter implements JsonbAdapter<Position, String> {
        @Override
        public String adaptToJson(Position position) throws Exception {
            return position.toMovetextRepr();
        }

        @Override
        public Position adaptFromJson(String stringRepr) throws Exception {
            return Position.fromMovetextRepr(stringRepr);
        }
    }
}
