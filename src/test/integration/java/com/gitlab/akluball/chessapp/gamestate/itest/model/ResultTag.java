package com.gitlab.akluball.chessapp.gamestate.itest.model;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;
import java.util.HashMap;

@JsonbTypeAdapter(ResultTag.Adapter.class)
public enum ResultTag {
    WHITE_WINS("1-0"),
    BLACK_WINS("0-1"),
    DRAW("1/2-1/2");

    private static final HashMap<String, ResultTag> STRING_TO_RESULT_TAG;

    static {
        STRING_TO_RESULT_TAG = new HashMap<>();
        for (ResultTag resultTag : ResultTag.values()) {
            STRING_TO_RESULT_TAG.put(resultTag.asString, resultTag);
        }
    }

    private final String asString;

    ResultTag(String asString) {
        this.asString = asString;
    }

    public static class Adapter implements JsonbAdapter<ResultTag, String> {
        @Override
        public String adaptToJson(ResultTag resultTag) throws Exception {
            return resultTag.asString;
        }

        @Override
        public ResultTag adaptFromJson(String asString) throws Exception {
            return STRING_TO_RESULT_TAG.get(asString);
        }
    }
}
