package com.gitlab.akluball.chessapp.gamestate.itest.security;

import com.gitlab.akluball.chessapp.gamestate.itest.TestConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;

import static com.gitlab.akluball.chessapp.gamestate.itest.security.SecurityUtil.generateRsaPrivateKey;

@Singleton
public class GameHistorySecurity {
    private final String token;
    private final String badRoleToken;
    private final String badServiceNameToken;
    private final String badSignatureToken;
    private final String expiredToken;

    @Inject
    GameHistorySecurity(TestConfig testConfig) throws JOSEException {
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        JWTClaimsSet claims = new JWTClaimsSet.Builder()
                .expirationTime(Date.from(Instant.now().plusSeconds(60)))
                .subject(ServiceName.GAMEHISTORY.asString())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.SERVICE.asString()))
                .build();
        JWTClaimsSet badRoleClaims = new JWTClaimsSet.Builder()
                .expirationTime(Date.from(Instant.now().plusSeconds(60)))
                .subject(ServiceName.GAMEHISTORY.asString())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList("bad-role"))
                .build();
        JWTClaimsSet badServiceNameClaims = new JWTClaimsSet.Builder()
                .expirationTime(Date.from(Instant.now().plusSeconds(60)))
                .subject("bad-service-name")
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.SERVICE.asString()))
                .build();
        JWTClaimsSet expiredClaims = new JWTClaimsSet.Builder()
                .expirationTime(Date.from(Instant.now().minusSeconds(60)))
                .subject(ServiceName.GAMEHISTORY.asString())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.SERVICE.asString()))
                .build();
        SignedJWT token = new SignedJWT(header, claims);
        SignedJWT badRoleToken = new SignedJWT(header, badRoleClaims);
        SignedJWT badServiceNameToken = new SignedJWT(header, badServiceNameClaims);
        SignedJWT badSignatureToken = new SignedJWT(header, claims);
        SignedJWT expiredToken = new SignedJWT(header, expiredClaims);
        RSASSASigner signer = new RSASSASigner(testConfig.gameHistoryPrivateKey());
        RSASSASigner badSigner = new RSASSASigner(generateRsaPrivateKey());
        token.sign(signer);
        badRoleToken.sign(signer);
        badServiceNameToken.sign(signer);
        badSignatureToken.sign(badSigner);
        expiredToken.sign(signer);
        this.token = token.serialize();
        this.badRoleToken = badRoleToken.serialize();
        this.badServiceNameToken = badServiceNameToken.serialize();
        this.badSignatureToken = badSignatureToken.serialize();
        this.expiredToken = expiredToken.serialize();
    }

    public String getToken() {
        return this.token;
    }

    public String getBadRoleToken() {
        return this.badRoleToken;
    }

    public String getBadServiceNameToken() {
        return badServiceNameToken;
    }

    public String getBadSignatureToken() {
        return this.badSignatureToken;
    }

    public String getExpiredToken() {
        return this.expiredToken;
    }
}
