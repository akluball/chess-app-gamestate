package com.gitlab.akluball.chessapp.gamestate.itest.security;

import com.gitlab.akluball.chessapp.gamestate.itest.TestConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;

import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.asRuntime;

public class GameStateSecurity {
    private final RSASSAVerifier verifier;

    @Inject
    GameStateSecurity(TestConfig testConfig) {
        this.verifier = new RSASSAVerifier(testConfig.gameStatePublicKey());
    }

    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
    }
}
