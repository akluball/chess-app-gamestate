package com.gitlab.akluball.chessapp.gamestate.itest.security;

public enum ServiceName {
    GAMESTATE("gamestate"), GAMEHISTORY("gamehistory");

    private final String asString;

    ServiceName(String asString) {
        this.asString = asString;
    }

    public String asString() {
        return this.asString;
    }
}
