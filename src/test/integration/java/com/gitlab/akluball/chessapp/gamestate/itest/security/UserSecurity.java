package com.gitlab.akluball.chessapp.gamestate.itest.security;

import com.gitlab.akluball.chessapp.gamestate.itest.TestConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;

import static com.gitlab.akluball.chessapp.gamestate.itest.security.SecurityUtil.generateRsaPrivateKey;
import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.asRuntime;

@Singleton
public class UserSecurity {
    private final RSASSASigner signer;
    private final RSASSASigner badSigner;

    @Inject
    UserSecurity(TestConfig testConfig) {
        this.signer = new RSASSASigner(testConfig.userPrivateKey());
        this.badSigner = new RSASSASigner(generateRsaPrivateKey());
    }

    private static String tokenFor(int userId, String role, JWSSigner jwsSigner, Instant expiration) {
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256)
                .build();
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject("" + userId)
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(role))
                .expirationTime(Date.from(expiration))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        try {
            token.sign(jwsSigner);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        return token.serialize();
    }

    public String tokenFor(int userId) {
        return tokenFor(userId, BearerRole.USER.asString(), this.signer, Instant.now().plusSeconds(60));
    }

    public String badRoleTokenFor(int userId) {
        return tokenFor(userId, "bad-role", this.signer, Instant.now().plusSeconds(60));
    }

    public String badSignatureTokenFor(int userId) {
        return tokenFor(userId, BearerRole.USER.asString(), this.badSigner, Instant.now().plusSeconds(60));
    }

    public String expiredTokenFor(int userId) {
        return tokenFor(userId, BearerRole.USER.asString(), this.signer, Instant.now().minusSeconds(60));
    }
}
