package com.gitlab.akluball.chessapp.gamestate.itest.transfer;

import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamestate.itest.model.ResultTag;
import com.gitlab.akluball.chessapp.gamestate.itest.model.TerminationTag;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EndGameDto {
    private ResultTag resultTag;
    private TerminationTag terminationTag;
    private List<MoveSummary> moveSummaries;

    public static class Builder {
        private final EndGameDto endGameDto;

        Builder() {
            this.endGameDto = new EndGameDto();
        }

        public Builder blackInCheckmate() {
            this.endGameDto.resultTag = ResultTag.WHITE_WINS;
            this.endGameDto.terminationTag = TerminationTag.NORMAL;
            return this;
        }

        public Builder whiteInCheckmate() {
            this.endGameDto.resultTag = ResultTag.BLACK_WINS;
            this.endGameDto.terminationTag = TerminationTag.NORMAL;
            return this;
        }

        public Builder blackTimerExpired() {
            this.endGameDto.resultTag = ResultTag.WHITE_WINS;
            this.endGameDto.terminationTag = TerminationTag.TIME_FORFEIT;
            return this;
        }

        public Builder whiteTimerExpired() {
            this.endGameDto.resultTag = ResultTag.BLACK_WINS;
            this.endGameDto.terminationTag = TerminationTag.TIME_FORFEIT;
            return this;
        }

        public Builder draw() {
            this.endGameDto.resultTag = ResultTag.DRAW;
            this.endGameDto.terminationTag = TerminationTag.NORMAL;
            return this;
        }

        public Builder moveSummary(MoveSummary moveSummary) {
            if (Objects.isNull(this.endGameDto.moveSummaries)) {
                this.endGameDto.moveSummaries = new ArrayList<>();
            }
            this.endGameDto.moveSummaries.add(moveSummary);
            return this;
        }

        public EndGameDto build() {
            return this.endGameDto;
        }
    }
}
