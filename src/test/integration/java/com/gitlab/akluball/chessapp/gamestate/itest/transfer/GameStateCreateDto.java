package com.gitlab.akluball.chessapp.gamestate.itest.transfer;

public class GameStateCreateDto {
    private Integer whiteSideUserId;
    private Integer blackSideUserId;
    private Long millisPerPlayer;

    public int getWhiteSideUserId() {
        return whiteSideUserId;
    }

    public int getBlackSideUserId() {
        return blackSideUserId;
    }

    public static class Builder {
        private final GameStateCreateDto gameStateCreateDto;

        Builder() {
            this.gameStateCreateDto = new GameStateCreateDto();
        }

        public Builder whiteSideUserId(Integer whiteSideUserId) {
            this.gameStateCreateDto.whiteSideUserId = whiteSideUserId;
            return this;
        }

        public Builder blackSideUserId(Integer blackSideUserId) {
            this.gameStateCreateDto.blackSideUserId = blackSideUserId;
            return this;
        }

        public Builder millisPerPlayer(Long millis) {
            this.gameStateCreateDto.millisPerPlayer = millis;
            return this;
        }

        public GameStateCreateDto build() {
            return this.gameStateCreateDto;
        }
    }
}
