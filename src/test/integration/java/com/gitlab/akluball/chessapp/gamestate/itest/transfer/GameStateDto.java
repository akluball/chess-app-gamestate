package com.gitlab.akluball.chessapp.gamestate.itest.transfer;

import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamestate.itest.model.Piece;
import com.gitlab.akluball.chessapp.gamestate.itest.model.Position;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameStateDto {
    private int gameHistoryId;
    private int whiteSideUserId;
    private int blackSideUserId;
    private List<MoveSummary.Dto> moveSummaries;
    private Map<String, String> positionToPiece;
    private long whiteSideMillisRemaining;
    private long blackSideMillisRemaining;

    public int getGameHistoryId() {
        return gameHistoryId;
    }

    public int getWhiteSideUserId() {
        return whiteSideUserId;
    }

    public int getBlackSideUserId() {
        return blackSideUserId;
    }

    public List<MoveSummary.Dto> getMoveSummaries() {
        return moveSummaries;
    }

    public HashMap<Position, Piece> getPositionToPiece() {
        HashMap<Position, Piece> deserialized = new HashMap<>();
        this.positionToPiece.forEach((positionAsString, pieceAsString) -> {
            deserialized.put(Position.fromMovetextRepr(positionAsString), Piece.fromStringRepr(pieceAsString));
        });
        return deserialized;
    }

    public long getWhiteSideMillisRemaining() {
        return whiteSideMillisRemaining;
    }

    public long getBlackSideMillisRemaining() {
        return blackSideMillisRemaining;
    }
}
