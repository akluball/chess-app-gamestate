package com.gitlab.akluball.chessapp.gamestate.itest.transfer;

import java.util.Objects;

public class MoveDto {
    private String pieceLetter;
    private String source;
    private String target;
    private String promotionPieceLetter;

    public static class Builder {
        private final MoveDto moveDto;

        Builder() {
            this.moveDto = new MoveDto();
        }

        public Builder pieceLetter(Character pieceChar) {
            this.moveDto.pieceLetter = Objects.nonNull(pieceChar) ? String.valueOf(pieceChar) : null;
            return this;
        }

        public Builder pawn() {
            return this.pieceLetter('P');
        }

        public Builder rook() {
            return this.pieceLetter('R');
        }

        public Builder knight() {
            return this.pieceLetter('N');
        }

        public Builder bishop() {
            return this.pieceLetter('B');
        }

        public Builder queen() {
            return this.pieceLetter('Q');
        }

        public Builder king() {
            return this.pieceLetter('K');
        }

        public Builder from(String source) {
            this.moveDto.source = source;
            return this;
        }

        public Builder to(String target) {
            this.moveDto.target = target;
            return this;
        }

        public Builder promoteToQueen() {
            this.moveDto.promotionPieceLetter = "Q";
            return this;
        }

        public Builder promoteToRook() {
            this.moveDto.promotionPieceLetter = "R";
            return this;
        }

        public Builder promoteToPawn() {
            this.moveDto.promotionPieceLetter = "P";
            return this;
        }

        public Builder promoteToKing() {
            this.moveDto.promotionPieceLetter = "K";
            return this;
        }

        public MoveDto build() {
            return this.moveDto;
        }
    }
}
