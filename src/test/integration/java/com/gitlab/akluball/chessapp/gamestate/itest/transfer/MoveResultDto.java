package com.gitlab.akluball.chessapp.gamestate.itest.transfer;

import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamestate.itest.model.SourceDisambiguation;

public class MoveResultDto {
    private boolean wasExecuted;
    private long whiteSideMillisRemaining;
    private long blackSideMillisRemaining;
    private MoveSummary.Dto moveSummary;

    public boolean wasExecuted() {
        return this.wasExecuted;
    }

    public long getWhiteSideMillisRemaining() {
        return whiteSideMillisRemaining;
    }

    public long getBlackSideMillisRemaining() {
        return blackSideMillisRemaining;
    }

    public String getPieceLetter() {
        return moveSummary.getPieceLetter();
    }

    public String getPromotionPieceLetter() {
        return this.moveSummary.getPromotionPieceLetter();
    }

    public String getSource() {
        return moveSummary.getSource().toMovetextRepr();
    }

    public String getTarget() {
        return moveSummary.getTarget().toMovetextRepr();
    }

    public SourceDisambiguation getSourceDisambiguation() {
        return this.moveSummary.getSourceDisambiguation();
    }

    public boolean isCapture() {
        return moveSummary.isCapture();
    }

    public long getDurationMillis() {
        return moveSummary.getDurationMillis();
    }

    public GameStatus getGameStatus() {
        return moveSummary.getGameStatus();
    }
}
