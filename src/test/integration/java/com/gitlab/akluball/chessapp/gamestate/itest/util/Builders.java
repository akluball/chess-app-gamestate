package com.gitlab.akluball.chessapp.gamestate.itest.util;

import com.gitlab.akluball.chessapp.gamestate.itest.model.Board;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.EndGameDto;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.GameStateCreateDto;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.MoveDto;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

@Singleton
public class Builders {
    private final Provider<GameStateCreateDto.Builder> gameStateCreateDtoBuilderProvider;
    private final Provider<GameState.Builder> gameStateBuilderProvider;
    private final Provider<MoveTimer.Builder> moveTimerBuilderProvider;
    private final Provider<Board.Builder> boardBuilderProvider;
    private final Provider<MoveHistory.Builder> moveHistoryBuilderProvider;
    private final Provider<MoveSummary.Builder> moveSummaryBuilderProvider;
    private final Provider<MoveDto.Builder> moveDtoBuilderProvider;
    private final Provider<EndGameDto.Builder> endGameDtoBuilderProvider;

    @Inject
    Builders(Provider<GameStateCreateDto.Builder> gameStateCreateDtoBuilderProvider,
            Provider<GameState.Builder> gameStateBuilderProvider,
            Provider<MoveTimer.Builder> moveTimerBuilderProvider,
            Provider<Board.Builder> boardBuilderProvider,
            Provider<MoveHistory.Builder> moveHistoryBuilderProvider,
            Provider<MoveSummary.Builder> moveSummaryBuilderProvider,
            Provider<MoveDto.Builder> moveDtoBuilderProvider,
            Provider<EndGameDto.Builder> endGameDtoBuilderProvider) {
        this.gameStateCreateDtoBuilderProvider = gameStateCreateDtoBuilderProvider;
        this.gameStateBuilderProvider = gameStateBuilderProvider;
        this.moveTimerBuilderProvider = moveTimerBuilderProvider;
        this.boardBuilderProvider = boardBuilderProvider;
        this.moveHistoryBuilderProvider = moveHistoryBuilderProvider;
        this.moveSummaryBuilderProvider = moveSummaryBuilderProvider;
        this.moveDtoBuilderProvider = moveDtoBuilderProvider;
        this.endGameDtoBuilderProvider = endGameDtoBuilderProvider;
    }

    public GameStateCreateDto.Builder gameStateCreateDto() {
        return this.gameStateCreateDtoBuilderProvider.get();
    }

    public GameState.Builder gameState() {
        return this.gameStateBuilderProvider.get();
    }

    public MoveTimer.Builder moveTimer() {
        return this.moveTimerBuilderProvider.get();
    }

    public Board.Builder board() {
        return this.boardBuilderProvider.get();
    }

    public MoveHistory.Builder moveHistory() {
        return this.moveHistoryBuilderProvider.get();
    }

    public MoveSummary.Builder moveSummary() {
        return this.moveSummaryBuilderProvider.get().gameStatus(GameStatus.IN_PROGRESS);
    }

    public MoveDto.Builder moveDto() {
        return this.moveDtoBuilderProvider.get();
    }

    public EndGameDto.Builder endGameDto() {
        return this.endGameDtoBuilderProvider.get();
    }
}
