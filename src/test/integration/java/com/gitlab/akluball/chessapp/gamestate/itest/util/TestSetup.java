package com.gitlab.akluball.chessapp.gamestate.itest.util;

import com.gitlab.akluball.chessapp.gamestate.itest.data.GameStateDao;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveHistory;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsClient;
import com.gitlab.akluball.chessapp.gamestate.itest.websocket.GameStateWsManager;

import javax.inject.Inject;

public class TestSetup {
    private final Builders builders;
    private final UniqueBuilders uniqueBuilders;
    private final GameStateDao gameStateDao;
    private final UserSecurity userSecurity;
    private final GameStateWsManager gameStateWsManager;

    @Inject
    TestSetup(Builders builders,
            UniqueBuilders uniqueBuilders,
            GameStateDao gameStateDao,
            UserSecurity userSecurity,
            GameStateWsManager gameStateWsManager) {
        this.builders = builders;
        this.uniqueBuilders = uniqueBuilders;
        this.gameStateDao = gameStateDao;
        this.userSecurity = userSecurity;
        this.gameStateWsManager = gameStateWsManager;
    }

    public GameState persistedGameState() {
        GameState gameState = this.uniqueBuilders.gameState().build();
        this.gameStateDao.persist(gameState);
        return gameState;
    }

    public GameState persistedGameState(String[] boardAsString, MoveHistory moveHistory) {
        GameState gameState = this.uniqueBuilders.gameState().moveHistory(moveHistory).board(boardAsString).build();
        this.gameStateDao.persist(gameState);
        return gameState;
    }

    public GameState persistedGameState(String[] boardAsString, MoveHistory moveHistory, MoveTimer moveTimer) {
        GameState gameState = this.uniqueBuilders.gameState().moveHistory(moveHistory).moveTimer(moveTimer)
                .board(boardAsString).build();
        this.gameStateDao.persist(gameState);
        return gameState;
    }

    public GameState persistedGameState(String[] boardAsString, MoveHistory moveHistory, GameStatus gameStatus) {
        GameState gameState = this.uniqueBuilders.gameState().moveHistory(moveHistory).board(boardAsString).build();
        this.gameStateDao.persist(gameState);
        return gameState;
    }

    public GameState whiteTurnPersistedGameState(String[] boardAsString) {
        MoveHistory moveHistory = this.builders.moveHistory().build();
        return this.persistedGameState(boardAsString, moveHistory);
    }

    public GameState blackTurnPersistedGameState(String[] boardAsString) {
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("h2").to("h3").build()).build();
        return this.persistedGameState(boardAsString, moveHistory);
    }

    public GameState whiteTurnPersistedGameState(String[] boardAsString, MoveTimer moveTimer) {
        MoveHistory moveHistory = this.builders.moveHistory().build();
        return this.persistedGameState(boardAsString, moveHistory, moveTimer);
    }

    public GameState blackTurnPersistedGameState(String[] boardAsString, MoveTimer moveTimer) {
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("h2").to("h3").build()).build();
        return this.persistedGameState(boardAsString, moveHistory, moveTimer);
    }

    public GameState whiteTurnPersistedGameState(String[] boardAsString, GameStatus gameStatus) {
        MoveHistory moveHistory = this.builders.moveHistory().build();
        return this.persistedGameState(boardAsString, moveHistory, gameStatus);
    }

    public GameState blackTurnPersistedGameState(String[] boardAsString, GameStatus gameStatus) {
        MoveHistory moveHistory = this.builders.moveHistory()
                .moveSummary(this.builders.moveSummary().pawn().from("h2").to("h3").build()).build();
        return this.persistedGameState(boardAsString, moveHistory, gameStatus);
    }

    public GameStateWsClient whiteSideWsClient(GameState gameState) {
        String token = this.userSecurity.tokenFor(gameState.getWhiteSideUserId());
        return this.gameStateWsManager.createClient(token, gameState);
    }

    public GameStateWsClient blackSideWsClient(GameState gameState) {
        String token = this.userSecurity.tokenFor(gameState.getBlackSideUserId());
        return this.gameStateWsManager.createClient(token, gameState);
    }

    public void cleanup() {
        this.gameStateWsManager.cleanup();
    }
}
