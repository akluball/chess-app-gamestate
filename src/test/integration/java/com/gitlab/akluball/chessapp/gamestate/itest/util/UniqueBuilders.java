package com.gitlab.akluball.chessapp.gamestate.itest.util;

import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;
import com.gitlab.akluball.chessapp.gamestate.itest.model.MoveTimer;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.GameStateCreateDto;

import javax.inject.Inject;

public class UniqueBuilders {
    private final UniqueData uniqueData;
    private final Builders builders;

    @Inject
    UniqueBuilders(UniqueData uniqueData, Builders builders) {
        this.uniqueData = uniqueData;
        this.builders = builders;
    }

    public GameStateCreateDto.Builder gameStateCreateDto() {
        return this.builders.gameStateCreateDto()
                .whiteSideUserId(this.uniqueData.userId())
                .blackSideUserId(this.uniqueData.userId())
                .millisPerPlayer(600_000L);
    }

    public GameState.Builder gameState() {
        MoveTimer moveTimer = this.builders.moveTimer().whiteSideMillis(600_000).blackSideMillis(600_000)
                .secondsSinceStart(5).build();
        return this.builders.gameState()
                .gameHistoryId(this.uniqueData.gameHistoryId())
                .whiteSideUserId(this.uniqueData.userId())
                .blackSideUserId(this.uniqueData.userId())
                .moveTimer(moveTimer);
    }
}
