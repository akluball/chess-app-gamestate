package com.gitlab.akluball.chessapp.gamestate.itest.util;

import javax.inject.Inject;
import java.util.concurrent.atomic.AtomicInteger;

public class UniqueData {
    private final AtomicInteger gameHistoryIdCounter;
    private final AtomicInteger userIdCounter;

    @Inject
    UniqueData(UniqueDataDao uniqueDataDao) {
        int uniqueDataId = uniqueDataDao.nextId();
        this.gameHistoryIdCounter = new AtomicInteger(uniqueDataId * 1_000);
        this.userIdCounter = new AtomicInteger(uniqueDataId * 1_000);
    }

    public int gameHistoryId() {
        return this.gameHistoryIdCounter.getAndIncrement();
    }

    public int userId() {
        return this.userIdCounter.getAndIncrement();
    }
}
