package com.gitlab.akluball.chessapp.gamestate.itest.util;

import com.google.common.collect.Range;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.time.Instant;
import java.util.stream.Collectors;

public class Util {
    public static RuntimeException asRuntime(Throwable throwable) {
        return new RuntimeException(throwable);
    }

    public static long secondsToMillis(long seconds) {
        return seconds * 1_000;
    }

    public static void sleepMillis(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw asRuntime(e);
        }
    }

    @SafeVarargs
    public static <E> E[] concat(Class<E> klass, E[]... arraysToConcat) {
        int concatLength = 0;
        for (E[] arrayToConcat : arraysToConcat) {
            concatLength += arrayToConcat.length;
        }
        @SuppressWarnings("unchecked")
        E[] concat = (E[]) Array.newInstance(klass, concatLength);
        int concatIndex = 0;
        for (E[] arrayToConcat : arraysToConcat) {
            for (E element : arrayToConcat) {
                concat[concatIndex] = element;
                concatIndex++;
            }
        }
        return concat;
    }

    public static String inputStreamToString(InputStream inputStream) {
        return new BufferedReader(new InputStreamReader(inputStream))
                .lines()
                .collect(Collectors.joining(System.lineSeparator()));
    }

    public static Range<Instant> plusMinusSecond(Instant instant) {
        return Range.closed(instant.minusSeconds(1), instant.plusSeconds(1));
    }

    public static Range<Instant> bufferMinusFourSeconds(Instant instant) {
        return Range.closed(instant.minusSeconds(4), instant);
    }

    public static Range<Long> bufferAbove5000(long start) {
        return Range.closed(start, start + 5000);
    }

    public static Range<Long> bufferBelow5000(long start) {
        return Range.closed(start - 5000, start);
    }
}
