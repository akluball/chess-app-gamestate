package com.gitlab.akluball.chessapp.gamestate.itest.websocket;

import com.gitlab.akluball.chessapp.gamestate.itest.transfer.MoveDto;
import com.gitlab.akluball.chessapp.gamestate.itest.transfer.MoveResultDto;

import javax.json.bind.Jsonb;
import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.Decoder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static com.gitlab.akluball.chessapp.gamestate.itest.param.GuicyParameterResolver.INJECTOR;
import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.asRuntime;

@ClientEndpoint(
        encoders = GameStateWsClient.MoveDtoEncoder.class,
        decoders = GameStateWsClient.MoveResultDecoder.class
)
public class GameStateWsClient {
    private static final long WAIT_MILLIS = 2000;
    private Session session;
    private BlockingQueue<MoveResultDto> moveResults = new ArrayBlockingQueue<>(10);
    private BlockingQueue<CloseReason> closeReasons = new ArrayBlockingQueue<>(1);

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        this.session = session;
    }

    @OnClose
    public void onClose(CloseReason closeReason) {
        this.closeReasons.add(closeReason);
    }

    @OnError
    public void onError(Throwable throwable) {
        throw asRuntime(throwable);
    }

    @OnMessage
    public void onMessage(MoveResultDto moveResultDto) {
        this.moveResults.add(moveResultDto);
    }

    public void sendMove(MoveDto moveDto) {
        try {
            if (Objects.nonNull(moveDto)) {
                this.session.getBasicRemote().sendObject(moveDto);
            } else {
                this.session.getBasicRemote().sendObject("null");
            }
        } catch (IOException | EncodeException e) {
            throw asRuntime(e);
        }
    }

    public MoveResultDto waitForMoveResult() {
        try {
            return this.moveResults.poll(WAIT_MILLIS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw asRuntime(e);
        }
    }

    public CloseReason waitForCloseReason() {
        try {
            return this.closeReasons.poll(WAIT_MILLIS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw asRuntime(e);
        }
    }

    public void close() {
        try {
            this.session.close();
        } catch (IOException e) {
            throw asRuntime(e);
        }
    }

    public static class MoveDtoEncoder implements Encoder.Text<MoveDto> {
        private final Jsonb jsonb = INJECTOR.getInstance(Jsonb.class);

        @Override
        public String encode(MoveDto moveDto) {
            return this.jsonb.toJson(moveDto);
        }

        @Override
        public void init(EndpointConfig config) {
        }

        @Override
        public void destroy() {
        }
    }

    public static class MoveResultDecoder implements Decoder.Text<MoveResultDto> {
        private final Jsonb jsonb = INJECTOR.getInstance(Jsonb.class);

        @Override
        public MoveResultDto decode(String encodedMoveResult) {
            return this.jsonb.fromJson(encodedMoveResult, MoveResultDto.class);
        }

        @Override
        public boolean willDecode(String encodedWsMessage) {
            return true;
        }

        @Override
        public void init(EndpointConfig config) {
        }

        @Override
        public void destroy() {
        }
    }
}
