package com.gitlab.akluball.chessapp.gamestate.itest.websocket;

import com.gitlab.akluball.chessapp.gamestate.itest.TestConfig;
import com.gitlab.akluball.chessapp.gamestate.itest.model.GameState;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.WebSocketContainer;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import static com.gitlab.akluball.chessapp.gamestate.itest.util.Util.asRuntime;

public class GameStateWsManager {

    private final Provider<GameStateWsClient> gameStateWsClientProvider;
    private WebSocketContainer websocketContainer;
    private UriBuilder wsUriBuilder;
    private Set<GameStateWsClient> clients;

    @Inject
    public GameStateWsManager(TestConfig testConfig,
            Provider<GameStateWsClient> gameStateWsClientProvider) {
        this.gameStateWsClientProvider = gameStateWsClientProvider;
        this.websocketContainer = ContainerProvider.getWebSocketContainer();
        this.wsUriBuilder = UriBuilder.fromUri(testConfig.gameStateUri())
                .scheme("ws")
                .path("api")
                .path("gamestate")
                .path("{gameHistoryId}");
        this.clients = new HashSet<>();
    }

    private GameStateWsClient createClient(String token, int gameHistoryId) {
        GameStateWsClient client = this.gameStateWsClientProvider.get();
        UriBuilder uriBuilder = this.wsUriBuilder.clone()
                .resolveTemplate("gameHistoryId", gameHistoryId);
        if (token != null) {
            uriBuilder.queryParam("bearer", token);
        }
        URI uri = uriBuilder.build();
        try {
            this.websocketContainer.connectToServer(client, uri);
        } catch (DeploymentException | IOException e) {
            throw asRuntime(e);
        }
        this.clients.add(client);
        return client;
    }

    public GameStateWsClient createClient(String token, GameState gameState) {
        return this.createClient(token, gameState.getGameHistoryId());
    }

    public void cleanup() {
        this.clients.forEach(GameStateWsClient::close);
    }
}
